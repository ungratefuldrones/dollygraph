{
  "format_version" : "1.0",
  "generated_by" : "cytoscape-3.4.0",
  "target_cytoscapejs_version" : "~2.1",
  "data" : {
    "shared_name" : "Main Component Final(1)",
    "name" : "Dolly Parton Ego Net",
    "SUID" : 52,
    "__Annotations" : [ "" ],
    "networkMetadata" : "",
    "selected" : true
  },
  "elements" : {
    "nodes" : [ {
      "data" : {
        "id" : "349",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Drew_Cline",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Drew Cline",
        "SelfLoops" : 0,
        "SUID" : 349,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 222.0365753173828,
        "y" : 110.40150451660156
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "348",
        "ClosenessCentrality" : 0.23735882,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82149829,
        "Stress" : 120704,
        "TopologicalCoefficient" : 0.3372549,
        "shared_name" : "Jere_Carr_II",
        "BetweennessCentrality" : 6.32E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jere_Carr_II",
        "SelfLoops" : 0,
        "SUID" : 348,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.21303075,
        "selected" : false,
        "NeighborhoodConnectivity" : 115.66666667
      },
      "position" : {
        "x" : 684.07763671875,
        "y" : 8.03072738647461
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "347",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mike_Casteel",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mike_Casteel",
        "SelfLoops" : 0,
        "SUID" : 347,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2305.818359375,
        "y" : 1517.81689453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "346",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Lori_Casteel",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lori_Casteel",
        "SelfLoops" : 0,
        "SUID" : 346,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 540.6624145507812,
        "y" : 546.3294067382812
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "345",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Matt_Boynton",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Matt_Boynton",
        "SelfLoops" : 0,
        "SUID" : 345,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1804.1280517578125,
        "y" : 1352.116943359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "344",
        "ClosenessCentrality" : 0.25175083,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.66666667,
        "Radiality" : 0.8348788,
        "Stress" : 31604,
        "TopologicalCoefficient" : 0.38584475,
        "shared_name" : "Lisa_Bevill",
        "BetweennessCentrality" : 4.69E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lisa_Bevill",
        "SelfLoops" : 0,
        "SUID" : 344,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 3.97218155,
        "selected" : false,
        "NeighborhoodConnectivity" : 225.33333333
      },
      "position" : {
        "x" : 777.2444458007812,
        "y" : 855.2557373046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "343",
        "ClosenessCentrality" : 0.23563912,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.81979014,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51618123,
        "shared_name" : "Proinsias_O'Maonaigh",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Proinsias_O'Maonaigh",
        "SelfLoops" : 0,
        "SUID" : 343,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24377745,
        "selected" : false,
        "NeighborhoodConnectivity" : 159.5
      },
      "position" : {
        "x" : 1625.5050048828125,
        "y" : 1146.9832763671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "342",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Kristin_Bauer",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kristin_Bauer",
        "SelfLoops" : 0,
        "SUID" : 342,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2184.669921875,
        "y" : 1423.0018310546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "341",
        "ClosenessCentrality" : 0.24493943,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82874211,
        "Stress" : 31604,
        "TopologicalCoefficient" : 0.53891509,
        "shared_name" : "Robert_Bailey,_Jr.",
        "BetweennessCentrality" : 4.69E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Robert_Bailey,_Jr.",
        "SelfLoops" : 0,
        "SUID" : 341,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.08264194,
        "selected" : false,
        "NeighborhoodConnectivity" : 229.5
      },
      "position" : {
        "x" : 682.5721435546875,
        "y" : 407.7384948730469
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "340",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Pennie_Aust",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Pennie_Aust",
        "SelfLoops" : 0,
        "SUID" : 340,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2092.94384765625,
        "y" : 1182.923095703125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "339",
        "ClosenessCentrality" : 0.25395055,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.1,
        "Radiality" : 0.8367903,
        "Stress" : 349358,
        "TopologicalCoefficient" : 0.22554517,
        "shared_name" : "Monisa_Angell",
        "BetweennessCentrality" : 9.236E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Monisa_Angell",
        "SelfLoops" : 0,
        "SUID" : 339,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 3.93777452,
        "selected" : false,
        "NeighborhoodConnectivity" : 145.4
      },
      "position" : {
        "x" : 308.1224365234375,
        "y" : 2052.457275390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "338",
        "ClosenessCentrality" : 0.23563912,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.81979014,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51618123,
        "shared_name" : "Frankie_Kennedy",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Frankie_Kennedy",
        "SelfLoops" : 0,
        "SUID" : 338,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24377745,
        "selected" : false,
        "NeighborhoodConnectivity" : 159.5
      },
      "position" : {
        "x" : 1591.2593994140625,
        "y" : 926.0430908203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "337",
        "ClosenessCentrality" : 0.23563912,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.81979014,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51618123,
        "shared_name" : "Mark_Kelly",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mark_Kelly",
        "SelfLoops" : 0,
        "SUID" : 337,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24377745,
        "selected" : false,
        "NeighborhoodConnectivity" : 159.5
      },
      "position" : {
        "x" : 1709.0093994140625,
        "y" : 1192.2437744140625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "336",
        "ClosenessCentrality" : 0.28168175,
        "degree_layout" : 7,
        "Eccentricity" : 10,
        "Degree" : 23,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.16600791,
        "Radiality" : 0.85832746,
        "Stress" : 7469938,
        "TopologicalCoefficient" : 0.06488294,
        "shared_name" : "Matt_Rollings",
        "BetweennessCentrality" : 0.00317575,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Matt_Rollings",
        "SelfLoops" : 0,
        "SUID" : 336,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 23,
        "AverageShortestPathLength" : 3.55010574,
        "selected" : false,
        "NeighborhoodConnectivity" : 105.56521739
      },
      "position" : {
        "x" : 745.0401000976562,
        "y" : 1644.0081787109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "335",
        "ClosenessCentrality" : 0.2366233,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82077075,
        "Stress" : 126426,
        "TopologicalCoefficient" : 0.5,
        "shared_name" : "Toby_Seay",
        "BetweennessCentrality" : 9.078E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Toby_Seay",
        "SelfLoops" : 0,
        "SUID" : 335,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.22612657,
        "selected" : false,
        "NeighborhoodConnectivity" : 151.0
      },
      "position" : {
        "x" : 357.4502868652344,
        "y" : 163.3596954345703
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "334",
        "ClosenessCentrality" : 0.24102574,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.66666667,
        "Radiality" : 0.8250592,
        "Stress" : 30180,
        "TopologicalCoefficient" : 0.35077519,
        "shared_name" : "Ciaran_Tourish",
        "BetweennessCentrality" : 1.213E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ciaran_Tourish",
        "SelfLoops" : 0,
        "SUID" : 334,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.14893444,
        "selected" : false,
        "NeighborhoodConnectivity" : 120.66666667
      },
      "position" : {
        "x" : 1445.00927734375,
        "y" : 1142.7044677734375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "333",
        "ClosenessCentrality" : 0.23684667,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82099217,
        "Stress" : 303238,
        "TopologicalCoefficient" : 0.34381551,
        "shared_name" : "Daithi_Sproule",
        "BetweennessCentrality" : 1.9668E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Daithi_Sproule",
        "SelfLoops" : 0,
        "SUID" : 333,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.22214088,
        "selected" : false,
        "NeighborhoodConnectivity" : 109.66666667
      },
      "position" : {
        "x" : 1686.492919921875,
        "y" : 920.4450073242188
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "332",
        "ClosenessCentrality" : 0.29639094,
        "degree_layout" : 21,
        "Eccentricity" : 11,
        "Degree" : 99,
        "PartnerOfMultiEdgedNodePairs" : 7,
        "ClusteringCoefficient" : 0.05398949,
        "Radiality" : 0.86811543,
        "Stress" : 14656078,
        "TopologicalCoefficient" : 0.0203577,
        "shared_name" : "Patty_Loveless",
        "BetweennessCentrality" : 0.0070388,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Patty_Loveless",
        "SelfLoops" : 0,
        "SUID" : 332,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 99,
        "AverageShortestPathLength" : 3.37392224,
        "selected" : false,
        "NeighborhoodConnectivity" : 39.19565217
      },
      "position" : {
        "x" : 672.261474609375,
        "y" : 1496.5877685546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "331",
        "ClosenessCentrality" : 0.24292602,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.82686225,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.52616279,
        "shared_name" : "Mike_Snider",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mike_Snider",
        "SelfLoops" : 0,
        "SUID" : 331,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.11647958,
        "selected" : false,
        "NeighborhoodConnectivity" : 181.0
      },
      "position" : {
        "x" : 1210.8131103515625,
        "y" : 1031.955322265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "330",
        "ClosenessCentrality" : 0.23650039,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82064874,
        "Stress" : 152634,
        "TopologicalCoefficient" : 0.34441367,
        "shared_name" : "Mairead_Ni_Mhaonaigh",
        "BetweennessCentrality" : 1.0756E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mairead_Ni_Mhaonaigh",
        "SelfLoops" : 0,
        "SUID" : 330,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.22832276,
        "selected" : false,
        "NeighborhoodConnectivity" : 124.66666667
      },
      "position" : {
        "x" : 1770.9112548828125,
        "y" : 958.4036254882812
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "329",
        "ClosenessCentrality" : 0.23482446,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81897222,
        "Stress" : 130824,
        "TopologicalCoefficient" : 0.50162866,
        "shared_name" : "Rick_Williams",
        "BetweennessCentrality" : 4.834E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rick_Williams",
        "SelfLoops" : 0,
        "SUID" : 329,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.25850008,
        "selected" : false,
        "NeighborhoodConnectivity" : 155.0
      },
      "position" : {
        "x" : 2219.848876953125,
        "y" : 1283.356689453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "328",
        "ClosenessCentrality" : 0.2667853,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.21818182,
        "Radiality" : 0.84731486,
        "Stress" : 1297820,
        "TopologicalCoefficient" : 0.11863503,
        "shared_name" : "David_Hungate",
        "BetweennessCentrality" : 4.0179E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Hungate",
        "SelfLoops" : 0,
        "SUID" : 328,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.74833252,
        "selected" : false,
        "NeighborhoodConnectivity" : 121.54545455
      },
      "position" : {
        "x" : 580.6727905273438,
        "y" : 1088.1993408203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "327",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Rollow_Welch",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rollow_Welch",
        "SelfLoops" : 0,
        "SUID" : 327,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 692.6537475585938,
        "y" : 538.3660888671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "326",
        "ClosenessCentrality" : 0.28252976,
        "degree_layout" : 8,
        "Eccentricity" : 10,
        "Degree" : 45,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.07751938,
        "Radiality" : 0.85891944,
        "Stress" : 14019744,
        "TopologicalCoefficient" : 0.0370345,
        "shared_name" : "Maura_O'Connell",
        "BetweennessCentrality" : 0.00717857,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Maura_O'Connell",
        "SelfLoops" : 0,
        "SUID" : 326,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 45,
        "AverageShortestPathLength" : 3.53945014,
        "selected" : false,
        "NeighborhoodConnectivity" : 50.6744186
      },
      "position" : {
        "x" : 1105.9205322265625,
        "y" : 1300.4930419921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "325",
        "ClosenessCentrality" : 0.23604178,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.82019233,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51215805,
        "shared_name" : "Don_Warden",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Don_Warden",
        "SelfLoops" : 0,
        "SUID" : 325,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.23653815,
        "selected" : false,
        "NeighborhoodConnectivity" : 168.5
      },
      "position" : {
        "x" : 1335.4266357421875,
        "y" : 1826.7862548828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "324",
        "ClosenessCentrality" : 0.23591495,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.8200658,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.34683281,
        "shared_name" : "Ciaran_Curran",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ciaran_Curran",
        "SelfLoops" : 0,
        "SUID" : 324,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.23881568,
        "selected" : false,
        "NeighborhoodConnectivity" : 111.33333333
      },
      "position" : {
        "x" : 1787.520263671875,
        "y" : 1054.759033203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "323",
        "ClosenessCentrality" : 0.23817273,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 15,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.02857143,
        "Radiality" : 0.82229814,
        "Stress" : 3088206,
        "TopologicalCoefficient" : 0.06996124,
        "shared_name" : "Dermot_Byrne",
        "BetweennessCentrality" : 0.00212135,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dermot_Byrne",
        "SelfLoops" : 0,
        "SUID" : 323,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 15,
        "AverageShortestPathLength" : 4.19863348,
        "selected" : false,
        "NeighborhoodConnectivity" : 24.86666667
      },
      "position" : {
        "x" : 1785.1546630859375,
        "y" : 1117.9796142578125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "322",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "David_Sutherland",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Sutherland",
        "SelfLoops" : 0,
        "SUID" : 322,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2340.758056640625,
        "y" : 1373.0111083984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "321",
        "ClosenessCentrality" : 0.24128592,
        "degree_layout" : 10,
        "Eccentricity" : 11,
        "Degree" : 32,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.02822581,
        "Radiality" : 0.82530774,
        "Stress" : 4279526,
        "TopologicalCoefficient" : 0.03431373,
        "shared_name" : "Altan",
        "BetweennessCentrality" : 0.0029502,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Altan",
        "SelfLoops" : 0,
        "SUID" : 321,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 32,
        "AverageShortestPathLength" : 4.14446071,
        "selected" : false,
        "NeighborhoodConnectivity" : 16.28125
      },
      "position" : {
        "x" : 1664.4464111328125,
        "y" : 1051.9730224609375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "320",
        "ClosenessCentrality" : 0.23573853,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81988956,
        "Stress" : 23932,
        "TopologicalCoefficient" : 0.51656627,
        "shared_name" : "Monty_Allen",
        "BetweennessCentrality" : 6.99E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Monty_Allen",
        "SelfLoops" : 0,
        "SUID" : 320,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24198796,
        "selected" : false,
        "NeighborhoodConnectivity" : 172.5
      },
      "position" : {
        "x" : 964.3264770507812,
        "y" : 458.4812316894531
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "319",
        "ClosenessCentrality" : 0.28911413,
        "degree_layout" : 11,
        "Eccentricity" : 10,
        "Degree" : 71,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.04347826,
        "Radiality" : 0.86339768,
        "Stress" : 15693430,
        "TopologicalCoefficient" : 0.02251261,
        "shared_name" : "Kris_Kristofferson",
        "BetweennessCentrality" : 0.00889447,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kris_Kristofferson",
        "SelfLoops" : 0,
        "SUID" : 319,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 71,
        "AverageShortestPathLength" : 3.45884171,
        "selected" : false,
        "NeighborhoodConnectivity" : 38.61428571
      },
      "position" : {
        "x" : 805.7879028320312,
        "y" : 1660.46533203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "318",
        "ClosenessCentrality" : 0.25143675,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.83460315,
        "Stress" : 416932,
        "TopologicalCoefficient" : 0.21172023,
        "shared_name" : "Larry_Knechtel",
        "BetweennessCentrality" : 1.4569E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Larry_Knechtel",
        "SelfLoops" : 0,
        "SUID" : 318,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 3.97714332,
        "selected" : false,
        "NeighborhoodConnectivity" : 113.0
      },
      "position" : {
        "x" : 1444.7354736328125,
        "y" : -125.41754913330078
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "317",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Phil_Sommers",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Phil_Sommers",
        "SelfLoops" : 0,
        "SUID" : 317,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 611.3468627929688,
        "y" : 281.81829833984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "316",
        "ClosenessCentrality" : 0.27273937,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 17,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.11764706,
        "Radiality" : 0.85186089,
        "Stress" : 4264236,
        "TopologicalCoefficient" : 0.07655241,
        "shared_name" : "Randy_Scruggs",
        "BetweennessCentrality" : 0.00194146,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Randy_Scruggs",
        "SelfLoops" : 0,
        "SUID" : 316,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 17,
        "AverageShortestPathLength" : 3.66650399,
        "selected" : false,
        "NeighborhoodConnectivity" : 99.41176471
      },
      "position" : {
        "x" : 624.390380859375,
        "y" : 1901.29345703125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "315",
        "ClosenessCentrality" : 0.26219928,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.12727273,
        "Radiality" : 0.84367261,
        "Stress" : 1500136,
        "TopologicalCoefficient" : 0.11714338,
        "shared_name" : "John_Wesley_Ryles",
        "BetweennessCentrality" : 4.3743E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "John_Wesley_Ryles",
        "SelfLoops" : 0,
        "SUID" : 315,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.81389296,
        "selected" : false,
        "NeighborhoodConnectivity" : 117.18181818
      },
      "position" : {
        "x" : 514.9420776367188,
        "y" : 1712.7767333984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "314",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Ray_Parker",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ray_Parker",
        "SelfLoops" : 0,
        "SUID" : 314,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1988.7547607421875,
        "y" : 1091.4793701171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "313",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Maighread_Ni_Dhomnaill",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Maighread_Ni_Dhomnaill",
        "SelfLoops" : 0,
        "SUID" : 313,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 348.711181640625,
        "y" : 22.188982009887695
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "312",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Sean_McClintock",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sean_McClintock",
        "SelfLoops" : 0,
        "SUID" : 312,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2360.1396484375,
        "y" : 1221.4290771484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "311",
        "ClosenessCentrality" : 0.28803711,
        "degree_layout" : 15,
        "Eccentricity" : 11,
        "Degree" : 47,
        "PartnerOfMultiEdgedNodePairs" : 5,
        "ClusteringCoefficient" : 0.17886179,
        "Radiality" : 0.86267918,
        "Stress" : 8092118,
        "TopologicalCoefficient" : 0.03993705,
        "shared_name" : "Chris_Thile",
        "BetweennessCentrality" : 0.00321401,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chris_Thile",
        "SelfLoops" : 0,
        "SUID" : 311,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 47,
        "AverageShortestPathLength" : 3.47177485,
        "selected" : false,
        "NeighborhoodConnectivity" : 61.07142857
      },
      "position" : {
        "x" : 1111.9403076171875,
        "y" : 1419.0596923828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "310",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Cari_Landers",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Cari_Landers",
        "SelfLoops" : 0,
        "SUID" : 310,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 512.0603637695312,
        "y" : 79.66313171386719
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "309",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mark_Kiracofe",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mark_Kiracofe",
        "SelfLoops" : 0,
        "SUID" : 309,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2035.578369140625,
        "y" : 805.890380859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "308",
        "ClosenessCentrality" : 0.25948205,
        "degree_layout" : 7,
        "Eccentricity" : 11,
        "Degree" : 26,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.17333333,
        "Radiality" : 0.84145383,
        "Stress" : 1722498,
        "TopologicalCoefficient" : 0.05503836,
        "shared_name" : "Terry_Eldredge",
        "BetweennessCentrality" : 7.0566E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Terry_Eldredge",
        "SelfLoops" : 0,
        "SUID" : 308,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 26,
        "AverageShortestPathLength" : 3.85383114,
        "selected" : false,
        "NeighborhoodConnectivity" : 43.08
      },
      "position" : {
        "x" : 351.81146240234375,
        "y" : 1059.2889404296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "307",
        "ClosenessCentrality" : 0.24599808,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.28571429,
        "Radiality" : 0.8297182,
        "Stress" : 358864,
        "TopologicalCoefficient" : 0.16495957,
        "shared_name" : "Sonny_Osborne",
        "BetweennessCentrality" : 1.2621E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sonny_Osborne",
        "SelfLoops" : 0,
        "SUID" : 307,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 4.06507239,
        "selected" : false,
        "NeighborhoodConnectivity" : 87.57142857
      },
      "position" : {
        "x" : 275.7605895996094,
        "y" : 832.5681762695312
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "306",
        "ClosenessCentrality" : 0.26809429,
        "degree_layout" : 8,
        "Eccentricity" : 11,
        "Degree" : 40,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.0625889,
        "Radiality" : 0.84833162,
        "Stress" : 5369644,
        "TopologicalCoefficient" : 0.03754814,
        "shared_name" : "Bobby_Osborne",
        "BetweennessCentrality" : 0.00287143,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bobby_Osborne",
        "SelfLoops" : 0,
        "SUID" : 306,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 40,
        "AverageShortestPathLength" : 3.73003091,
        "selected" : false,
        "NeighborhoodConnectivity" : 34.13157895
      },
      "position" : {
        "x" : 487.8940734863281,
        "y" : 1201.702880859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "305",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Teresa_Hughes",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Teresa_Hughes",
        "SelfLoops" : 0,
        "SUID" : 305,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 475.2672119140625,
        "y" : -48.57632827758789
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "304",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Phil_Gitomer",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Phil_Gitomer",
        "SelfLoops" : 0,
        "SUID" : 304,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2226.4423828125,
        "y" : 1137.7923583984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "303",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Curtis_Flatt",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Curtis_Flatt",
        "SelfLoops" : 0,
        "SUID" : 303,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 759.1148681640625,
        "y" : 260.72021484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "302",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Jennifer_O'Brien_Enoch",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jennifer_O'Brien_Enoch",
        "SelfLoops" : 0,
        "SUID" : 302,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1940.59326171875,
        "y" : 1244.064697265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "301",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Carol_Elliott",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Carol_Elliott",
        "SelfLoops" : 0,
        "SUID" : 301,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 641.2689208984375,
        "y" : 140.3193817138672
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "300",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Michael_Davis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Michael_Davis",
        "SelfLoops" : 0,
        "SUID" : 300,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2240.572021484375,
        "y" : 997.5978393554688
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "299",
        "ClosenessCentrality" : 0.24562946,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.09090909,
        "Radiality" : 0.82937928,
        "Stress" : 1188592,
        "TopologicalCoefficient" : 0.10695187,
        "shared_name" : "Bruce_Watkins",
        "BetweennessCentrality" : 5.4649E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bruce_Watkins",
        "SelfLoops" : 0,
        "SUID" : 299,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 4.07117293,
        "selected" : false,
        "NeighborhoodConnectivity" : 62.18181818
      },
      "position" : {
        "x" : 76.91251373291016,
        "y" : 1686.9375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "298",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Suzanne_Coz",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Suzanne_Coz",
        "SelfLoops" : 0,
        "SUID" : 298,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 846.2694702148438,
        "y" : 545.0504150390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "297",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Shane_Cooper",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Shane_Cooper",
        "SelfLoops" : 0,
        "SUID" : 297,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2367.57958984375,
        "y" : 1067.341552734375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "296",
        "ClosenessCentrality" : 0.26391602,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 8,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.10714286,
        "Radiality" : 0.84505088,
        "Stress" : 5754994,
        "TopologicalCoefficient" : 0.1396771,
        "shared_name" : "David_Lindley",
        "BetweennessCentrality" : 0.00379947,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Lindley",
        "SelfLoops" : 0,
        "SUID" : 296,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 8,
        "AverageShortestPathLength" : 3.78908411,
        "selected" : false,
        "NeighborhoodConnectivity" : 143.25
      },
      "position" : {
        "x" : 499.919189453125,
        "y" : 1995.1102294921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "295",
        "ClosenessCentrality" : 0.25196754,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 9,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.08333333,
        "Radiality" : 0.8350686,
        "Stress" : 350136,
        "TopologicalCoefficient" : 0.13085622,
        "shared_name" : "Bob_Mater",
        "BetweennessCentrality" : 1.3927E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bob_Mater",
        "SelfLoops" : 0,
        "SUID" : 295,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 9,
        "AverageShortestPathLength" : 3.96876525,
        "selected" : false,
        "NeighborhoodConnectivity" : 81.44444444
      },
      "position" : {
        "x" : -37.51172637939453,
        "y" : 1078.927001953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "294",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Timmy_Collins",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Timmy_Collins",
        "SelfLoops" : 0,
        "SUID" : 294,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 906.2974243164062,
        "y" : 256.63714599609375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "293",
        "ClosenessCentrality" : 0.25878292,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.8408754,
        "Stress" : 211630,
        "TopologicalCoefficient" : 0.36785512,
        "shared_name" : "Don_Cobb",
        "BetweennessCentrality" : 3.892E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Don_Cobb",
        "SelfLoops" : 0,
        "SUID" : 293,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 3.86424272,
        "selected" : false,
        "NeighborhoodConnectivity" : 217.0
      },
      "position" : {
        "x" : 850.1416015625,
        "y" : 917.921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "292",
        "ClosenessCentrality" : 0.27684824,
        "degree_layout" : 8,
        "Eccentricity" : 11,
        "Degree" : 37,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.04654655,
        "Radiality" : 0.85488404,
        "Stress" : 17178754,
        "TopologicalCoefficient" : 0.03883176,
        "shared_name" : "Hargus_\"Pig\"_Robbins",
        "BetweennessCentrality" : 0.00787758,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Hargus_\"Pig\"_Robbins",
        "SelfLoops" : 0,
        "SUID" : 292,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 37,
        "AverageShortestPathLength" : 3.6120872,
        "selected" : false,
        "NeighborhoodConnectivity" : 67.32432432
      },
      "position" : {
        "x" : 489.3944091796875,
        "y" : 1436.018798828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "291",
        "ClosenessCentrality" : 0.26269231,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 17,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.08823529,
        "Radiality" : 0.84407028,
        "Stress" : 3793040,
        "TopologicalCoefficient" : 0.07772159,
        "shared_name" : "Jim_Hoke",
        "BetweennessCentrality" : 0.00180172,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jim_Hoke",
        "SelfLoops" : 0,
        "SUID" : 291,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 17,
        "AverageShortestPathLength" : 3.80673499,
        "selected" : false,
        "NeighborhoodConnectivity" : 69.0
      },
      "position" : {
        "x" : 1409.0423583984375,
        "y" : 149.8012237548828
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "290",
        "ClosenessCentrality" : 0.23493665,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81908519,
        "Stress" : 48894,
        "TopologicalCoefficient" : 0.50327869,
        "shared_name" : "Michael_Clark",
        "BetweennessCentrality" : 2.297E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Michael_Clark",
        "SelfLoops" : 0,
        "SUID" : 290,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.25646657,
        "selected" : false,
        "NeighborhoodConnectivity" : 154.5
      },
      "position" : {
        "x" : 2344.899658203125,
        "y" : 901.20947265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "289",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Tony_Chase",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tony_Chase",
        "SelfLoops" : 0,
        "SUID" : 289,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1719.6395263671875,
        "y" : 2274.700439453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "288",
        "ClosenessCentrality" : 0.23977064,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82385265,
        "Stress" : 1602142,
        "TopologicalCoefficient" : 0.26987179,
        "shared_name" : "Jamie_Johnson",
        "BetweennessCentrality" : 7.8171E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jamie_Johnson",
        "SelfLoops" : 0,
        "SUID" : 288,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.17065235,
        "selected" : false,
        "NeighborhoodConnectivity" : 105.5
      },
      "position" : {
        "x" : -22.267391204833984,
        "y" : 1188.75537109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "287",
        "ClosenessCentrality" : 0.2814045,
        "degree_layout" : 14,
        "Eccentricity" : 10,
        "Degree" : 95,
        "PartnerOfMultiEdgedNodePairs" : 4,
        "ClusteringCoefficient" : 0.01978022,
        "Radiality" : 0.85813315,
        "Stress" : 18952416,
        "TopologicalCoefficient" : 0.01636763,
        "shared_name" : "George_Jones",
        "BetweennessCentrality" : 0.01059205,
        "NumberOfUndirectedEdges" : 0,
        "name" : "George_Jones",
        "SelfLoops" : 0,
        "SUID" : 287,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 95,
        "AverageShortestPathLength" : 3.55360338,
        "selected" : false,
        "NeighborhoodConnectivity" : 23.03296703
      },
      "position" : {
        "x" : 411.89044189453125,
        "y" : 1533.112548828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "286",
        "ClosenessCentrality" : 0.26984197,
        "degree_layout" : 6,
        "Eccentricity" : 11,
        "Degree" : 70,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.00538302,
        "Radiality" : 0.84967373,
        "Stress" : 15444876,
        "TopologicalCoefficient" : 0.01806723,
        "shared_name" : "Robbie_Fulks",
        "BetweennessCentrality" : 0.01053774,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Robbie_Fulks",
        "SelfLoops" : 0,
        "SUID" : 286,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 70,
        "AverageShortestPathLength" : 3.70587278,
        "selected" : false,
        "NeighborhoodConnectivity" : 14.9
      },
      "position" : {
        "x" : 1020.2537841796875,
        "y" : 1917.993408203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "285",
        "ClosenessCentrality" : 0.26873306,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.19047619,
        "Radiality" : 0.84882418,
        "Stress" : 1762246,
        "TopologicalCoefficient" : 0.16666667,
        "shared_name" : "Victor_Battista",
        "BetweennessCentrality" : 7.3402E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Victor_Battista",
        "SelfLoops" : 0,
        "SUID" : 285,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.7211648,
        "selected" : false,
        "NeighborhoodConnectivity" : 178.28571429
      },
      "position" : {
        "x" : 417.25653076171875,
        "y" : 1654.0892333984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "284",
        "ClosenessCentrality" : 0.29900769,
        "degree_layout" : 8,
        "Eccentricity" : 10,
        "Degree" : 52,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.10558069,
        "Radiality" : 0.8697558,
        "Stress" : 23609558,
        "TopologicalCoefficient" : 0.0330206,
        "shared_name" : "Roy_M._\"Junior\"_Husky",
        "BetweennessCentrality" : 0.01042226,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Roy_M._\"Junior\"_Husky",
        "SelfLoops" : 0,
        "SUID" : 284,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 52,
        "AverageShortestPathLength" : 3.34439564,
        "selected" : false,
        "NeighborhoodConnectivity" : 76.26923077
      },
      "position" : {
        "x" : 745.4080810546875,
        "y" : 1509.347412109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "283",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Ricky_Baker",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ricky_Baker",
        "SelfLoops" : 0,
        "SUID" : 283,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1972.2265625,
        "y" : 930.6343383789062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "282",
        "ClosenessCentrality" : 0.27468329,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 35,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.06417112,
        "Radiality" : 0.85330242,
        "Stress" : 9844368,
        "TopologicalCoefficient" : 0.03776763,
        "shared_name" : "Herb_Pedersen",
        "BetweennessCentrality" : 0.00568658,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Herb_Pedersen",
        "SelfLoops" : 0,
        "SUID" : 282,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 35,
        "AverageShortestPathLength" : 3.64055637,
        "selected" : false,
        "NeighborhoodConnectivity" : 50.35294118
      },
      "position" : {
        "x" : 322.0438537597656,
        "y" : 1485.1767578125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "281",
        "ClosenessCentrality" : 0.28275069,
        "degree_layout" : 8,
        "Eccentricity" : 11,
        "Degree" : 45,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.09302326,
        "Radiality" : 0.85907308,
        "Stress" : 7843692,
        "TopologicalCoefficient" : 0.03742357,
        "shared_name" : "Paul_Franklin",
        "BetweennessCentrality" : 0.00304025,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Paul_Franklin",
        "SelfLoops" : 0,
        "SUID" : 281,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 45,
        "AverageShortestPathLength" : 3.53668456,
        "selected" : false,
        "NeighborhoodConnectivity" : 68.04545455
      },
      "position" : {
        "x" : 503.2705078125,
        "y" : 1630.877197265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "280",
        "ClosenessCentrality" : 0.26985381,
        "degree_layout" : 19,
        "Eccentricity" : 11,
        "Degree" : 43,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.02657807,
        "Radiality" : 0.84968277,
        "Stress" : 3538066,
        "TopologicalCoefficient" : 0.03283955,
        "shared_name" : "Martina_McBride",
        "BetweennessCentrality" : 0.00186465,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Martina_McBride",
        "SelfLoops" : 0,
        "SUID" : 280,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 43,
        "AverageShortestPathLength" : 3.7057101,
        "selected" : false,
        "NeighborhoodConnectivity" : 29.27906977
      },
      "position" : {
        "x" : 745.9390869140625,
        "y" : 1926.471923828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "279",
        "ClosenessCentrality" : 0.28052481,
        "degree_layout" : 8,
        "Eccentricity" : 10,
        "Degree" : 28,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.16402116,
        "Radiality" : 0.85751405,
        "Stress" : 5460984,
        "TopologicalCoefficient" : 0.05721787,
        "shared_name" : "Eddie_Bayers",
        "BetweennessCentrality" : 0.00177168,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Eddie_Bayers",
        "SelfLoops" : 0,
        "SUID" : 279,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 28,
        "AverageShortestPathLength" : 3.56474703,
        "selected" : false,
        "NeighborhoodConnectivity" : 98.10714286
      },
      "position" : {
        "x" : 597.2392578125,
        "y" : 1630.802490234375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "278",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Barbara_Richardson",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Barbara_Richardson",
        "SelfLoops" : 0,
        "SUID" : 278,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 795.30078125,
        "y" : 120.3163833618164
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "277",
        "ClosenessCentrality" : 0.2993353,
        "degree_layout" : 13,
        "Eccentricity" : 10,
        "Degree" : 105,
        "PartnerOfMultiEdgedNodePairs" : 11,
        "ClusteringCoefficient" : 0.08053077,
        "Radiality" : 0.86995915,
        "Stress" : 25838716,
        "TopologicalCoefficient" : 0.02059408,
        "shared_name" : "Tony_Rice",
        "BetweennessCentrality" : 0.01179759,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tony_Rice",
        "SelfLoops" : 0,
        "SUID" : 277,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 105,
        "AverageShortestPathLength" : 3.34073532,
        "selected" : false,
        "NeighborhoodConnectivity" : 44.41489362
      },
      "position" : {
        "x" : 799.937744140625,
        "y" : 1446.019287109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "276",
        "ClosenessCentrality" : 0.278586,
        "degree_layout" : 14,
        "Eccentricity" : 11,
        "Degree" : 25,
        "PartnerOfMultiEdgedNodePairs" : 3,
        "ClusteringCoefficient" : 0.29437229,
        "Radiality" : 0.85613578,
        "Stress" : 1959332,
        "TopologicalCoefficient" : 0.07281363,
        "shared_name" : "Jim_Mills",
        "BetweennessCentrality" : 6.718E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jim_Mills",
        "SelfLoops" : 0,
        "SUID" : 276,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 25,
        "AverageShortestPathLength" : 3.58955588,
        "selected" : false,
        "NeighborhoodConnectivity" : 84.36363636
      },
      "position" : {
        "x" : 809.5380859375,
        "y" : 1300.7679443359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "275",
        "ClosenessCentrality" : 0.24049296,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.2,
        "Radiality" : 0.82454856,
        "Stress" : 1151464,
        "TopologicalCoefficient" : 0.21148936,
        "shared_name" : "Jan_Howard",
        "BetweennessCentrality" : 6.3396E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jan_Howard",
        "SelfLoops" : 0,
        "SUID" : 275,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 4.15812592,
        "selected" : false,
        "NeighborhoodConnectivity" : 99.8
      },
      "position" : {
        "x" : 1109.0240478515625,
        "y" : 1969.693603515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "274",
        "ClosenessCentrality" : 0.24200311,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82599009,
        "Stress" : 43780,
        "TopologicalCoefficient" : 0.28043478,
        "shared_name" : "Becky_Isaacs_Bowman",
        "BetweennessCentrality" : 1.362E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Becky_Isaacs_Bowman",
        "SelfLoops" : 0,
        "SUID" : 274,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.1321783,
        "selected" : false,
        "NeighborhoodConnectivity" : 129.0
      },
      "position" : {
        "x" : 206.8369140625,
        "y" : 1285.7276611328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "273",
        "ClosenessCentrality" : 0.3269333,
        "degree_layout" : 32,
        "Eccentricity" : 10,
        "Degree" : 235,
        "PartnerOfMultiEdgedNodePairs" : 22,
        "ClusteringCoefficient" : 0.03906458,
        "Radiality" : 0.88562623,
        "Stress" : 108264800,
        "TopologicalCoefficient" : 0.01032839,
        "shared_name" : "Jerry_Douglas",
        "BetweennessCentrality" : 0.05925759,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jerry_Douglas",
        "SelfLoops" : 0,
        "SUID" : 273,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 235,
        "AverageShortestPathLength" : 3.05872783,
        "selected" : false,
        "NeighborhoodConnectivity" : 38.88262911
      },
      "position" : {
        "x" : 951.443603515625,
        "y" : 1402.0789794921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "272",
        "ClosenessCentrality" : 0.26402371,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 16,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.1,
        "Radiality" : 0.84513674,
        "Stress" : 2265992,
        "TopologicalCoefficient" : 0.07834225,
        "shared_name" : "Bill_Anderson",
        "BetweennessCentrality" : 0.00119642,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bill_Anderson",
        "SelfLoops" : 0,
        "SUID" : 272,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 16,
        "AverageShortestPathLength" : 3.78753864,
        "selected" : false,
        "NeighborhoodConnectivity" : 73.5
      },
      "position" : {
        "x" : 900.3556518554688,
        "y" : 1910.1064453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "271",
        "ClosenessCentrality" : 0.24225585,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 6,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.8262296,
        "Stress" : 134322,
        "TopologicalCoefficient" : 0.17866667,
        "shared_name" : "Chip_Davis",
        "BetweennessCentrality" : 5.457E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chip_Davis",
        "SelfLoops" : 0,
        "SUID" : 271,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 6,
        "AverageShortestPathLength" : 4.12786725,
        "selected" : false,
        "NeighborhoodConnectivity" : 68.0
      },
      "position" : {
        "x" : 838.9586791992188,
        "y" : -24.19470977783203
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "270",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Jennie_Carey",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jennie_Carey",
        "SelfLoops" : 0,
        "SUID" : 270,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1740.88916015625,
        "y" : 1827.3331298828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "269",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Marc_Campbell",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Marc_Campbell",
        "SelfLoops" : 0,
        "SUID" : 269,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2306.474609375,
        "y" : 761.4569702148438
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "268",
        "ClosenessCentrality" : 0.29356002,
        "degree_layout" : 16,
        "Eccentricity" : 10,
        "Degree" : 75,
        "PartnerOfMultiEdgedNodePairs" : 4,
        "ClusteringCoefficient" : 0.05754527,
        "Radiality" : 0.86630786,
        "Stress" : 16323146,
        "TopologicalCoefficient" : 0.02623585,
        "shared_name" : "Claire_Lynch",
        "BetweennessCentrality" : 0.00822657,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Claire_Lynch",
        "SelfLoops" : 0,
        "SUID" : 268,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 75,
        "AverageShortestPathLength" : 3.40645844,
        "selected" : false,
        "NeighborhoodConnectivity" : 43.08450704
      },
      "position" : {
        "x" : 755.9871826171875,
        "y" : 1362.0330810546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "267",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "David_Blair",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Blair",
        "SelfLoops" : 0,
        "SUID" : 267,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -252.9500732421875,
        "y" : 1310.156494140625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "266",
        "ClosenessCentrality" : 0.23568882,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.81983985,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.52265372,
        "shared_name" : "Kent_Wells",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kent_Wells",
        "SelfLoops" : 0,
        "SUID" : 266,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24288271,
        "selected" : false,
        "NeighborhoodConnectivity" : 161.5
      },
      "position" : {
        "x" : 49.58953094482422,
        "y" : 1092.349365234375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "265",
        "ClosenessCentrality" : 0.26519123,
        "degree_layout" : 3,
        "Eccentricity" : 10,
        "Degree" : 6,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.4,
        "Radiality" : 0.84606312,
        "Stress" : 189338,
        "TopologicalCoefficient" : 0.20038314,
        "shared_name" : "Steve_Turner",
        "BetweennessCentrality" : 3.572E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Steve_Turner",
        "SelfLoops" : 0,
        "SUID" : 265,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 6,
        "AverageShortestPathLength" : 3.77086384,
        "selected" : false,
        "NeighborhoodConnectivity" : 174.33333333
      },
      "position" : {
        "x" : 272.84649658203125,
        "y" : 1363.8587646484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "264",
        "ClosenessCentrality" : 0.26828736,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.14545455,
        "Radiality" : 0.84848074,
        "Stress" : 2374122,
        "TopologicalCoefficient" : 0.11225556,
        "shared_name" : "Dennis_Wilson",
        "BetweennessCentrality" : 0.00114679,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dennis_Wilson",
        "SelfLoops" : 0,
        "SUID" : 264,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.72734667,
        "selected" : false,
        "NeighborhoodConnectivity" : 137.09090909
      },
      "position" : {
        "x" : 461.8138122558594,
        "y" : 1753.75244140625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "263",
        "ClosenessCentrality" : 0.23936916,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82346402,
        "Stress" : 15694,
        "TopologicalCoefficient" : 0.50311526,
        "shared_name" : "David_Richman",
        "BetweennessCentrality" : 1.014E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Richman",
        "SelfLoops" : 0,
        "SUID" : 263,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.17764763,
        "selected" : false,
        "NeighborhoodConnectivity" : 162.5
      },
      "position" : {
        "x" : 1157.78955078125,
        "y" : -49.34634017944336
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "262",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Virginia_Team",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Virginia_Team",
        "SelfLoops" : 0,
        "SUID" : 262,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2176.829345703125,
        "y" : 868.3236083984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "261",
        "ClosenessCentrality" : 0.27351606,
        "degree_layout" : 4,
        "Eccentricity" : 10,
        "Degree" : 13,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.14102564,
        "Radiality" : 0.85243931,
        "Stress" : 1836994,
        "TopologicalCoefficient" : 0.09691439,
        "shared_name" : "Kristin_Wilkinson",
        "BetweennessCentrality" : 5.889E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kristin_Wilkinson",
        "SelfLoops" : 0,
        "SUID" : 261,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 13,
        "AverageShortestPathLength" : 3.6560924,
        "selected" : false,
        "NeighborhoodConnectivity" : 120.38461538
      },
      "position" : {
        "x" : 711.6351318359375,
        "y" : 1861.5853271484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "260",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "David_Sutton",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Sutton",
        "SelfLoops" : 0,
        "SUID" : 260,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 603.8623657226562,
        "y" : -101.27202606201172
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "259",
        "ClosenessCentrality" : 0.24864493,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 9,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.11111111,
        "Radiality" : 0.83212226,
        "Stress" : 790922,
        "TopologicalCoefficient" : 0.12544803,
        "shared_name" : "Joe_Spivey",
        "BetweennessCentrality" : 4.7624E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joe_Spivey",
        "SelfLoops" : 0,
        "SUID" : 259,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 9,
        "AverageShortestPathLength" : 4.02179925,
        "selected" : false,
        "NeighborhoodConnectivity" : 66.33333333
      },
      "position" : {
        "x" : 156.18121337890625,
        "y" : 1812.8642578125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "258",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Beth_Stevens",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Beth_Stevens",
        "SelfLoops" : 0,
        "SUID" : 258,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2259.102783203125,
        "y" : 614.9122314453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "257",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "April_Stevens",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "April_Stevens",
        "SelfLoops" : 0,
        "SUID" : 257,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1033.2381591796875,
        "y" : 346.7269287109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "256",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Rose_Sanico",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rose_Sanico",
        "SelfLoops" : 0,
        "SUID" : 256,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 109.32549285888672,
        "y" : 670.5428466796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "255",
        "ClosenessCentrality" : 0.25334872,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 29,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.04843305,
        "Radiality" : 0.83627063,
        "Stress" : 3811374,
        "TopologicalCoefficient" : 0.04761905,
        "shared_name" : "Darrell_Webb",
        "BetweennessCentrality" : 0.00259536,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Darrell_Webb",
        "SelfLoops" : 0,
        "SUID" : 255,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 29,
        "AverageShortestPathLength" : 3.94712868,
        "selected" : false,
        "NeighborhoodConnectivity" : 27.37037037
      },
      "position" : {
        "x" : 1081.3509521484375,
        "y" : 959.149169921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "254",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Keith_Rogers",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Keith_Rogers",
        "SelfLoops" : 0,
        "SUID" : 254,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2160.281494140625,
        "y" : 729.256103515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "253",
        "ClosenessCentrality" : 0.2401125,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 6,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82418253,
        "Stress" : 4131114,
        "TopologicalCoefficient" : 0.16705069,
        "shared_name" : "Jerry_O'Sullivan",
        "BetweennessCentrality" : 0.00286136,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jerry_O'Sullivan",
        "SelfLoops" : 0,
        "SUID" : 253,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 6,
        "AverageShortestPathLength" : 4.16471449,
        "selected" : false,
        "NeighborhoodConnectivity" : 73.5
      },
      "position" : {
        "x" : 2100.59521484375,
        "y" : 1001.9404907226562
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "252",
        "ClosenessCentrality" : 0.25844563,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.15555556,
        "Radiality" : 0.84059523,
        "Stress" : 3438606,
        "TopologicalCoefficient" : 0.12068966,
        "shared_name" : "Louis_Dean_Nunley",
        "BetweennessCentrality" : 0.00150068,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Louis_Dean_Nunley",
        "SelfLoops" : 0,
        "SUID" : 252,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 3.86928583,
        "selected" : false,
        "NeighborhoodConnectivity" : 119.2
      },
      "position" : {
        "x" : 279.9444885253906,
        "y" : 1768.5062255859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "251",
        "ClosenessCentrality" : 0.24493943,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82874211,
        "Stress" : 31604,
        "TopologicalCoefficient" : 0.53891509,
        "shared_name" : "Cheryl_Riddle",
        "BetweennessCentrality" : 4.69E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Cheryl_Riddle",
        "SelfLoops" : 0,
        "SUID" : 251,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.08264194,
        "selected" : false,
        "NeighborhoodConnectivity" : 229.5
      },
      "position" : {
        "x" : 1983.1009521484375,
        "y" : 2077.650390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "250",
        "ClosenessCentrality" : 0.24332509,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.13333333,
        "Radiality" : 0.82723732,
        "Stress" : 187450,
        "TopologicalCoefficient" : 0.11372998,
        "shared_name" : "Danny_Roberts",
        "BetweennessCentrality" : 8.909E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Danny_Roberts",
        "SelfLoops" : 0,
        "SUID" : 250,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 4.10972832,
        "selected" : false,
        "NeighborhoodConnectivity" : 50.2
      },
      "position" : {
        "x" : 88.98629760742188,
        "y" : 937.3269653320312
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "249",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Arthur_Rice",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Arthur_Rice",
        "SelfLoops" : 0,
        "SUID" : 249,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 744.2650756835938,
        "y" : -142.60076904296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "248",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Jennifer_O'Brien",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jennifer_O'Brien",
        "SelfLoops" : 0,
        "SUID" : 248,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1561.326904296875,
        "y" : 2345.60888671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "247",
        "ClosenessCentrality" : 0.25379851,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.83665926,
        "Stress" : 271422,
        "TopologicalCoefficient" : 0.2924812,
        "shared_name" : "Vicki_Hampton",
        "BetweennessCentrality" : 3.585E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Vicki_Hampton",
        "SelfLoops" : 0,
        "SUID" : 247,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 3.9401334,
        "selected" : false,
        "NeighborhoodConnectivity" : 194.75
      },
      "position" : {
        "x" : 708.358154296875,
        "y" : 924.2905883789062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "246",
        "ClosenessCentrality" : 0.24727463,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 13,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.16666667,
        "Radiality" : 0.83088408,
        "Stress" : 407974,
        "TopologicalCoefficient" : 0.09749921,
        "shared_name" : "Robert_Hale",
        "BetweennessCentrality" : 2.0896E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Robert_Hale",
        "SelfLoops" : 0,
        "SUID" : 246,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 13,
        "AverageShortestPathLength" : 4.04408655,
        "selected" : false,
        "NeighborhoodConnectivity" : 47.46153846
      },
      "position" : {
        "x" : 1004.3226318359375,
        "y" : 866.8196411132812
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "245",
        "ClosenessCentrality" : 0.23527836,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81942863,
        "Stress" : 2819440,
        "TopologicalCoefficient" : 0.5,
        "shared_name" : "Kathy_Stewart",
        "BetweennessCentrality" : 0.00169398,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kathy_Stewart",
        "SelfLoops" : 0,
        "SUID" : 245,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.25028469,
        "selected" : false,
        "NeighborhoodConnectivity" : 149.5
      },
      "position" : {
        "x" : 1850.4083251953125,
        "y" : 2025.9239501953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "244",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Steve_French",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Steve_French",
        "SelfLoops" : 0,
        "SUID" : 244,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2097.883056640625,
        "y" : 605.9800415039062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "243",
        "ClosenessCentrality" : 0.28448455,
        "degree_layout" : 5,
        "Eccentricity" : 10,
        "Degree" : 29,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.07635468,
        "Radiality" : 0.86027059,
        "Stress" : 10155930,
        "TopologicalCoefficient" : 0.048467,
        "shared_name" : "Dan_Dugmore",
        "BetweennessCentrality" : 0.00439163,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dan_Dugmore",
        "SelfLoops" : 0,
        "SUID" : 243,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 29,
        "AverageShortestPathLength" : 3.51512933,
        "selected" : false,
        "NeighborhoodConnectivity" : 85.93103448
      },
      "position" : {
        "x" : 681.5405883789062,
        "y" : 1738.782958984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "242",
        "ClosenessCentrality" : 0.24744385,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.83103772,
        "Stress" : 110554,
        "TopologicalCoefficient" : 0.52531646,
        "shared_name" : "David_Hidalgo",
        "BetweennessCentrality" : 1.893E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Hidalgo",
        "SelfLoops" : 0,
        "SUID" : 242,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.04132097,
        "selected" : false,
        "NeighborhoodConnectivity" : 250.0
      },
      "position" : {
        "x" : -237.7214813232422,
        "y" : 1467.5899658203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "241",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Gary_D._Davis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gary_D._Davis",
        "SelfLoops" : 0,
        "SUID" : 241,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 938.0430908203125,
        "y" : 104.88543701171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "240",
        "ClosenessCentrality" : 0.26362174,
        "degree_layout" : 14,
        "Eccentricity" : 11,
        "Degree" : 37,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.04761905,
        "Radiality" : 0.8448159,
        "Stress" : 4168846,
        "TopologicalCoefficient" : 0.03773722,
        "shared_name" : "The_Grascals",
        "BetweennessCentrality" : 0.00203537,
        "NumberOfUndirectedEdges" : 0,
        "name" : "The_Grascals",
        "SelfLoops" : 0,
        "SUID" : 240,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 37,
        "AverageShortestPathLength" : 3.79331381,
        "selected" : false,
        "NeighborhoodConnectivity" : 29.33333333
      },
      "position" : {
        "x" : 211.265625,
        "y" : 1142.2998046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "239",
        "ClosenessCentrality" : 0.26652503,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 14,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.04395604,
        "Radiality" : 0.84711151,
        "Stress" : 2148452,
        "TopologicalCoefficient" : 0.09195643,
        "shared_name" : "David_Davidson",
        "BetweennessCentrality" : 6.9544E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Davidson",
        "SelfLoops" : 0,
        "SUID" : 239,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 14,
        "AverageShortestPathLength" : 3.75199284,
        "selected" : false,
        "NeighborhoodConnectivity" : 94.64285714
      },
      "position" : {
        "x" : 583.236083984375,
        "y" : 2074.125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "238",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Gary_\"Biscuit\"_Davis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gary_\"Biscuit\"_Davis",
        "SelfLoops" : 0,
        "SUID" : 238,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2188.799560546875,
        "y" : 482.42730712890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "237",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Bambi_Breakstone",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bambi_Breakstone",
        "SelfLoops" : 0,
        "SUID" : 237,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1970.67431640625,
        "y" : 669.357421875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "236",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Blueniques",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Blueniques",
        "SelfLoops" : 0,
        "SUID" : 236,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1022.0853271484375,
        "y" : 584.5768432617188
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "235",
        "ClosenessCentrality" : 0.28213425,
        "degree_layout" : 7,
        "Eccentricity" : 10,
        "Degree" : 41,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.10487805,
        "Radiality" : 0.85864378,
        "Stress" : 6131602,
        "TopologicalCoefficient" : 0.04050523,
        "shared_name" : "Alan_O'Bryant",
        "BetweennessCentrality" : 0.00239341,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Alan_O'Bryant",
        "SelfLoops" : 0,
        "SUID" : 235,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 41,
        "AverageShortestPathLength" : 3.54441191,
        "selected" : false,
        "NeighborhoodConnectivity" : 54.53658537
      },
      "position" : {
        "x" : 944.8883666992188,
        "y" : 1245.864501953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "234",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Eric_Bennett",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Eric_Bennett",
        "SelfLoops" : 0,
        "SUID" : 234,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1820.353515625,
        "y" : 673.1799926757812
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "233",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Robert_Behar",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Robert_Behar",
        "SelfLoops" : 0,
        "SUID" : 233,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 484.26165771484375,
        "y" : 225.2322998046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "232",
        "ClosenessCentrality" : 0.29058334,
        "degree_layout" : 6,
        "Eccentricity" : 10,
        "Degree" : 28,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.13492063,
        "Radiality" : 0.86436925,
        "Stress" : 11560264,
        "TopologicalCoefficient" : 0.05281614,
        "shared_name" : "Roy_Huskey_Jr.",
        "BetweennessCentrality" : 0.00420943,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Roy_Huskey_Jr.",
        "SelfLoops" : 0,
        "SUID" : 232,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 28,
        "AverageShortestPathLength" : 3.44135351,
        "selected" : false,
        "NeighborhoodConnectivity" : 104.96428571
      },
      "position" : {
        "x" : 654.7996215820312,
        "y" : 1571.441162109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "231",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Maxine_Willard_Waters",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Maxine_Willard_Waters",
        "SelfLoops" : 0,
        "SUID" : 231,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 886.5426635742188,
        "y" : -167.5615997314453
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "230",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Julia_Waters",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Julia_Waters",
        "SelfLoops" : 0,
        "SUID" : 230,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2024.391845703125,
        "y" : 490.18231201171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "229",
        "ClosenessCentrality" : 0.31510957,
        "degree_layout" : 38,
        "Eccentricity" : 10,
        "Degree" : 234,
        "PartnerOfMultiEdgedNodePairs" : 17,
        "ClusteringCoefficient" : 0.02713774,
        "Radiality" : 0.87925004,
        "Stress" : 73545764,
        "TopologicalCoefficient" : 0.00988837,
        "shared_name" : "Ricky_Skaggs",
        "BetweennessCentrality" : 0.03844517,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ricky_Skaggs",
        "SelfLoops" : 0,
        "SUID" : 229,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 234,
        "AverageShortestPathLength" : 3.17349927,
        "selected" : false,
        "NeighborhoodConnectivity" : 29.30875576
      },
      "position" : {
        "x" : 644.1136474609375,
        "y" : 1285.360347375809
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "228",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Dorothy_Remsen",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dorothy_Remsen",
        "SelfLoops" : 0,
        "SUID" : 228,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1027.7984619140625,
        "y" : -173.8817901611328
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "227",
        "ClosenessCentrality" : 0.25364666,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 14,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.07692308,
        "Radiality" : 0.83652821,
        "Stress" : 1183904,
        "TopologicalCoefficient" : 0.09016393,
        "shared_name" : "David_Talbot",
        "BetweennessCentrality" : 5.0844E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Talbot",
        "SelfLoops" : 0,
        "SUID" : 227,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 14,
        "AverageShortestPathLength" : 3.94249227,
        "selected" : false,
        "NeighborhoodConnectivity" : 55.5
      },
      "position" : {
        "x" : 492.0165710449219,
        "y" : 1011.5029296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "226",
        "ClosenessCentrality" : 0.25784396,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.19047619,
        "Radiality" : 0.84009363,
        "Stress" : 607870,
        "TopologicalCoefficient" : 0.16492588,
        "shared_name" : "Bill_Payne",
        "BetweennessCentrality" : 2.6602E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bill_Payne",
        "SelfLoops" : 0,
        "SUID" : 226,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.87831463,
        "selected" : false,
        "NeighborhoodConnectivity" : 140.14285714
      },
      "position" : {
        "x" : 348.4800720214844,
        "y" : 1882.080322265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "225",
        "ClosenessCentrality" : 0.25382995,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.04761905,
        "Radiality" : 0.83668637,
        "Stress" : 700250,
        "TopologicalCoefficient" : 0.16723277,
        "shared_name" : "Dean_Parks",
        "BetweennessCentrality" : 3.124E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dean_Parks",
        "SelfLoops" : 0,
        "SUID" : 225,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.93964536,
        "selected" : false,
        "NeighborhoodConnectivity" : 120.28571429
      },
      "position" : {
        "x" : 1264.6689453125,
        "y" : 2444.669677734375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "224",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Stephen_Munns",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Stephen_Munns",
        "SelfLoops" : 0,
        "SUID" : 224,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1170.5618896484375,
        "y" : 627.5787963867188
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "223",
        "ClosenessCentrality" : 0.26287767,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 26,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.10666667,
        "Radiality" : 0.8442194,
        "Stress" : 19507704,
        "TopologicalCoefficient" : 0.05512485,
        "shared_name" : "Andy_Hall",
        "BetweennessCentrality" : 0.00916813,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Andy_Hall",
        "SelfLoops" : 0,
        "SUID" : 223,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 26,
        "AverageShortestPathLength" : 3.80405076,
        "selected" : false,
        "NeighborhoodConnectivity" : 46.48
      },
      "position" : {
        "x" : 272.7417907714844,
        "y" : 1197.4761962890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "222",
        "ClosenessCentrality" : 0.24167961,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82568281,
        "Stress" : 52864,
        "TopologicalCoefficient" : 0.52432432,
        "shared_name" : "Gary_Herbig",
        "BetweennessCentrality" : 1.013E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gary_Herbig",
        "SelfLoops" : 0,
        "SUID" : 222,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.13770945,
        "selected" : false,
        "NeighborhoodConnectivity" : 195.0
      },
      "position" : {
        "x" : 991.0906372070312,
        "y" : -36.425479888916016
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "221",
        "ClosenessCentrality" : 0.23761113,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82174683,
        "Stress" : 83702,
        "TopologicalCoefficient" : 0.52720207,
        "shared_name" : "Roy_Galloway",
        "BetweennessCentrality" : 1.343E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Roy_Galloway",
        "SelfLoops" : 0,
        "SUID" : 221,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.20855702,
        "selected" : false,
        "NeighborhoodConnectivity" : 204.5
      },
      "position" : {
        "x" : 1935.6273193359375,
        "y" : 382.9267272949219
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "220",
        "ClosenessCentrality" : 0.24192691,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82591779,
        "Stress" : 269060,
        "TopologicalCoefficient" : 0.3526936,
        "shared_name" : "Chuck_Findley",
        "BetweennessCentrality" : 8.525E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chuck_Findley",
        "SelfLoops" : 0,
        "SUID" : 220,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.13347975,
        "selected" : false,
        "NeighborhoodConnectivity" : 140.66666667
      },
      "position" : {
        "x" : 1181.3369140625,
        "y" : 333.2477722167969
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "219",
        "ClosenessCentrality" : 0.29098914,
        "degree_layout" : 17,
        "Eccentricity" : 11,
        "Degree" : 61,
        "PartnerOfMultiEdgedNodePairs" : 8,
        "ClusteringCoefficient" : 0.14513788,
        "Radiality" : 0.86463587,
        "Stress" : 14859186,
        "TopologicalCoefficient" : 0.03379122,
        "shared_name" : "Dan_Tyminski",
        "BetweennessCentrality" : 0.00693624,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dan_Tyminski",
        "SelfLoops" : 0,
        "SUID" : 219,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 61,
        "AverageShortestPathLength" : 3.43655442,
        "selected" : false,
        "NeighborhoodConnectivity" : 59.66037736
      },
      "position" : {
        "x" : 943.8706665039062,
        "y" : 1489.73486328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "218",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Earl_Dumler",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Earl_Dumler",
        "SelfLoops" : 0,
        "SUID" : 218,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2018.3597412109375,
        "y" : 254.77169799804688
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "217",
        "ClosenessCentrality" : 0.29857198,
        "degree_layout" : 22,
        "Eccentricity" : 11,
        "Degree" : 82,
        "PartnerOfMultiEdgedNodePairs" : 7,
        "ClusteringCoefficient" : 0.11315315,
        "Radiality" : 0.86948466,
        "Stress" : 19169742,
        "TopologicalCoefficient" : 0.02682731,
        "shared_name" : "Bryan_Sutton",
        "BetweennessCentrality" : 0.00787583,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bryan_Sutton",
        "SelfLoops" : 0,
        "SUID" : 217,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 82,
        "AverageShortestPathLength" : 3.34927607,
        "selected" : false,
        "NeighborhoodConnectivity" : 57.96
      },
      "position" : {
        "x" : 907.84619140625,
        "y" : 1310.9949951171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "216",
        "ClosenessCentrality" : 0.23939713,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82349113,
        "Stress" : 23216,
        "TopologicalCoefficient" : 0.51532033,
        "shared_name" : "Quitman_Dennis",
        "BetweennessCentrality" : 6.1E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Quitman_Dennis",
        "SelfLoops" : 0,
        "SUID" : 216,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.17715959,
        "selected" : false,
        "NeighborhoodConnectivity" : 186.0
      },
      "position" : {
        "x" : 1171.7215576171875,
        "y" : -183.41259765625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "215",
        "ClosenessCentrality" : 0.31881953,
        "degree_layout" : 35,
        "Eccentricity" : 10,
        "Degree" : 199,
        "PartnerOfMultiEdgedNodePairs" : 10,
        "ClusteringCoefficient" : 0.03512327,
        "Radiality" : 0.88130163,
        "Stress" : 74245350,
        "TopologicalCoefficient" : 0.01111111,
        "shared_name" : "Alison_Krauss",
        "BetweennessCentrality" : 0.04101238,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Alison_Krauss",
        "SelfLoops" : 0,
        "SUID" : 215,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 199,
        "AverageShortestPathLength" : 3.13657068,
        "selected" : false,
        "NeighborhoodConnectivity" : 35.41798942
      },
      "position" : {
        "x" : 992.306640625,
        "y" : 1538.55859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "214",
        "ClosenessCentrality" : 0.24237048,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82633805,
        "Stress" : 473770,
        "TopologicalCoefficient" : 0.25709607,
        "shared_name" : "Lenny_Castro",
        "BetweennessCentrality" : 2.1165E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lenny_Castro",
        "SelfLoops" : 0,
        "SUID" : 214,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.12591508,
        "selected" : false,
        "NeighborhoodConnectivity" : 118.0
      },
      "position" : {
        "x" : 1377.8826904296875,
        "y" : 304.20782470703125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "213",
        "ClosenessCentrality" : 0.26147989,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.11111111,
        "Radiality" : 0.84308967,
        "Stress" : 2242716,
        "TopologicalCoefficient" : 0.12476489,
        "shared_name" : "Lisa_Silver",
        "BetweennessCentrality" : 9.6073E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lisa_Silver",
        "SelfLoops" : 0,
        "SUID" : 213,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 3.82438588,
        "selected" : false,
        "NeighborhoodConnectivity" : 119.7
      },
      "position" : {
        "x" : 382.15509033203125,
        "y" : 2114.083984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "212",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Harry_Bluestone",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Harry_Bluestone",
        "SelfLoops" : 0,
        "SUID" : 212,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1094.9771728515625,
        "y" : 76.03094482421875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "211",
        "ClosenessCentrality" : 0.32137816,
        "degree_layout" : 26,
        "Eccentricity" : 10,
        "Degree" : 128,
        "PartnerOfMultiEdgedNodePairs" : 5,
        "ClusteringCoefficient" : 0.08396641,
        "Radiality" : 0.88268894,
        "Stress" : 75105320,
        "TopologicalCoefficient" : 0.01709065,
        "shared_name" : "Stuart_Duncan",
        "BetweennessCentrality" : 0.03584395,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Stuart_Duncan",
        "SelfLoops" : 0,
        "SUID" : 211,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 128,
        "AverageShortestPathLength" : 3.11159915,
        "selected" : false,
        "NeighborhoodConnectivity" : 60.46341463
      },
      "position" : {
        "x" : 864.8557739257812,
        "y" : 1434.1707763671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "210",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Marty_Walsh",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Marty_Walsh",
        "SelfLoops" : 0,
        "SUID" : 210,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1574.0068359375,
        "y" : 584.9981689453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "209",
        "ClosenessCentrality" : 0.28560808,
        "degree_layout" : 8,
        "Eccentricity" : 11,
        "Degree" : 38,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.08819346,
        "Radiality" : 0.86103881,
        "Stress" : 10408896,
        "TopologicalCoefficient" : 0.03977048,
        "shared_name" : "Byron_House",
        "BetweennessCentrality" : 0.00593275,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Byron_House",
        "SelfLoops" : 0,
        "SUID" : 209,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 38,
        "AverageShortestPathLength" : 3.50130145,
        "selected" : false,
        "NeighborhoodConnectivity" : 58.36842105
      },
      "position" : {
        "x" : 1122.8165283203125,
        "y" : 1535.87841796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "208",
        "ClosenessCentrality" : 0.27682331,
        "degree_layout" : 19,
        "Eccentricity" : 11,
        "Degree" : 56,
        "PartnerOfMultiEdgedNodePairs" : 5,
        "ClusteringCoefficient" : 0.12470588,
        "Radiality" : 0.85486597,
        "Stress" : 5545690,
        "TopologicalCoefficient" : 0.03764773,
        "shared_name" : "Ron_Block",
        "BetweennessCentrality" : 0.00241821,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ron_Block",
        "SelfLoops" : 0,
        "SUID" : 208,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 56,
        "AverageShortestPathLength" : 3.61241256,
        "selected" : false,
        "NeighborhoodConnectivity" : 43.90196078
      },
      "position" : {
        "x" : 1029.6795654296875,
        "y" : 1437.3179931640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "207",
        "ClosenessCentrality" : 0.26765071,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 14,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0989011,
        "Radiality" : 0.84798818,
        "Stress" : 4918724,
        "TopologicalCoefficient" : 0.08994102,
        "shared_name" : "Reggie_Young",
        "BetweennessCentrality" : 0.00208411,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Reggie_Young",
        "SelfLoops" : 0,
        "SUID" : 207,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 14,
        "AverageShortestPathLength" : 3.73621279,
        "selected" : false,
        "NeighborhoodConnectivity" : 118.07142857
      },
      "position" : {
        "x" : 341.374755859375,
        "y" : 1643.674560546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "206",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Ian_Underwood",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ian_Underwood",
        "SelfLoops" : 0,
        "SUID" : 206,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1303.908447265625,
        "y" : 557.6784057617188
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "205",
        "ClosenessCentrality" : 0.23902477,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82312962,
        "Stress" : 107734,
        "TopologicalCoefficient" : 0.50382653,
        "shared_name" : "Tom_Tierney",
        "BetweennessCentrality" : 6.53E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tom_Tierney",
        "SelfLoops" : 0,
        "SUID" : 205,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.18366683,
        "selected" : false,
        "NeighborhoodConnectivity" : 198.5
      },
      "position" : {
        "x" : 1842.2210693359375,
        "y" : 280.2544250488281
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "204",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Stephanie_Spruill",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Stephanie_Spruill",
        "SelfLoops" : 0,
        "SUID" : 204,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1139.2509765625,
        "y" : 479.51300048828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "203",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Sid_Sharp",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sid_Sharp",
        "SelfLoops" : 0,
        "SUID" : 203,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2107.92919921875,
        "y" : 362.4357604980469
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "202",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Richard_Schlosser",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Richard_Schlosser",
        "SelfLoops" : 0,
        "SUID" : 202,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1301.40234375,
        "y" : -134.08880615234375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "201",
        "ClosenessCentrality" : 0.25981107,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.31111111,
        "Radiality" : 0.84172496,
        "Stress" : 694232,
        "TopologicalCoefficient" : 0.1289011,
        "shared_name" : "Lee_Ann_Womack",
        "BetweennessCentrality" : 2.6303E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lee_Ann_Womack",
        "SelfLoops" : 0,
        "SUID" : 201,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 3.84895071,
        "selected" : false,
        "NeighborhoodConnectivity" : 117.3
      },
      "position" : {
        "x" : 526.29345703125,
        "y" : 1353.6629638671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "200",
        "ClosenessCentrality" : 0.23832971,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.82245178,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.50791139,
        "shared_name" : "Joey_Scarbury",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joey_Scarbury",
        "SelfLoops" : 0,
        "SUID" : 200,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.1958679,
        "selected" : false,
        "NeighborhoodConnectivity" : 160.5
      },
      "position" : {
        "x" : 4.739853382110596,
        "y" : 1472.3560791015625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "199",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Tom_Saviano",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tom_Saviano",
        "SelfLoops" : 0,
        "SUID" : 199,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1610.9300537109375,
        "y" : 448.0990905761719
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "198",
        "ClosenessCentrality" : 0.26172482,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 16,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.11666667,
        "Radiality" : 0.84328851,
        "Stress" : 1257288,
        "TopologicalCoefficient" : 0.08213351,
        "shared_name" : "Brent_Truitt",
        "BetweennessCentrality" : 6.2616E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Brent_Truitt",
        "SelfLoops" : 0,
        "SUID" : 198,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 16,
        "AverageShortestPathLength" : 3.8208069,
        "selected" : false,
        "NeighborhoodConnectivity" : 63.0
      },
      "position" : {
        "x" : 1227.137451171875,
        "y" : 1255.4466552734375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "197",
        "ClosenessCentrality" : 0.24192691,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82591779,
        "Stress" : 269060,
        "TopologicalCoefficient" : 0.3526936,
        "shared_name" : "William_Frank_\"Bill\"_Reichenbach_Jr.",
        "BetweennessCentrality" : 8.525E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "William_Frank_\"Bill\"_Reichenbach_Jr.",
        "SelfLoops" : 0,
        "SUID" : 197,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.13347975,
        "selected" : false,
        "NeighborhoodConnectivity" : 140.66666667
      },
      "position" : {
        "x" : 1293.114501953125,
        "y" : 425.7303161621094
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "196",
        "ClosenessCentrality" : 0.2739549,
        "degree_layout" : 12,
        "Eccentricity" : 11,
        "Degree" : 26,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.2173913,
        "Radiality" : 0.85276467,
        "Stress" : 1374006,
        "TopologicalCoefficient" : 0.06332183,
        "shared_name" : "Paul_Brewster",
        "BetweennessCentrality" : 4.6518E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Paul_Brewster",
        "SelfLoops" : 0,
        "SUID" : 196,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 26,
        "AverageShortestPathLength" : 3.65023589,
        "selected" : false,
        "NeighborhoodConnectivity" : 64.33333333
      },
      "position" : {
        "x" : 706.0682983398438,
        "y" : 1230.8028564453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "195",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mary_Ellen_Quinn",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mary_Ellen_Quinn",
        "SelfLoops" : 0,
        "SUID" : 195,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1433.959228515625,
        "y" : 619.6489868164062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "194",
        "ClosenessCentrality" : 0.27914264,
        "degree_layout" : 8,
        "Eccentricity" : 11,
        "Degree" : 30,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.24137931,
        "Radiality" : 0.85653345,
        "Stress" : 3608740,
        "TopologicalCoefficient" : 0.05436447,
        "shared_name" : "Barry_Bales",
        "BetweennessCentrality" : 0.00136752,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Barry_Bales",
        "SelfLoops" : 0,
        "SUID" : 194,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 30,
        "AverageShortestPathLength" : 3.58239792,
        "selected" : false,
        "NeighborhoodConnectivity" : 71.03333333
      },
      "position" : {
        "x" : 989.7948608398438,
        "y" : 1331.0198974609375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "193",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mike_Post",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mike_Post",
        "SelfLoops" : 0,
        "SUID" : 193,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1253.4075927734375,
        "y" : 58.642005920410156
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "192",
        "ClosenessCentrality" : 0.28537604,
        "degree_layout" : 17,
        "Eccentricity" : 11,
        "Degree" : 67,
        "PartnerOfMultiEdgedNodePairs" : 7,
        "ClusteringCoefficient" : 0.13841808,
        "Radiality" : 0.86088065,
        "Stress" : 13399160,
        "TopologicalCoefficient" : 0.03299385,
        "shared_name" : "Adam_Steffey",
        "BetweennessCentrality" : 0.00474342,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Adam_Steffey",
        "SelfLoops" : 0,
        "SUID" : 192,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 67,
        "AverageShortestPathLength" : 3.50414837,
        "selected" : false,
        "NeighborhoodConnectivity" : 51.83333333
      },
      "position" : {
        "x" : 878.4718627929688,
        "y" : 1356.9207763671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "191",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Gregg_Perry",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gregg_Perry",
        "SelfLoops" : 0,
        "SUID" : 191,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1884.380859375,
        "y" : 538.3236694335938
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "190",
        "ClosenessCentrality" : 0.29435426,
        "degree_layout" : 12,
        "Eccentricity" : 10,
        "Degree" : 100,
        "PartnerOfMultiEdgedNodePairs" : 8,
        "ClusteringCoefficient" : 0.05040612,
        "Radiality" : 0.8668185,
        "Stress" : 26975630,
        "TopologicalCoefficient" : 0.01860216,
        "shared_name" : "David_Grisman",
        "BetweennessCentrality" : 0.01577859,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Grisman",
        "SelfLoops" : 0,
        "SUID" : 190,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 100,
        "AverageShortestPathLength" : 3.39726696,
        "selected" : false,
        "NeighborhoodConnectivity" : 36.15217391
      },
      "position" : {
        "x" : 714.2610473632812,
        "y" : 1427.9500732421875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "189",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Colleen_Owens",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Colleen_Owens",
        "SelfLoops" : 0,
        "SUID" : 189,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1547.966796875,
        "y" : 729.9381103515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "188",
        "ClosenessCentrality" : 0.29450234,
        "degree_layout" : 27,
        "Eccentricity" : 10,
        "Degree" : 231,
        "PartnerOfMultiEdgedNodePairs" : 7,
        "ClusteringCoefficient" : 0.00836803,
        "Radiality" : 0.8669134,
        "has_nested_network" : false,
        "Stress" : 63701626,
        "TopologicalCoefficient" : 0.00785973,
        "shared_name" : "Willie_Nelson",
        "BetweennessCentrality" : 0.03453664,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Willie_Nelson",
        "SelfLoops" : 0,
        "SUID" : 188,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 231,
        "AverageShortestPathLength" : 3.39555881,
        "selected" : false,
        "NeighborhoodConnectivity" : 14.62946429
      },
      "position" : {
        "x" : 592.6590576171875,
        "y" : 1790.87353515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "187",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Judy_Ogle",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Judy_Ogle",
        "SelfLoops" : 0,
        "SUID" : 187,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 832.1394653320312,
        "y" : 392.83453369140625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "186",
        "ClosenessCentrality" : 0.23743675,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82157511,
        "Stress" : 76962,
        "TopologicalCoefficient" : 0.35013263,
        "shared_name" : "Ron_Oates",
        "BetweennessCentrality" : 2.14E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ron_Oates",
        "SelfLoops" : 0,
        "SUID" : 186,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.21164796,
        "selected" : false,
        "NeighborhoodConnectivity" : 133.0
      },
      "position" : {
        "x" : 1378.125244140625,
        "y" : -3.6249794960021973
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "185",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Joe_McGuffee",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joe_McGuffee",
        "SelfLoops" : 0,
        "SUID" : 185,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1718.849853515625,
        "y" : 562.9015502929688
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "184",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Denise_Maynelli",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Denise_Maynelli",
        "SelfLoops" : 0,
        "SUID" : 184,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1918.81005859375,
        "y" : 156.2503662109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "183",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mary_Malin",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mary_Malin",
        "SelfLoops" : 0,
        "SUID" : 183,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1211.9940185546875,
        "y" : 197.22584533691406
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "182",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Kim_Hutchcroft",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kim_Hutchcroft",
        "SelfLoops" : 0,
        "SUID" : 182,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1691.0950927734375,
        "y" : 316.95648193359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "181",
        "ClosenessCentrality" : 0.24192691,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82591779,
        "Stress" : 269060,
        "TopologicalCoefficient" : 0.3526936,
        "shared_name" : "Jerry_Hey",
        "BetweennessCentrality" : 8.525E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jerry_Hey",
        "SelfLoops" : 0,
        "SUID" : 181,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.13347975,
        "selected" : false,
        "NeighborhoodConnectivity" : 140.66666667
      },
      "position" : {
        "x" : 1528.1932373046875,
        "y" : 53.0157356262207
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "180",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "John_Goux",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "John_Goux",
        "SelfLoops" : 0,
        "SUID" : 180,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1421.5343017578125,
        "y" : 795.4105834960938
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "179",
        "ClosenessCentrality" : 0.23567074,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81982177,
        "Stress" : 99174,
        "TopologicalCoefficient" : 0.50171233,
        "shared_name" : "Joanne_Feltman",
        "BetweennessCentrality" : 5.644E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joanne_Feltman",
        "SelfLoops" : 0,
        "SUID" : 179,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24320807,
        "selected" : false,
        "NeighborhoodConnectivity" : 147.5
      },
      "position" : {
        "x" : 1542.974609375,
        "y" : 321.59625244140625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "178",
        "ClosenessCentrality" : 0.23567074,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81982177,
        "Stress" : 99174,
        "TopologicalCoefficient" : 0.50171233,
        "shared_name" : "Mandana_Eidgah",
        "BetweennessCentrality" : 5.644E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mandana_Eidgah",
        "SelfLoops" : 0,
        "SUID" : 178,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24320807,
        "selected" : false,
        "NeighborhoodConnectivity" : 147.5
      },
      "position" : {
        "x" : 1814.3428955078125,
        "y" : 56.13298034667969
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "177",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Claudia_Depkin",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Claudia_Depkin",
        "SelfLoops" : 0,
        "SUID" : 177,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1582.96875,
        "y" : -73.65091705322266
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "176",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Richard_Dennison",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Richard_Dennison",
        "SelfLoops" : 0,
        "SUID" : 176,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -186.08372497558594,
        "y" : 1678.8336181640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "175",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Frank_DeCaro",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Frank_DeCaro",
        "SelfLoops" : 0,
        "SUID" : 175,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1693.017333984375,
        "y" : 723.592529296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "174",
        "ClosenessCentrality" : 0.32828647,
        "degree_layout" : 41,
        "Eccentricity" : 10,
        "Degree" : 444,
        "PartnerOfMultiEdgedNodePairs" : 24,
        "ClusteringCoefficient" : 0.01061484,
        "Radiality" : 0.88632666,
        "Stress" : 184796958,
        "TopologicalCoefficient" : 0.00527743,
        "shared_name" : "Emmylou_Harris",
        "BetweennessCentrality" : 0.11074285,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Emmylou_Harris",
        "SelfLoops" : 0,
        "SUID" : 174,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 444,
        "AverageShortestPathLength" : 3.04612006,
        "selected" : false,
        "NeighborhoodConnectivity" : 18.5952381
      },
      "position" : {
        "x" : 694.6902465820312,
        "y" : 1658.3226318359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "173",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "George_Corsillo",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "George_Corsillo",
        "SelfLoops" : 0,
        "SUID" : 173,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1684.060791015625,
        "y" : 19.28046226501465
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "172",
        "ClosenessCentrality" : 0.29484843,
        "degree_layout" : 35,
        "Eccentricity" : 10,
        "Degree" : 178,
        "PartnerOfMultiEdgedNodePairs" : 8,
        "ClusteringCoefficient" : 0.02408632,
        "Radiality" : 0.86713483,
        "Stress" : 52945114,
        "TopologicalCoefficient" : 0.01339741,
        "shared_name" : "Rhonda_Vincent",
        "BetweennessCentrality" : 0.02305921,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rhonda_Vincent",
        "SelfLoops" : 0,
        "SUID" : 172,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 178,
        "AverageShortestPathLength" : 3.39157313,
        "selected" : false,
        "NeighborhoodConnectivity" : 24.30588235
      },
      "position" : {
        "x" : 607.48193359375,
        "y" : 1425.5963134765625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "171",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Leonard_Castro",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Leonard_Castro",
        "SelfLoops" : 0,
        "SUID" : 171,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1302.369384765625,
        "y" : 710.1065673828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "170",
        "ClosenessCentrality" : 0.27749814,
        "degree_layout" : 11,
        "Eccentricity" : 11,
        "Degree" : 40,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.06970128,
        "Radiality" : 0.85535401,
        "Stress" : 10174914,
        "TopologicalCoefficient" : 0.03605001,
        "shared_name" : "Sara_Watkins",
        "BetweennessCentrality" : 0.0064114,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sara_Watkins",
        "SelfLoops" : 0,
        "SUID" : 170,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 40,
        "AverageShortestPathLength" : 3.60362779,
        "selected" : false,
        "NeighborhoodConnectivity" : 40.10526316
      },
      "position" : {
        "x" : 1078.9080810546875,
        "y" : 1381.9949951171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "169",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Chris_Willis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chris_Willis",
        "SelfLoops" : 0,
        "SUID" : 169,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1420.429931640625,
        "y" : 2397.8291015625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "168",
        "ClosenessCentrality" : 0.23535493,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81950545,
        "Stress" : 64578,
        "TopologicalCoefficient" : 0.5,
        "shared_name" : "Larry_Carlton",
        "BetweennessCentrality" : 5.592E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Larry_Carlton",
        "SelfLoops" : 0,
        "SUID" : 168,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.2489019,
        "selected" : false,
        "NeighborhoodConnectivity" : 147.5
      },
      "position" : {
        "x" : 1452.8408203125,
        "y" : 462.5598449707031
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "167",
        "ClosenessCentrality" : 0.27296343,
        "degree_layout" : 3,
        "Eccentricity" : 10,
        "Degree" : 15,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.12380952,
        "Radiality" : 0.85202809,
        "Stress" : 2607568,
        "TopologicalCoefficient" : 0.08339041,
        "shared_name" : "Pat_McInerney",
        "BetweennessCentrality" : 0.00112354,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Pat_McInerney",
        "SelfLoops" : 0,
        "SUID" : 167,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 15,
        "AverageShortestPathLength" : 3.66349439,
        "selected" : false,
        "NeighborhoodConnectivity" : 97.8
      },
      "position" : {
        "x" : 521.3291625976562,
        "y" : 1529.5897216796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "166",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Duane_Starling",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Duane_Starling",
        "SelfLoops" : 0,
        "SUID" : 166,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1264.3641357421875,
        "y" : 2316.1025390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "165",
        "ClosenessCentrality" : 0.24047885,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.824535,
        "Stress" : 340028,
        "TopologicalCoefficient" : 0.33569182,
        "shared_name" : "Jeff_Baxter",
        "BetweennessCentrality" : 2.6669E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jeff_Baxter",
        "SelfLoops" : 0,
        "SUID" : 165,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.15836994,
        "selected" : false,
        "NeighborhoodConnectivity" : 143.33333333
      },
      "position" : {
        "x" : 1596.5894775390625,
        "y" : 177.91864013671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "164",
        "ClosenessCentrality" : 0.27411371,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 20,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.30526316,
        "Radiality" : 0.85288216,
        "Stress" : 2989380,
        "TopologicalCoefficient" : 0.07452203,
        "shared_name" : "Rob_McCoury",
        "BetweennessCentrality" : 0.00114307,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rob_McCoury",
        "SelfLoops" : 0,
        "SUID" : 164,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 20,
        "AverageShortestPathLength" : 3.64812103,
        "selected" : false,
        "NeighborhoodConnectivity" : 89.65
      },
      "position" : {
        "x" : 584.7067260742188,
        "y" : 1191.821533203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "163",
        "ClosenessCentrality" : 0.29388282,
        "degree_layout" : 10,
        "Eccentricity" : 11,
        "Degree" : 66,
        "PartnerOfMultiEdgedNodePairs" : 3,
        "ClusteringCoefficient" : 0.08704557,
        "Radiality" : 0.86651573,
        "Stress" : 12297816,
        "TopologicalCoefficient" : 0.02710336,
        "shared_name" : "Ronnie_McCoury",
        "BetweennessCentrality" : 0.00615677,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ronnie_McCoury",
        "SelfLoops" : 0,
        "SUID" : 163,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 66,
        "AverageShortestPathLength" : 3.40271677,
        "selected" : false,
        "NeighborhoodConnectivity" : 51.17460317
      },
      "position" : {
        "x" : 763.9291381835938,
        "y" : 1251.12060546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "162",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Anita_Ball",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Anita_Ball",
        "SelfLoops" : 0,
        "SUID" : 162,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1782.2169189453125,
        "y" : 430.5060729980469
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "161",
        "ClosenessCentrality" : 0.30563842,
        "degree_layout" : 296,
        "Eccentricity" : 10,
        "Degree" : 296,
        "PartnerOfMultiEdgedNodePairs" : 9,
        "ClusteringCoefficient" : 0.00894228,
        "Radiality" : 0.87378667,
        "Stress" : 88124980,
        "TopologicalCoefficient" : 0.00773739,
        "shared_name" : "Dolly Parton",
        "BetweennessCentrality" : 0.04684796,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dolly Parton",
        "SelfLoops" : 0,
        "SUID" : 161,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 296,
        "AverageShortestPathLength" : 3.27183992,
        "selected" : false,
        "NeighborhoodConnectivity" : 16.19860627
      },
      "position" : {
        "x" : 1047.98388671875,
        "y" : 1133.7042236328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "160",
        "ClosenessCentrality" : 0.25815257,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.3,
        "Radiality" : 0.84035121,
        "Stress" : 397362,
        "TopologicalCoefficient" : 0.23007519,
        "shared_name" : "Gary_VanOsdale",
        "BetweennessCentrality" : 6.488E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gary_VanOsdale",
        "SelfLoops" : 0,
        "SUID" : 160,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 3.87367822,
        "selected" : false,
        "NeighborhoodConnectivity" : 183.8
      },
      "position" : {
        "x" : 714.8339233398438,
        "y" : 2028.8955078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "159",
        "ClosenessCentrality" : 0.26478,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 34,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.06060606,
        "Radiality" : 0.84573776,
        "Stress" : 4548360,
        "TopologicalCoefficient" : 0.04514181,
        "shared_name" : "Sonya_Isaacs",
        "BetweennessCentrality" : 0.00200551,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sonya_Isaacs",
        "SelfLoops" : 0,
        "SUID" : 159,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 34,
        "AverageShortestPathLength" : 3.77672035,
        "selected" : false,
        "NeighborhoodConnectivity" : 50.88235294
      },
      "position" : {
        "x" : 139.4650421142578,
        "y" : 1367.5008544921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "158",
        "ClosenessCentrality" : 0.24943191,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.83282721,
        "Stress" : 88070,
        "TopologicalCoefficient" : 0.38139825,
        "shared_name" : "Chris_Rodriguez",
        "BetweennessCentrality" : 1.212E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chris_Rodriguez",
        "SelfLoops" : 0,
        "SUID" : 158,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.00911014,
        "selected" : false,
        "NeighborhoodConnectivity" : 204.0
      },
      "position" : {
        "x" : 1405.085693359375,
        "y" : 2263.714111328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "157",
        "ClosenessCentrality" : 0.24595379,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.1,
        "Radiality" : 0.82967753,
        "Stress" : 202642,
        "TopologicalCoefficient" : 0.22746988,
        "shared_name" : "Mary_Kathryn_Van_Osdale",
        "BetweennessCentrality" : 5.107E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mary_Kathryn_Van_Osdale",
        "SelfLoops" : 0,
        "SUID" : 157,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 4.06580446,
        "selected" : false,
        "NeighborhoodConnectivity" : 95.0
      },
      "position" : {
        "x" : 659.4285888671875,
        "y" : 2251.132080078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "156",
        "ClosenessCentrality" : 0.26908597,
        "degree_layout" : 9,
        "Eccentricity" : 11,
        "Degree" : 25,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.13438735,
        "Radiality" : 0.84909531,
        "Stress" : 3837272,
        "TopologicalCoefficient" : 0.05889961,
        "shared_name" : "Sean_Watkins",
        "BetweennessCentrality" : 0.00239733,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sean_Watkins",
        "SelfLoops" : 0,
        "SUID" : 156,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 25,
        "AverageShortestPathLength" : 3.71628437,
        "selected" : false,
        "NeighborhoodConnectivity" : 51.82608696
      },
      "position" : {
        "x" : 1170.2044677734375,
        "y" : 1376.06591796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "155",
        "ClosenessCentrality" : 0.2393645,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.8234595,
        "Stress" : 73902,
        "TopologicalCoefficient" : 0.36119116,
        "shared_name" : "Catherine_Umstead",
        "BetweennessCentrality" : 1.501E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Catherine_Umstead",
        "SelfLoops" : 0,
        "SUID" : 155,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.17772897,
        "selected" : false,
        "NeighborhoodConnectivity" : 125.66666667
      },
      "position" : {
        "x" : 756.9793090820312,
        "y" : 2268.26123046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "154",
        "ClosenessCentrality" : 0.25820137,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 5,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.3,
        "Radiality" : 0.84039188,
        "Stress" : 279694,
        "TopologicalCoefficient" : 0.23534362,
        "shared_name" : "Don_Potter",
        "BetweennessCentrality" : 5.984E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Don_Potter",
        "SelfLoops" : 0,
        "SUID" : 154,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 5,
        "AverageShortestPathLength" : 3.87294615,
        "selected" : false,
        "NeighborhoodConnectivity" : 167.8
      },
      "position" : {
        "x" : 238.7641143798828,
        "y" : 1978.5191650390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "153",
        "ClosenessCentrality" : 0.27379627,
        "degree_layout" : 3,
        "Eccentricity" : 10,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.32727273,
        "Radiality" : 0.85264718,
        "Stress" : 1380006,
        "TopologicalCoefficient" : 0.11640526,
        "shared_name" : "Jim_Keltner",
        "BetweennessCentrality" : 5.4174E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jim_Keltner",
        "SelfLoops" : 0,
        "SUID" : 153,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.65235074,
        "selected" : false,
        "NeighborhoodConnectivity" : 142.36363636
      },
      "position" : {
        "x" : 1086.190185546875,
        "y" : 1783.6275634765625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "152",
        "ClosenessCentrality" : 0.28274419,
        "degree_layout" : 14,
        "Eccentricity" : 11,
        "Degree" : 110,
        "PartnerOfMultiEdgedNodePairs" : 9,
        "ClusteringCoefficient" : 0.04019802,
        "Radiality" : 0.85906856,
        "Stress" : 30153342,
        "TopologicalCoefficient" : 0.01848225,
        "shared_name" : "Randy_Kohrs",
        "BetweennessCentrality" : 0.01492233,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Randy_Kohrs",
        "SelfLoops" : 0,
        "SUID" : 152,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 110,
        "AverageShortestPathLength" : 3.5367659,
        "selected" : false,
        "NeighborhoodConnectivity" : 30.3960396
      },
      "position" : {
        "x" : 877.3097534179688,
        "y" : 1218.2652587890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "151",
        "ClosenessCentrality" : 0.24153716,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82554724,
        "Stress" : 103612,
        "TopologicalCoefficient" : 0.27261614,
        "shared_name" : "Alan_Umstead",
        "BetweennessCentrality" : 2.979E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Alan_Umstead",
        "SelfLoops" : 0,
        "SUID" : 151,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.14014967,
        "selected" : false,
        "NeighborhoodConnectivity" : 111.75
      },
      "position" : {
        "x" : 981.64697265625,
        "y" : 2106.5439453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "150",
        "ClosenessCentrality" : 0.23650494,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82065325,
        "Stress" : 34042,
        "TopologicalCoefficient" : 0.50615385,
        "shared_name" : "John_Popper",
        "BetweennessCentrality" : 1.654E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "John_Popper",
        "SelfLoops" : 0,
        "SUID" : 150,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.22824142,
        "selected" : false,
        "NeighborhoodConnectivity" : 165.5
      },
      "position" : {
        "x" : 1610.203125,
        "y" : 1891.393310546875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "149",
        "ClosenessCentrality" : 0.27273937,
        "degree_layout" : 7,
        "Eccentricity" : 10,
        "Degree" : 15,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.17142857,
        "Radiality" : 0.85186089,
        "Stress" : 2052346,
        "TopologicalCoefficient" : 0.09162183,
        "shared_name" : "Pamela_Sixfin",
        "BetweennessCentrality" : 5.4764E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Pamela_Sixfin",
        "SelfLoops" : 0,
        "SUID" : 149,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 15,
        "AverageShortestPathLength" : 3.66650399,
        "selected" : false,
        "NeighborhoodConnectivity" : 119.33333333
      },
      "position" : {
        "x" : 781.4266967773438,
        "y" : 1777.1761474609375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "148",
        "ClosenessCentrality" : 0.2472249,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 6,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.06666667,
        "Radiality" : 0.83083889,
        "Stress" : 426512,
        "TopologicalCoefficient" : 0.17804323,
        "shared_name" : "Farrell_Morris",
        "BetweennessCentrality" : 2.3397E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Farrell_Morris",
        "SelfLoops" : 0,
        "SUID" : 148,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 6,
        "AverageShortestPathLength" : 4.04489995,
        "selected" : false,
        "NeighborhoodConnectivity" : 105.0
      },
      "position" : {
        "x" : 973.0231323242188,
        "y" : 2469.912841796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "147",
        "ClosenessCentrality" : 0.25086724,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.14285714,
        "Radiality" : 0.83410155,
        "Stress" : 3361872,
        "TopologicalCoefficient" : 0.16057797,
        "shared_name" : "Bob_Carlin",
        "BetweennessCentrality" : 0.00141386,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bob_Carlin",
        "SelfLoops" : 0,
        "SUID" : 147,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.98617212,
        "selected" : false,
        "NeighborhoodConnectivity" : 84.28571429
      },
      "position" : {
        "x" : 1055.7626953125,
        "y" : 208.06768798828125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "146",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Darci_Monet",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Darci_Monet",
        "SelfLoops" : 0,
        "SUID" : 146,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1573.463623046875,
        "y" : 2039.5712890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "145",
        "ClosenessCentrality" : 0.26633449,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 8,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.28571429,
        "Radiality" : 0.84696238,
        "Stress" : 359264,
        "TopologicalCoefficient" : 0.14783917,
        "shared_name" : "Suzanne_Cox",
        "BetweennessCentrality" : 1.1523E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Suzanne_Cox",
        "SelfLoops" : 0,
        "SUID" : 145,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 8,
        "AverageShortestPathLength" : 3.75467708,
        "selected" : false,
        "NeighborhoodConnectivity" : 135.375
      },
      "position" : {
        "x" : 1090.8443603515625,
        "y" : 1662.4979248046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "144",
        "ClosenessCentrality" : 0.25640277,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.06666667,
        "Radiality" : 0.83888256,
        "Stress" : 954228,
        "TopologicalCoefficient" : 0.11925134,
        "shared_name" : "Liana_Manis",
        "BetweennessCentrality" : 3.5259E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Liana_Manis",
        "SelfLoops" : 0,
        "SUID" : 144,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 3.90011388,
        "selected" : false,
        "NeighborhoodConnectivity" : 89.6
      },
      "position" : {
        "x" : 59.7097282409668,
        "y" : 1588.1427001953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "143",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Andy_Landis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Andy_Landis",
        "SelfLoops" : 0,
        "SUID" : 143,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1122.0146484375,
        "y" : 2426.5
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "142",
        "ClosenessCentrality" : 0.259493,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 16,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.06666667,
        "Radiality" : 0.84146286,
        "Stress" : 2685700,
        "TopologicalCoefficient" : 0.07207207,
        "shared_name" : "Raul_Malo",
        "BetweennessCentrality" : 0.00168135,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Raul_Malo",
        "SelfLoops" : 0,
        "SUID" : 142,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 16,
        "AverageShortestPathLength" : 3.85366846,
        "selected" : false,
        "NeighborhoodConnectivity" : 48.4375
      },
      "position" : {
        "x" : 1391.9454345703125,
        "y" : 227.462158203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "141",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Ladysmith_Black_Mambazo",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ladysmith_Black_Mambazo",
        "SelfLoops" : 0,
        "SUID" : 141,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1591.4256591796875,
        "y" : 2211.998046875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "140",
        "ClosenessCentrality" : 0.25653117,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.38095238,
        "Radiality" : 0.83899102,
        "Stress" : 199722,
        "TopologicalCoefficient" : 0.18136311,
        "shared_name" : "Rebecca_Lynn_Howard",
        "BetweennessCentrality" : 5.337E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Rebecca_Lynn_Howard",
        "SelfLoops" : 0,
        "SUID" : 140,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.8981617,
        "selected" : false,
        "NeighborhoodConnectivity" : 134.71428571
      },
      "position" : {
        "x" : 461.9571228027344,
        "y" : 1282.1700439453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "139",
        "ClosenessCentrality" : 0.26705187,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 13,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.08974359,
        "Radiality" : 0.84752273,
        "Stress" : 1905704,
        "TopologicalCoefficient" : 0.09517601,
        "shared_name" : "John_Mock",
        "BetweennessCentrality" : 8.8894E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "John_Mock",
        "SelfLoops" : 0,
        "SUID" : 139,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 13,
        "AverageShortestPathLength" : 3.74459086,
        "selected" : false,
        "NeighborhoodConnectivity" : 73.46153846
      },
      "position" : {
        "x" : 1879.8289794921875,
        "y" : 801.7576293945312
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "138",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Steve_Dorff",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Steve_Dorff",
        "SelfLoops" : 0,
        "SUID" : 138,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1722.5726318359375,
        "y" : 1980.4820556640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "137",
        "ClosenessCentrality" : 0.27238889,
        "degree_layout" : 9,
        "Eccentricity" : 10,
        "Degree" : 34,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0855615,
        "Radiality" : 0.85159879,
        "Stress" : 2848138,
        "TopologicalCoefficient" : 0.04019608,
        "shared_name" : "Solomon_Burke",
        "BetweennessCentrality" : 0.00171506,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Solomon_Burke",
        "SelfLoops" : 0,
        "SUID" : 137,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 34,
        "AverageShortestPathLength" : 3.67122173,
        "selected" : false,
        "NeighborhoodConnectivity" : 41.23529412
      },
      "position" : {
        "x" : 996.4378051757812,
        "y" : 1784.032958984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "136",
        "ClosenessCentrality" : 0.26848657,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.31111111,
        "Radiality" : 0.84863438,
        "Stress" : 1096126,
        "TopologicalCoefficient" : 0.12933458,
        "shared_name" : "Jim_Horn",
        "BetweennessCentrality" : 4.6227E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jim_Horn",
        "SelfLoops" : 0,
        "SUID" : 136,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 3.7245811,
        "selected" : false,
        "NeighborhoodConnectivity" : 138.2
      },
      "position" : {
        "x" : 425.8038635253906,
        "y" : 1938.882568359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "135",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Kim_Carnes",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kim_Carnes",
        "SelfLoops" : 0,
        "SUID" : 135,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -251.35427856445312,
        "y" : 1018.933837890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "134",
        "ClosenessCentrality" : 0.27746682,
        "degree_layout" : 6,
        "Eccentricity" : 11,
        "Degree" : 23,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.18181818,
        "Radiality" : 0.85533142,
        "Stress" : 3317116,
        "TopologicalCoefficient" : 0.06466093,
        "shared_name" : "Viktor_Krauss",
        "BetweennessCentrality" : 0.00145369,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Viktor_Krauss",
        "SelfLoops" : 0,
        "SUID" : 134,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 23,
        "AverageShortestPathLength" : 3.60403449,
        "selected" : false,
        "NeighborhoodConnectivity" : 75.95652174
      },
      "position" : {
        "x" : 1081.7176513671875,
        "y" : 1589.6859130859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "133",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Crystal_Bernard",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Crystal_Bernard",
        "SelfLoops" : 0,
        "SUID" : 133,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1868.4454345703125,
        "y" : 1859.0858154296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "132",
        "ClosenessCentrality" : 0.2638707,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 9,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.13888889,
        "Radiality" : 0.84501473,
        "Stress" : 569520,
        "TopologicalCoefficient" : 0.13435504,
        "shared_name" : "Jim_Grosjean",
        "BetweennessCentrality" : 1.5021E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jim_Grosjean",
        "SelfLoops" : 0,
        "SUID" : 132,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 9,
        "AverageShortestPathLength" : 3.78973483,
        "selected" : false,
        "NeighborhoodConnectivity" : 117.22222222
      },
      "position" : {
        "x" : 837.1190185546875,
        "y" : 1967.77783203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "131",
        "ClosenessCentrality" : 0.2426623,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.8266137,
        "Stress" : 53252,
        "TopologicalCoefficient" : 0.27332536,
        "shared_name" : "Matraca_Berg",
        "BetweennessCentrality" : 2.342E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Matraca_Berg",
        "SelfLoops" : 0,
        "SUID" : 131,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.12095331,
        "selected" : false,
        "NeighborhoodConnectivity" : 114.5
      },
      "position" : {
        "x" : 225.90969848632812,
        "y" : 1601.5948486328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "130",
        "ClosenessCentrality" : 0.25935615,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.09090909,
        "Radiality" : 0.84134989,
        "Stress" : 1115028,
        "TopologicalCoefficient" : 0.11710703,
        "shared_name" : "Mark_Casstevens",
        "BetweennessCentrality" : 3.7491E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mark_Casstevens",
        "SelfLoops" : 0,
        "SUID" : 130,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.85570197,
        "selected" : false,
        "NeighborhoodConnectivity" : 95.45454545
      },
      "position" : {
        "x" : 175.14532470703125,
        "y" : 1466.531982421875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "129",
        "ClosenessCentrality" : 0.25175083,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.66666667,
        "Radiality" : 0.8348788,
        "Stress" : 31604,
        "TopologicalCoefficient" : 0.38584475,
        "shared_name" : "Bob_Bailey",
        "BetweennessCentrality" : 4.69E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Bob_Bailey",
        "SelfLoops" : 0,
        "SUID" : 129,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 3.97218155,
        "selected" : false,
        "NeighborhoodConnectivity" : 225.33333333
      },
      "position" : {
        "x" : 560.7105102539062,
        "y" : 889.3496704101562
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "128",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Yusuf",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Yusuf",
        "SelfLoops" : 0,
        "SUID" : 128,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -268.0191650390625,
        "y" : 1163.43505859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "127",
        "ClosenessCentrality" : 0.28183673,
        "degree_layout" : 6,
        "Eccentricity" : 10,
        "Degree" : 40,
        "PartnerOfMultiEdgedNodePairs" : 2,
        "ClusteringCoefficient" : 0.10099573,
        "Radiality" : 0.85843591,
        "Stress" : 8499348,
        "TopologicalCoefficient" : 0.03975364,
        "shared_name" : "Harry_Stinson",
        "BetweennessCentrality" : 0.00421298,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Harry_Stinson",
        "SelfLoops" : 0,
        "SUID" : 127,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 40,
        "AverageShortestPathLength" : 3.54815357,
        "selected" : false,
        "NeighborhoodConnectivity" : 67.5
      },
      "position" : {
        "x" : 828.1111450195312,
        "y" : 1727.3006591796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "126",
        "ClosenessCentrality" : 0.26072019,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 8,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.14285714,
        "Radiality" : 0.84247058,
        "Stress" : 710348,
        "TopologicalCoefficient" : 0.14932591,
        "shared_name" : "Connie_Ellisor",
        "BetweennessCentrality" : 1.6496E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Connie_Ellisor",
        "SelfLoops" : 0,
        "SUID" : 126,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 8,
        "AverageShortestPathLength" : 3.83552953,
        "selected" : false,
        "NeighborhoodConnectivity" : 127.75
      },
      "position" : {
        "x" : 822.2357788085938,
        "y" : 1897.6553955078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "125",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Anna_Wilson",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Anna_Wilson",
        "SelfLoops" : 0,
        "SUID" : 125,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1710.75,
        "y" : 2123.9443359375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "124",
        "ClosenessCentrality" : 0.24383181,
        "degree_layout" : 1,
        "Eccentricity" : 10,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.8277118,
        "Stress" : 408792,
        "TopologicalCoefficient" : 0.25414692,
        "shared_name" : "John_Sebastian",
        "BetweennessCentrality" : 2.5629E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "John_Sebastian",
        "SelfLoops" : 0,
        "SUID" : 124,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.10118757,
        "selected" : false,
        "NeighborhoodConnectivity" : 108.25
      },
      "position" : {
        "x" : 1474.6407470703125,
        "y" : 2146.2265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "123",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Emily_Webb",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Emily_Webb",
        "SelfLoops" : 0,
        "SUID" : 123,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -137.53890991210938,
        "y" : 840.8887939453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "122",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Doctor_Ming_Wang",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Doctor_Ming_Wang",
        "SelfLoops" : 0,
        "SUID" : 122,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1849.485595703125,
        "y" : 2177.903564453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "121",
        "ClosenessCentrality" : 0.25571479,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 6,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.26666667,
        "Radiality" : 0.83829962,
        "Stress" : 465314,
        "TopologicalCoefficient" : 0.21130952,
        "shared_name" : "Billy_Joe_Walker,_Jr.",
        "BetweennessCentrality" : 8.624E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Billy_Joe_Walker,_Jr.",
        "SelfLoops" : 0,
        "SUID" : 121,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 6,
        "AverageShortestPathLength" : 3.9106068,
        "selected" : false,
        "NeighborhoodConnectivity" : 154.16666667
      },
      "position" : {
        "x" : -257.7043762207031,
        "y" : 873.73681640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "120",
        "ClosenessCentrality" : 0.2704177,
        "degree_layout" : 9,
        "Eccentricity" : 11,
        "Degree" : 51,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.01795918,
        "Radiality" : 0.85011207,
        "Stress" : 6366680,
        "TopologicalCoefficient" : 0.02718833,
        "shared_name" : "Porter_Wagoner",
        "BetweennessCentrality" : 0.0039481,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Porter_Wagoner",
        "SelfLoops" : 0,
        "SUID" : 120,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 51,
        "AverageShortestPathLength" : 3.69798276,
        "selected" : false,
        "NeighborhoodConnectivity" : 21.14
      },
      "position" : {
        "x" : 990.7364501953125,
        "y" : 1671.14794921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "119",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Sasha_Vosk",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sasha_Vosk",
        "SelfLoops" : 0,
        "SUID" : 119,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1675.99755859375,
        "y" : 1709.9310302734375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "118",
        "ClosenessCentrality" : 0.24303167,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82696166,
        "Stress" : 21174,
        "TopologicalCoefficient" : 0.36228656,
        "shared_name" : "Keith_Urban",
        "BetweennessCentrality" : 8.19E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Keith_Urban",
        "SelfLoops" : 0,
        "SUID" : 118,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.11469009,
        "selected" : false,
        "NeighborhoodConnectivity" : 163.0
      },
      "position" : {
        "x" : 404.30224609375,
        "y" : 990.0042114257812
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "117",
        "ClosenessCentrality" : 0.2354586,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81960938,
        "Stress" : 14708,
        "TopologicalCoefficient" : 0.51125402,
        "shared_name" : "Ilya_Toshinsky",
        "BetweennessCentrality" : 4.85E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ilya_Toshinsky",
        "SelfLoops" : 0,
        "SUID" : 117,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24703107,
        "selected" : false,
        "NeighborhoodConnectivity" : 160.0
      },
      "position" : {
        "x" : -40.18207550048828,
        "y" : 729.5384521484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "116",
        "ClosenessCentrality" : 0.24710068,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.83072592,
        "Stress" : 403064,
        "TopologicalCoefficient" : 0.35050742,
        "shared_name" : "Pam_Tillis",
        "BetweennessCentrality" : 1.8396E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Pam_Tillis",
        "SelfLoops" : 0,
        "SUID" : 116,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.04693346,
        "selected" : false,
        "NeighborhoodConnectivity" : 150.66666667
      },
      "position" : {
        "x" : 1984.2999267578125,
        "y" : 1939.2005615234375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "115",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Chuck_Tilley",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Chuck_Tilley",
        "SelfLoops" : 0,
        "SUID" : 115,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -193.59561157226562,
        "y" : 703.5317993164062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "114",
        "ClosenessCentrality" : 0.2674411,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.30909091,
        "Radiality" : 0.8478255,
        "Stress" : 718920,
        "TopologicalCoefficient" : 0.11466423,
        "shared_name" : "Gary_Paczosa",
        "BetweennessCentrality" : 2.2729E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Gary_Paczosa",
        "SelfLoops" : 0,
        "SUID" : 114,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.73914104,
        "selected" : false,
        "NeighborhoodConnectivity" : 91.36363636
      },
      "position" : {
        "x" : 1242.591064453125,
        "y" : 1475.647705078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "113",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Tony_Smith",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tony_Smith",
        "SelfLoops" : 0,
        "SUID" : 113,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -13.516881942749023,
        "y" : 590.4169311523438
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "112",
        "ClosenessCentrality" : 0.24434551,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.28571429,
        "Radiality" : 0.82819081,
        "Stress" : 212688,
        "TopologicalCoefficient" : 0.16071429,
        "shared_name" : "Jimmy_Mattingly",
        "BetweennessCentrality" : 1.023E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jimmy_Mattingly",
        "SelfLoops" : 0,
        "SUID" : 112,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 4.09256548,
        "selected" : false,
        "NeighborhoodConnectivity" : 72.28571429
      },
      "position" : {
        "x" : 43.83588790893555,
        "y" : 982.8787231445312
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "111",
        "ClosenessCentrality" : 0.25379327,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.83665474,
        "Stress" : 1603972,
        "TopologicalCoefficient" : 0.28695652,
        "shared_name" : "Michael_Omartian",
        "BetweennessCentrality" : 0.00113884,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Michael_Omartian",
        "SelfLoops" : 0,
        "SUID" : 111,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 3.94021474,
        "selected" : false,
        "NeighborhoodConnectivity" : 165.25
      },
      "position" : {
        "x" : 1398.29443359375,
        "y" : 1645.444580078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "110",
        "ClosenessCentrality" : 0.25879381,
        "degree_layout" : 11,
        "Eccentricity" : 11,
        "Degree" : 31,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.02988506,
        "Radiality" : 0.84088444,
        "Stress" : 3284970,
        "TopologicalCoefficient" : 0.04442446,
        "shared_name" : "Mindy_Smith",
        "BetweennessCentrality" : 0.00157303,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mindy_Smith",
        "SelfLoops" : 0,
        "SUID" : 110,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 31,
        "AverageShortestPathLength" : 3.86408004,
        "selected" : false,
        "NeighborhoodConnectivity" : 25.23333333
      },
      "position" : {
        "x" : 911.9802856445312,
        "y" : 1705.0311279296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "109",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Alan_Silverman",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Alan_Silverman",
        "SelfLoops" : 0,
        "SUID" : 109,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1724.5572509765625,
        "y" : 1578.08447265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "108",
        "ClosenessCentrality" : 0.23937382,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82346854,
        "Stress" : 30978,
        "TopologicalCoefficient" : 0.51424501,
        "shared_name" : "Jeannie_Seely",
        "BetweennessCentrality" : 9.9E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jeannie_Seely",
        "SelfLoops" : 0,
        "SUID" : 108,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.17756629,
        "selected" : false,
        "NeighborhoodConnectivity" : 181.5
      },
      "position" : {
        "x" : -146.04066467285156,
        "y" : 562.53466796875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "107",
        "ClosenessCentrality" : 0.26185304,
        "degree_layout" : 5,
        "Eccentricity" : 11,
        "Degree" : 19,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.13450292,
        "Radiality" : 0.84339244,
        "Stress" : 1349288,
        "TopologicalCoefficient" : 0.0727252,
        "shared_name" : "Darrin_Vincent",
        "BetweennessCentrality" : 5.0819E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Darrin_Vincent",
        "SelfLoops" : 0,
        "SUID" : 107,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 19,
        "AverageShortestPathLength" : 3.81893607,
        "selected" : false,
        "NeighborhoodConnectivity" : 67.15789474
      },
      "position" : {
        "x" : 753.1073608398438,
        "y" : 1116.9239501953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "106",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Joey_Schmidt",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joey_Schmidt",
        "SelfLoops" : 0,
        "SUID" : 106,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1837.980712890625,
        "y" : 1697.6700439453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "105",
        "ClosenessCentrality" : 0.23714363,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.8212859,
        "Stress" : 33444,
        "TopologicalCoefficient" : 0.35140562,
        "shared_name" : "Carole_Rabinowitz-Neuen",
        "BetweennessCentrality" : 9.64E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Carole_Rabinowitz-Neuen",
        "SelfLoops" : 0,
        "SUID" : 105,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.21685375,
        "selected" : false,
        "NeighborhoodConnectivity" : 117.0
      },
      "position" : {
        "x" : 1277.4146728515625,
        "y" : 1969.5772705078125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "104",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Kelly_Pribble",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kelly_Pribble",
        "SelfLoops" : 0,
        "SUID" : 104,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : -51.97808837890625,
        "y" : 443.8566589355469
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "103",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Ira_Parker",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Ira_Parker",
        "SelfLoops" : 0,
        "SUID" : 103,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1851.585205078125,
        "y" : 1520.2864990234375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "102",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Opry_Gang",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Opry_Gang",
        "SelfLoops" : 0,
        "SUID" : 102,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 238.81600952148438,
        "y" : 599.1492919921875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "101",
        "ClosenessCentrality" : 0.25813631,
        "degree_layout" : 6,
        "Eccentricity" : 11,
        "Degree" : 16,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.16666667,
        "Radiality" : 0.84033765,
        "Stress" : 1095006,
        "TopologicalCoefficient" : 0.07743644,
        "shared_name" : "Nickel_Creek",
        "BetweennessCentrality" : 6.4346E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Nickel_Creek",
        "SelfLoops" : 0,
        "SUID" : 101,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 16,
        "AverageShortestPathLength" : 3.87392224,
        "selected" : false,
        "NeighborhoodConnectivity" : 46.0
      },
      "position" : {
        "x" : 1280.4368896484375,
        "y" : 1404.75146484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "100",
        "ClosenessCentrality" : 0.26656548,
        "degree_layout" : 6,
        "Eccentricity" : 11,
        "Degree" : 27,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.07692308,
        "Radiality" : 0.84714314,
        "Stress" : 2994110,
        "TopologicalCoefficient" : 0.05162131,
        "shared_name" : "Johnny_Russell",
        "BetweennessCentrality" : 0.00152891,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Johnny_Russell",
        "SelfLoops" : 0,
        "SUID" : 100,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 27,
        "AverageShortestPathLength" : 3.75142346,
        "selected" : false,
        "NeighborhoodConnectivity" : 38.44444444
      },
      "position" : {
        "x" : 464.0442199707031,
        "y" : 1139.79638671875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "99",
        "ClosenessCentrality" : 0.2355398,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81969073,
        "Stress" : 46490,
        "TopologicalCoefficient" : 0.51032448,
        "shared_name" : "Joe_Nichols",
        "BetweennessCentrality" : 1.429E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Joe_Nichols",
        "SelfLoops" : 0,
        "SUID" : 99,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24556694,
        "selected" : false,
        "NeighborhoodConnectivity" : 174.0
      },
      "position" : {
        "x" : 1991.8792724609375,
        "y" : 1783.8948974609375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "98",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Jimmy_C._Newman",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jimmy_C._Newman",
        "SelfLoops" : 0,
        "SUID" : 98,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 105.90335845947266,
        "y" : 511.7202453613281
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "97",
        "ClosenessCentrality" : 0.24938131,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.83278203,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.53363229,
        "shared_name" : "Abraham_Laboriel,_Sr.",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Abraham_Laboriel,_Sr.",
        "SelfLoops" : 0,
        "SUID" : 97,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.00992354,
        "selected" : false,
        "NeighborhoodConnectivity" : 238.0
      },
      "position" : {
        "x" : 1419.5042724609375,
        "y" : 1510.5709228515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "96",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mel_McDaniel",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mel_McDaniel",
        "SelfLoops" : 0,
        "SUID" : 96,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 131.9844970703125,
        "y" : 373.72186279296875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "95",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Brenda_Lee",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Brenda_Lee",
        "SelfLoops" : 0,
        "SUID" : 95,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1697.9779052734375,
        "y" : 1433.5606689453125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "94",
        "ClosenessCentrality" : 0.25145218,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 7,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0952381,
        "Radiality" : 0.83461671,
        "Stress" : 269908,
        "TopologicalCoefficient" : 0.16648267,
        "shared_name" : "Anthony_LaMarchina",
        "BetweennessCentrality" : 7.477E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Anthony_LaMarchina",
        "SelfLoops" : 0,
        "SUID" : 94,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 7,
        "AverageShortestPathLength" : 3.9768993,
        "selected" : false,
        "NeighborhoodConnectivity" : 108.28571429
      },
      "position" : {
        "x" : 657.775390625,
        "y" : 2104.113525390625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "93",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Kid_Connection_Inc.",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kid_Connection_Inc.",
        "SelfLoops" : 0,
        "SUID" : 93,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 394.5531005859375,
        "y" : 463.566162109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "92",
        "ClosenessCentrality" : 0.24262399,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82657755,
        "Stress" : 1623218,
        "TopologicalCoefficient" : 0.35073628,
        "shared_name" : "Norah_Jones",
        "BetweennessCentrality" : 7.4614E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Norah_Jones",
        "SelfLoops" : 0,
        "SUID" : 92,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.12160403,
        "selected" : false,
        "NeighborhoodConnectivity" : 175.0
      },
      "position" : {
        "x" : 470.6502380371094,
        "y" : 2163.82470703125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "91",
        "ClosenessCentrality" : 0.23676,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.16666667,
        "Radiality" : 0.82090631,
        "Stress" : 98622,
        "TopologicalCoefficient" : 0.25933908,
        "shared_name" : "Roger_McGuinn",
        "BetweennessCentrality" : 4.491E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Roger_McGuinn",
        "SelfLoops" : 0,
        "SUID" : 91,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 4.22368635,
        "selected" : false,
        "NeighborhoodConnectivity" : 90.75
      },
      "position" : {
        "x" : 2111.20361328125,
        "y" : 1917.6505126953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "90",
        "ClosenessCentrality" : 0.26889176,
        "degree_layout" : 6,
        "Eccentricity" : 11,
        "Degree" : 11,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.18181818,
        "Radiality" : 0.84894619,
        "Stress" : 1344958,
        "TopologicalCoefficient" : 0.11730205,
        "shared_name" : "Carl_Gorodetzky",
        "BetweennessCentrality" : 4.1594E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Carl_Gorodetzky",
        "SelfLoops" : 0,
        "SUID" : 90,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 11,
        "AverageShortestPathLength" : 3.7189686,
        "selected" : false,
        "NeighborhoodConnectivity" : 131.18181818
      },
      "position" : {
        "x" : 936.2973022460938,
        "y" : 1845.811767578125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "89",
        "ClosenessCentrality" : 0.24017348,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.82424127,
        "Stress" : 16240,
        "TopologicalCoefficient" : 0.52234637,
        "shared_name" : "Jack_Jezzro",
        "BetweennessCentrality" : 3.2E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jack_Jezzro",
        "SelfLoops" : 0,
        "SUID" : 89,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.16365707,
        "selected" : false,
        "NeighborhoodConnectivity" : 188.0
      },
      "position" : {
        "x" : 2178.6162109375,
        "y" : 1794.861328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "88",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Tommy_Lee_James",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tommy_Lee_James",
        "SelfLoops" : 0,
        "SUID" : 88,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 7.375668048858643,
        "y" : 320.34906005859375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "87",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Tommy_James",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tommy_James",
        "SelfLoops" : 0,
        "SUID" : 87,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2029.1673583984375,
        "y" : 1496.1729736328125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "86",
        "ClosenessCentrality" : 0.23622773,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.8203776,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.53054662,
        "shared_name" : "Jon_Mark_Ivey",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jon_Mark_Ivey",
        "SelfLoops" : 0,
        "SUID" : 86,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.23320319,
        "selected" : false,
        "NeighborhoodConnectivity" : 165.0
      },
      "position" : {
        "x" : 824.4949951171875,
        "y" : 2216.193603515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "85",
        "ClosenessCentrality" : 0.23625043,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.8204002,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51442308,
        "shared_name" : "Tom_Howard",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Tom_Howard",
        "SelfLoops" : 0,
        "SUID" : 85,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.23279649,
        "selected" : false,
        "NeighborhoodConnectivity" : 160.5
      },
      "position" : {
        "x" : 1201.388671875,
        "y" : 2059.376953125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "84",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Mary_Hopkin",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Mary_Hopkin",
        "SelfLoops" : 0,
        "SUID" : 84,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 253.353759765625,
        "y" : 448.2077331542969
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "83",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Paul_Hollowell",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Paul_Hollowell",
        "SelfLoops" : 0,
        "SUID" : 83,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1955.6219482421875,
        "y" : 1624.0384521484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "82",
        "ClosenessCentrality" : 0.26510545,
        "degree_layout" : 4,
        "Eccentricity" : 10,
        "Degree" : 13,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.24358974,
        "Radiality" : 0.84599534,
        "Stress" : 1148446,
        "TopologicalCoefficient" : 0.10427702,
        "shared_name" : "Leland_Sklar",
        "BetweennessCentrality" : 3.9262E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Leland_Sklar",
        "SelfLoops" : 0,
        "SUID" : 82,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 13,
        "AverageShortestPathLength" : 3.77208394,
        "selected" : false,
        "NeighborhoodConnectivity" : 101.69230769
      },
      "position" : {
        "x" : 398.55731201171875,
        "y" : 1376.144287109375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "81",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "George_Hamilton_IV",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "George_Hamilton_IV",
        "SelfLoops" : 0,
        "SUID" : 81,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 386.0635070800781,
        "y" : 596.2041625976562
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "80",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Duane_Hamilton",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Duane_Hamilton",
        "SelfLoops" : 0,
        "SUID" : 80,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 1934.2747802734375,
        "y" : 1395.201171875
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "79",
        "ClosenessCentrality" : 0.31127203,
        "degree_layout" : 25,
        "Eccentricity" : 10,
        "Degree" : 137,
        "PartnerOfMultiEdgedNodePairs" : 9,
        "ClusteringCoefficient" : 0.07431102,
        "Radiality" : 0.87707644,
        "Stress" : 48832040,
        "TopologicalCoefficient" : 0.01651025,
        "shared_name" : "Sam_Bush",
        "BetweennessCentrality" : 0.02409793,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Sam_Bush",
        "SelfLoops" : 0,
        "SUID" : 79,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 137,
        "AverageShortestPathLength" : 3.21262404,
        "selected" : false,
        "NeighborhoodConnectivity" : 47.640625
      },
      "position" : {
        "x" : 877.5429077148438,
        "y" : 1524.6485595703125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "78",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Jack_Green",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Jack_Green",
        "SelfLoops" : 0,
        "SUID" : 78,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 375.743896484375,
        "y" : 323.9107666015625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "77",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Dave_Fowler",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Dave_Fowler",
        "SelfLoops" : 0,
        "SUID" : 77,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2087.97119140625,
        "y" : 1686.119384765625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "76",
        "ClosenessCentrality" : 0.23503097,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81918009,
        "Stress" : 155518,
        "TopologicalCoefficient" : 0.50477707,
        "shared_name" : "David_Foster",
        "BetweennessCentrality" : 5.732E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "David_Foster",
        "SelfLoops" : 0,
        "SUID" : 76,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.25475842,
        "selected" : false,
        "NeighborhoodConnectivity" : 159.5
      },
      "position" : {
        "x" : 113.44640350341797,
        "y" : 209.84954833984375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "75",
        "ClosenessCentrality" : 0.25537484,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.66666667,
        "Radiality" : 0.83801041,
        "Stress" : 88070,
        "TopologicalCoefficient" : 0.3021137,
        "shared_name" : "Kim_Fleming",
        "BetweennessCentrality" : 1.212E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Kim_Fleming",
        "SelfLoops" : 0,
        "SUID" : 75,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 3.91581259,
        "selected" : false,
        "NeighborhoodConnectivity" : 207.25
      },
      "position" : {
        "x" : 650.1820068359375,
        "y" : 852.7426147460938
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "74",
        "ClosenessCentrality" : 0.26492264,
        "degree_layout" : 4,
        "Eccentricity" : 11,
        "Degree" : 23,
        "PartnerOfMultiEdgedNodePairs" : 1,
        "ClusteringCoefficient" : 0.07359307,
        "Radiality" : 0.84585073,
        "Stress" : 3147904,
        "TopologicalCoefficient" : 0.05711243,
        "shared_name" : "Keith_Little",
        "BetweennessCentrality" : 0.00181437,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Keith_Little",
        "SelfLoops" : 0,
        "SUID" : 74,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 23,
        "AverageShortestPathLength" : 3.77468684,
        "selected" : false,
        "NeighborhoodConnectivity" : 46.59090909
      },
      "position" : {
        "x" : 689.3720703125,
        "y" : 1126.408447265625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "73",
        "ClosenessCentrality" : 0.2522933,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.5,
        "Radiality" : 0.83535329,
        "Stress" : 31542,
        "TopologicalCoefficient" : 0.28010204,
        "shared_name" : "Neal_Cappellino",
        "BetweennessCentrality" : 1.373E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Neal_Cappellino",
        "SelfLoops" : 0,
        "SUID" : 73,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 3.9636408,
        "selected" : false,
        "NeighborhoodConnectivity" : 137.5
      },
      "position" : {
        "x" : 1255.5357666015625,
        "y" : 1593.4351806640625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "72",
        "ClosenessCentrality" : 0.23563912,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 1.0,
        "Radiality" : 0.81979014,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.51618123,
        "shared_name" : "The_Dermot_Byrne_Blues_Combo",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "The_Dermot_Byrne_Blues_Combo",
        "SelfLoops" : 0,
        "SUID" : 72,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.24377745,
        "selected" : false,
        "NeighborhoodConnectivity" : 159.5
      },
      "position" : {
        "x" : 1569.498291015625,
        "y" : 1018.4412231445312
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "71",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Steve_Davis",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Steve_Davis",
        "SelfLoops" : 0,
        "SUID" : 71,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2154.080810546875,
        "y" : 1561.4869384765625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "70",
        "ClosenessCentrality" : 0.23434105,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81848417,
        "Stress" : 1340086,
        "TopologicalCoefficient" : 0.50171233,
        "shared_name" : "Phoebe_Cryar",
        "BetweennessCentrality" : 4.7072E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Phoebe_Cryar",
        "SelfLoops" : 0,
        "SUID" : 70,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.26728485,
        "selected" : false,
        "NeighborhoodConnectivity" : 147.5
      },
      "position" : {
        "x" : 244.2506866455078,
        "y" : 274.96868896484375
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "69",
        "ClosenessCentrality" : 0.25654188,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 4,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.5,
        "Radiality" : 0.83900005,
        "Stress" : 148872,
        "TopologicalCoefficient" : 0.28622611,
        "shared_name" : "Steve_Buckingham",
        "BetweennessCentrality" : 3.011E-5,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Steve_Buckingham",
        "SelfLoops" : 0,
        "SUID" : 69,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 4,
        "AverageShortestPathLength" : 3.89799902,
        "selected" : false,
        "NeighborhoodConnectivity" : 179.75
      },
      "position" : {
        "x" : 1203.8404541015625,
        "y" : 1753.013916015625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "68",
        "ClosenessCentrality" : 0.28540254,
        "degree_layout" : 7,
        "Eccentricity" : 10,
        "Degree" : 34,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.1426025,
        "Radiality" : 0.86089872,
        "Stress" : 7686834,
        "TopologicalCoefficient" : 0.04628926,
        "shared_name" : "Carl_Jackson",
        "BetweennessCentrality" : 0.00305668,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Carl_Jackson",
        "SelfLoops" : 0,
        "SUID" : 68,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 34,
        "AverageShortestPathLength" : 3.503823,
        "selected" : false,
        "NeighborhoodConnectivity" : 82.5
      },
      "position" : {
        "x" : 806.9840698242188,
        "y" : 1557.3590087890625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "67",
        "ClosenessCentrality" : 0.23434105,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81848417,
        "Stress" : 1340086,
        "TopologicalCoefficient" : 0.50171233,
        "shared_name" : "Callie_Cryar",
        "BetweennessCentrality" : 4.7072E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Callie_Cryar",
        "SelfLoops" : 0,
        "SUID" : 67,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.26728485,
        "selected" : false,
        "NeighborhoodConnectivity" : 147.5
      },
      "position" : {
        "x" : 2255.440673828125,
        "y" : 1665.2193603515625
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "66",
        "ClosenessCentrality" : 0.24700139,
        "degree_layout" : 3,
        "Eccentricity" : 11,
        "Degree" : 10,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.15555556,
        "Radiality" : 0.83063554,
        "Stress" : 727076,
        "TopologicalCoefficient" : 0.11683333,
        "shared_name" : "Terry_Smith",
        "BetweennessCentrality" : 2.5349E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Terry_Smith",
        "SelfLoops" : 0,
        "SUID" : 66,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 10,
        "AverageShortestPathLength" : 4.04856027,
        "selected" : false,
        "NeighborhoodConnectivity" : 70.5
      },
      "position" : {
        "x" : 206.42733764648438,
        "y" : 980.5703735351562
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "65",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Angel_Cruz",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Angel_Cruz",
        "SelfLoops" : 0,
        "SUID" : 65,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 524.7659301757812,
        "y" : 399.7970275878906
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "64",
        "ClosenessCentrality" : 0.23409563,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 1,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.81823563,
        "Stress" : 0,
        "TopologicalCoefficient" : 0.0,
        "shared_name" : "Judy_Collins",
        "BetweennessCentrality" : 0.0,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Judy_Collins",
        "SelfLoops" : 0,
        "SUID" : 64,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 1,
        "AverageShortestPathLength" : 4.27175858,
        "selected" : false,
        "NeighborhoodConnectivity" : 287.0
      },
      "position" : {
        "x" : 2069.078369140625,
        "y" : 1331.62158203125
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "63",
        "ClosenessCentrality" : 0.23857947,
        "degree_layout" : 1,
        "Eccentricity" : 11,
        "Degree" : 2,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.0,
        "Radiality" : 0.8226958,
        "Stress" : 104280,
        "TopologicalCoefficient" : 0.5,
        "shared_name" : "Glenn_Korman",
        "BetweennessCentrality" : 1.3236E-4,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Glenn_Korman",
        "SelfLoops" : 0,
        "SUID" : 63,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 2,
        "AverageShortestPathLength" : 4.19147552,
        "selected" : false,
        "NeighborhoodConnectivity" : 171.0
      },
      "position" : {
        "x" : 1737.5487060546875,
        "y" : 174.64321899414062
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "62",
        "ClosenessCentrality" : 0.24582592,
        "degree_layout" : 2,
        "Eccentricity" : 11,
        "Degree" : 3,
        "PartnerOfMultiEdgedNodePairs" : 0,
        "ClusteringCoefficient" : 0.33333333,
        "Radiality" : 0.82956004,
        "Stress" : 37342,
        "TopologicalCoefficient" : 0.37360179,
        "shared_name" : "Lisa_Cochran",
        "BetweennessCentrality" : 6.25E-6,
        "NumberOfUndirectedEdges" : 0,
        "name" : "Lisa_Cochran",
        "SelfLoops" : 0,
        "SUID" : 62,
        "IsSingleNode" : false,
        "NumberOfDirectedEdges" : 3,
        "AverageShortestPathLength" : 4.06791931,
        "selected" : false,
        "NeighborhoodConnectivity" : 167.33333333
      },
      "position" : {
        "x" : 914.5097045898438,
        "y" : 2227.39208984375
      },
      "selected" : false
    } ],
    "edges" : [ {
      "data" : {
        "id" : "1050",
        "source" : "332",
        "target" : "279",
        "EdgeBetweenness" : 7261.04749845,
        "shared_name" : "Patty_Loveless () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 1050,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1049",
        "source" : "332",
        "target" : "336",
        "EdgeBetweenness" : 13081.10967971,
        "shared_name" : "Patty_Loveless () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Matt_Rollings",
        "interaction" : "",
        "SUID" : 1049,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1048",
        "source" : "332",
        "target" : "194",
        "EdgeBetweenness" : 9186.59656485,
        "shared_name" : "Patty_Loveless () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Barry_Bales",
        "interaction" : "",
        "SUID" : 1048,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1047",
        "source" : "332",
        "target" : "174",
        "EdgeBetweenness" : 94652.95449171,
        "shared_name" : "Patty_Loveless () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 1047,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1046",
        "source" : "332",
        "target" : "140",
        "EdgeBetweenness" : 3611.86169121,
        "shared_name" : "Patty_Loveless () Rebecca_Lynn_Howard",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Rebecca_Lynn_Howard",
        "interaction" : "",
        "SUID" : 1046,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1045",
        "source" : "332",
        "target" : "68",
        "EdgeBetweenness" : 10883.45793315,
        "shared_name" : "Patty_Loveless () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Carl_Jackson",
        "interaction" : "",
        "SUID" : 1045,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1044",
        "source" : "332",
        "target" : "164",
        "EdgeBetweenness" : 6731.72768406,
        "shared_name" : "Patty_Loveless () Rob_McCoury",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Rob_McCoury",
        "interaction" : "",
        "SUID" : 1044,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1043",
        "source" : "332",
        "target" : "163",
        "EdgeBetweenness" : 18720.08463329,
        "shared_name" : "Patty_Loveless () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 1043,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1042",
        "source" : "332",
        "target" : "217",
        "EdgeBetweenness" : 20361.98009292,
        "shared_name" : "Patty_Loveless () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 1042,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1041",
        "source" : "332",
        "target" : "131",
        "EdgeBetweenness" : 7565.37566587,
        "shared_name" : "Patty_Loveless () Matraca_Berg",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Matraca_Berg",
        "interaction" : "",
        "SUID" : 1041,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1040",
        "source" : "332",
        "target" : "281",
        "EdgeBetweenness" : 9330.1082606,
        "shared_name" : "Patty_Loveless () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Paul_Franklin",
        "interaction" : "",
        "SUID" : 1040,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1039",
        "source" : "332",
        "target" : "268",
        "EdgeBetweenness" : 27555.45654616,
        "shared_name" : "Patty_Loveless () Claire_Lynch",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Claire_Lynch",
        "interaction" : "",
        "SUID" : 1039,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1038",
        "source" : "332",
        "target" : "82",
        "EdgeBetweenness" : 5580.2515909,
        "shared_name" : "Patty_Loveless () Leland_Sklar",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Leland_Sklar",
        "interaction" : "",
        "SUID" : 1038,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1037",
        "source" : "332",
        "target" : "211",
        "EdgeBetweenness" : 45716.58757818,
        "shared_name" : "Patty_Loveless () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 1037,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1036",
        "source" : "332",
        "target" : "215",
        "EdgeBetweenness" : 47095.60588665,
        "shared_name" : "Patty_Loveless () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Alison_Krauss",
        "interaction" : "",
        "SUID" : 1036,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1035",
        "source" : "332",
        "target" : "292",
        "EdgeBetweenness" : 50487.92109774,
        "shared_name" : "Patty_Loveless () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 1035,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1034",
        "source" : "332",
        "target" : "127",
        "EdgeBetweenness" : 16476.95030742,
        "shared_name" : "Patty_Loveless () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Patty_Loveless () Harry_Stinson",
        "interaction" : "",
        "SUID" : 1034,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1033",
        "source" : "326",
        "target" : "79",
        "EdgeBetweenness" : 63083.74369874,
        "shared_name" : "Maura_O'Connell () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Maura_O'Connell () Sam_Bush",
        "interaction" : "",
        "SUID" : 1033,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1032",
        "source" : "326",
        "target" : "211",
        "EdgeBetweenness" : 74834.02008274,
        "shared_name" : "Maura_O'Connell () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Maura_O'Connell () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 1032,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1031",
        "source" : "326",
        "target" : "235",
        "EdgeBetweenness" : 21780.03715409,
        "shared_name" : "Maura_O'Connell () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Maura_O'Connell () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 1031,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1030",
        "source" : "326",
        "target" : "273",
        "EdgeBetweenness" : 91257.93399597,
        "shared_name" : "Maura_O'Connell () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Maura_O'Connell () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 1030,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1029",
        "source" : "326",
        "target" : "334",
        "EdgeBetweenness" : 8504.45688345,
        "shared_name" : "Maura_O'Connell () Ciaran_Tourish",
        "shared_interaction" : "",
        "name" : "Maura_O'Connell () Ciaran_Tourish",
        "interaction" : "",
        "SUID" : 1029,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1028",
        "source" : "323",
        "target" : "324",
        "EdgeBetweenness" : 232.43010914,
        "shared_name" : "Dermot_Byrne () Ciaran_Curran",
        "shared_interaction" : "",
        "name" : "Dermot_Byrne () Ciaran_Curran",
        "interaction" : "",
        "SUID" : 1028,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1027",
        "source" : "321",
        "target" : "323",
        "EdgeBetweenness" : 5417.77909174,
        "shared_name" : "Altan () Dermot_Byrne",
        "shared_interaction" : "",
        "name" : "Altan () Dermot_Byrne",
        "interaction" : "",
        "SUID" : 1027,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1026",
        "source" : "321",
        "target" : "338",
        "EdgeBetweenness" : 1055.56281944,
        "shared_name" : "Altan () Frankie_Kennedy",
        "shared_interaction" : "",
        "name" : "Altan () Frankie_Kennedy",
        "interaction" : "",
        "SUID" : 1026,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1025",
        "source" : "321",
        "target" : "72",
        "EdgeBetweenness" : 1055.56281944,
        "shared_name" : "Altan () The_Dermot_Byrne_Blues_Combo",
        "shared_interaction" : "",
        "name" : "Altan () The_Dermot_Byrne_Blues_Combo",
        "interaction" : "",
        "SUID" : 1025,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1024",
        "source" : "321",
        "target" : "324",
        "EdgeBetweenness" : 917.03761869,
        "shared_name" : "Altan () Ciaran_Curran",
        "shared_interaction" : "",
        "name" : "Altan () Ciaran_Curran",
        "interaction" : "",
        "SUID" : 1024,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1023",
        "source" : "321",
        "target" : "337",
        "EdgeBetweenness" : 1055.56281944,
        "shared_name" : "Altan () Mark_Kelly",
        "shared_interaction" : "",
        "name" : "Altan () Mark_Kelly",
        "interaction" : "",
        "SUID" : 1023,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1022",
        "source" : "321",
        "target" : "330",
        "EdgeBetweenness" : 1713.38212544,
        "shared_name" : "Altan () Mairead_Ni_Mhaonaigh",
        "shared_interaction" : "",
        "name" : "Altan () Mairead_Ni_Mhaonaigh",
        "interaction" : "",
        "SUID" : 1022,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1021",
        "source" : "321",
        "target" : "343",
        "EdgeBetweenness" : 1055.56281944,
        "shared_name" : "Altan () Proinsias_O'Maonaigh",
        "shared_interaction" : "",
        "name" : "Altan () Proinsias_O'Maonaigh",
        "interaction" : "",
        "SUID" : 1021,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1020",
        "source" : "321",
        "target" : "333",
        "EdgeBetweenness" : 3493.46081044,
        "shared_name" : "Altan () Daithi_Sproule",
        "shared_interaction" : "",
        "name" : "Altan () Daithi_Sproule",
        "interaction" : "",
        "SUID" : 1020,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1019",
        "source" : "321",
        "target" : "334",
        "EdgeBetweenness" : 2693.47005961,
        "shared_name" : "Altan () Ciaran_Tourish",
        "shared_interaction" : "",
        "name" : "Altan () Ciaran_Tourish",
        "interaction" : "",
        "SUID" : 1019,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1018",
        "source" : "319",
        "target" : "336",
        "EdgeBetweenness" : 18797.23997953,
        "shared_name" : "Kris_Kristofferson () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Matt_Rollings",
        "interaction" : "",
        "SUID" : 1018,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1017",
        "source" : "319",
        "target" : "170",
        "EdgeBetweenness" : 49724.55847719,
        "shared_name" : "Kris_Kristofferson () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Sara_Watkins",
        "interaction" : "",
        "SUID" : 1017,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1016",
        "source" : "319",
        "target" : "279",
        "EdgeBetweenness" : 12473.67568501,
        "shared_name" : "Kris_Kristofferson () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 1016,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1015",
        "source" : "319",
        "target" : "79",
        "EdgeBetweenness" : 88417.58954784,
        "shared_name" : "Kris_Kristofferson () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Sam_Bush",
        "interaction" : "",
        "SUID" : 1015,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1014",
        "source" : "319",
        "target" : "211",
        "EdgeBetweenness" : 119474.1836675,
        "shared_name" : "Kris_Kristofferson () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 1014,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1013",
        "source" : "319",
        "target" : "174",
        "EdgeBetweenness" : 131582.66166258,
        "shared_name" : "Kris_Kristofferson () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 1013,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1012",
        "source" : "319",
        "target" : "209",
        "EdgeBetweenness" : 42726.22300427,
        "shared_name" : "Kris_Kristofferson () Byron_House",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Byron_House",
        "interaction" : "",
        "SUID" : 1012,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1011",
        "source" : "319",
        "target" : "188",
        "EdgeBetweenness" : 76038.26817745,
        "shared_name" : "Kris_Kristofferson () Willie_Nelson",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Willie_Nelson",
        "interaction" : "",
        "SUID" : 1011,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1010",
        "source" : "319",
        "target" : "316",
        "EdgeBetweenness" : 15760.32363684,
        "shared_name" : "Kris_Kristofferson () Randy_Scruggs",
        "shared_interaction" : "",
        "name" : "Kris_Kristofferson () Randy_Scruggs",
        "interaction" : "",
        "SUID" : 1010,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1009",
        "source" : "311",
        "target" : "209",
        "EdgeBetweenness" : 12988.84813374,
        "shared_name" : "Chris_Thile () Byron_House",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Byron_House",
        "interaction" : "",
        "SUID" : 1009,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1008",
        "source" : "311",
        "target" : "114",
        "EdgeBetweenness" : 2295.92763014,
        "shared_name" : "Chris_Thile () Gary_Paczosa",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Gary_Paczosa",
        "interaction" : "",
        "SUID" : 1008,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1007",
        "source" : "311",
        "target" : "217",
        "EdgeBetweenness" : 8150.2524354,
        "shared_name" : "Chris_Thile () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 1007,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1006",
        "source" : "311",
        "target" : "170",
        "EdgeBetweenness" : 21559.81829216,
        "shared_name" : "Chris_Thile () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Sara_Watkins",
        "interaction" : "",
        "SUID" : 1006,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1005",
        "source" : "311",
        "target" : "156",
        "EdgeBetweenness" : 12182.90842759,
        "shared_name" : "Chris_Thile () Sean_Watkins",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Sean_Watkins",
        "interaction" : "",
        "SUID" : 1005,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1004",
        "source" : "311",
        "target" : "79",
        "EdgeBetweenness" : 17473.51571163,
        "shared_name" : "Chris_Thile () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Sam_Bush",
        "interaction" : "",
        "SUID" : 1004,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1003",
        "source" : "311",
        "target" : "273",
        "EdgeBetweenness" : 24148.17173871,
        "shared_name" : "Chris_Thile () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 1003,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1002",
        "source" : "311",
        "target" : "211",
        "EdgeBetweenness" : 18379.27098273,
        "shared_name" : "Chris_Thile () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Chris_Thile () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 1002,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1001",
        "source" : "308",
        "target" : "306",
        "EdgeBetweenness" : 3160.1057603,
        "shared_name" : "Terry_Eldredge () Bobby_Osborne",
        "shared_interaction" : "",
        "name" : "Terry_Eldredge () Bobby_Osborne",
        "interaction" : "",
        "SUID" : 1001,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "1000",
        "source" : "308",
        "target" : "307",
        "EdgeBetweenness" : 789.20482493,
        "shared_name" : "Terry_Eldredge () Sonny_Osborne",
        "shared_interaction" : "",
        "name" : "Terry_Eldredge () Sonny_Osborne",
        "interaction" : "",
        "SUID" : 1000,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "999",
        "source" : "308",
        "target" : "66",
        "EdgeBetweenness" : 1252.46474787,
        "shared_name" : "Terry_Eldredge () Terry_Smith",
        "shared_interaction" : "",
        "name" : "Terry_Eldredge () Terry_Smith",
        "interaction" : "",
        "SUID" : 999,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "998",
        "source" : "306",
        "target" : "172",
        "EdgeBetweenness" : 43584.42894453,
        "shared_name" : "Bobby_Osborne () Rhonda_Vincent",
        "shared_interaction" : "",
        "name" : "Bobby_Osborne () Rhonda_Vincent",
        "interaction" : "",
        "SUID" : 998,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "997",
        "source" : "306",
        "target" : "196",
        "EdgeBetweenness" : 8302.15901986,
        "shared_name" : "Bobby_Osborne () Paul_Brewster",
        "shared_interaction" : "",
        "name" : "Bobby_Osborne () Paul_Brewster",
        "interaction" : "",
        "SUID" : 997,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "996",
        "source" : "306",
        "target" : "276",
        "EdgeBetweenness" : 9908.16865181,
        "shared_name" : "Bobby_Osborne () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Bobby_Osborne () Jim_Mills",
        "interaction" : "",
        "SUID" : 996,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "995",
        "source" : "287",
        "target" : "144",
        "EdgeBetweenness" : 10914.69021241,
        "shared_name" : "George_Jones () Liana_Manis",
        "shared_interaction" : "",
        "name" : "George_Jones () Liana_Manis",
        "interaction" : "",
        "SUID" : 995,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "994",
        "source" : "287",
        "target" : "161",
        "EdgeBetweenness" : 183365.8860964,
        "shared_name" : "George_Jones () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "George_Jones () Dolly_Parton",
        "interaction" : "",
        "SUID" : 994,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "993",
        "source" : "287",
        "target" : "315",
        "EdgeBetweenness" : 8027.27866391,
        "shared_name" : "George_Jones () John_Wesley_Ryles",
        "shared_interaction" : "",
        "name" : "George_Jones () John_Wesley_Ryles",
        "interaction" : "",
        "SUID" : 993,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "992",
        "source" : "287",
        "target" : "172",
        "EdgeBetweenness" : 141430.3340591,
        "shared_name" : "George_Jones () Rhonda_Vincent",
        "shared_interaction" : "",
        "name" : "George_Jones () Rhonda_Vincent",
        "interaction" : "",
        "SUID" : 992,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "991",
        "source" : "287",
        "target" : "292",
        "EdgeBetweenness" : 40205.34184648,
        "shared_name" : "George_Jones () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "George_Jones () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 991,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "990",
        "source" : "287",
        "target" : "279",
        "EdgeBetweenness" : 24104.0862468,
        "shared_name" : "George_Jones () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "George_Jones () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 990,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "989",
        "source" : "287",
        "target" : "281",
        "EdgeBetweenness" : 28665.35435992,
        "shared_name" : "George_Jones () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "George_Jones () Paul_Franklin",
        "interaction" : "",
        "SUID" : 989,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "988",
        "source" : "287",
        "target" : "252",
        "EdgeBetweenness" : 11533.23152766,
        "shared_name" : "George_Jones () Louis_Dean_Nunley",
        "shared_interaction" : "",
        "name" : "George_Jones () Louis_Dean_Nunley",
        "interaction" : "",
        "SUID" : 988,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "987",
        "source" : "287",
        "target" : "299",
        "EdgeBetweenness" : 16124.11250346,
        "shared_name" : "George_Jones () Bruce_Watkins",
        "shared_interaction" : "",
        "name" : "George_Jones () Bruce_Watkins",
        "interaction" : "",
        "SUID" : 987,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "986",
        "source" : "287",
        "target" : "207",
        "EdgeBetweenness" : 23794.19389475,
        "shared_name" : "George_Jones () Reggie_Young",
        "shared_interaction" : "",
        "name" : "George_Jones () Reggie_Young",
        "interaction" : "",
        "SUID" : 986,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "985",
        "source" : "286",
        "target" : "272",
        "EdgeBetweenness" : 30393.55475761,
        "shared_name" : "Robbie_Fulks () Bill_Anderson",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Bill_Anderson",
        "interaction" : "",
        "SUID" : 985,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "984",
        "source" : "286",
        "target" : "161",
        "EdgeBetweenness" : 263871.93844416,
        "shared_name" : "Robbie_Fulks () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Dolly_Parton",
        "interaction" : "",
        "SUID" : 984,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "983",
        "source" : "286",
        "target" : "120",
        "EdgeBetweenness" : 49894.62051497,
        "shared_name" : "Robbie_Fulks () Porter_Wagoner",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Porter_Wagoner",
        "interaction" : "",
        "SUID" : 983,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "982",
        "source" : "286",
        "target" : "79",
        "EdgeBetweenness" : 258070.14669188,
        "shared_name" : "Robbie_Fulks () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Sam_Bush",
        "interaction" : "",
        "SUID" : 982,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "981",
        "source" : "286",
        "target" : "90",
        "EdgeBetweenness" : 26167.55132838,
        "shared_name" : "Robbie_Fulks () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 981,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "980",
        "source" : "286",
        "target" : "151",
        "EdgeBetweenness" : 5091.12079998,
        "shared_name" : "Robbie_Fulks () Alan_Umstead",
        "shared_interaction" : "",
        "name" : "Robbie_Fulks () Alan_Umstead",
        "interaction" : "",
        "SUID" : 980,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "979",
        "source" : "282",
        "target" : "200",
        "EdgeBetweenness" : 4779.03965762,
        "shared_name" : "Herb_Pedersen () Joey_Scarbury",
        "shared_interaction" : "",
        "name" : "Herb_Pedersen () Joey_Scarbury",
        "interaction" : "",
        "SUID" : 979,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "978",
        "source" : "282",
        "target" : "82",
        "EdgeBetweenness" : 8342.1009399,
        "shared_name" : "Herb_Pedersen () Leland_Sklar",
        "shared_interaction" : "",
        "name" : "Herb_Pedersen () Leland_Sklar",
        "interaction" : "",
        "SUID" : 978,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "977",
        "source" : "281",
        "target" : "259",
        "EdgeBetweenness" : 9171.89244056,
        "shared_name" : "Paul_Franklin () Joe_Spivey",
        "shared_interaction" : "",
        "name" : "Paul_Franklin () Joe_Spivey",
        "interaction" : "",
        "SUID" : 977,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "976",
        "source" : "280",
        "target" : "279",
        "EdgeBetweenness" : 7220.84838075,
        "shared_name" : "Martina_McBride () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 976,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "975",
        "source" : "280",
        "target" : "62",
        "EdgeBetweenness" : 2158.95489281,
        "shared_name" : "Martina_McBride () Lisa_Cochran",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Lisa_Cochran",
        "interaction" : "",
        "SUID" : 975,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "974",
        "source" : "280",
        "target" : "239",
        "EdgeBetweenness" : 4328.96769431,
        "shared_name" : "Martina_McBride () David_Davidson",
        "shared_interaction" : "",
        "name" : "Martina_McBride () David_Davidson",
        "interaction" : "",
        "SUID" : 974,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "973",
        "source" : "280",
        "target" : "211",
        "EdgeBetweenness" : 49270.69533779,
        "shared_name" : "Martina_McBride () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 973,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "972",
        "source" : "280",
        "target" : "126",
        "EdgeBetweenness" : 2618.76273186,
        "shared_name" : "Martina_McBride () Connie_Ellisor",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Connie_Ellisor",
        "interaction" : "",
        "SUID" : 972,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "971",
        "source" : "280",
        "target" : "281",
        "EdgeBetweenness" : 9027.85300635,
        "shared_name" : "Martina_McBride () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Paul_Franklin",
        "interaction" : "",
        "SUID" : 971,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "970",
        "source" : "280",
        "target" : "90",
        "EdgeBetweenness" : 3179.83760258,
        "shared_name" : "Martina_McBride () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 970,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "969",
        "source" : "280",
        "target" : "132",
        "EdgeBetweenness" : 2748.1252647,
        "shared_name" : "Martina_McBride () Jim_Grosjean",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Jim_Grosjean",
        "interaction" : "",
        "SUID" : 969,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "968",
        "source" : "280",
        "target" : "86",
        "EdgeBetweenness" : 3650.50281415,
        "shared_name" : "Martina_McBride () Jon_Mark_Ivey",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Jon_Mark_Ivey",
        "interaction" : "",
        "SUID" : 968,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "967",
        "source" : "280",
        "target" : "94",
        "EdgeBetweenness" : 2211.20898409,
        "shared_name" : "Martina_McBride () Anthony_LaMarchina",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Anthony_LaMarchina",
        "interaction" : "",
        "SUID" : 967,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "966",
        "source" : "280",
        "target" : "161",
        "EdgeBetweenness" : 41175.77186488,
        "shared_name" : "Martina_McBride () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Dolly_Parton",
        "interaction" : "",
        "SUID" : 966,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "965",
        "source" : "280",
        "target" : "315",
        "EdgeBetweenness" : 3754.43925679,
        "shared_name" : "Martina_McBride () John_Wesley_Ryles",
        "shared_interaction" : "",
        "name" : "Martina_McBride () John_Wesley_Ryles",
        "interaction" : "",
        "SUID" : 965,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "964",
        "source" : "280",
        "target" : "149",
        "EdgeBetweenness" : 3946.7048703,
        "shared_name" : "Martina_McBride () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 964,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "963",
        "source" : "280",
        "target" : "219",
        "EdgeBetweenness" : 22622.36807555,
        "shared_name" : "Martina_McBride () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 963,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "962",
        "source" : "280",
        "target" : "151",
        "EdgeBetweenness" : 3549.91056703,
        "shared_name" : "Martina_McBride () Alan_Umstead",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Alan_Umstead",
        "interaction" : "",
        "SUID" : 962,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "961",
        "source" : "280",
        "target" : "155",
        "EdgeBetweenness" : 3268.62986376,
        "shared_name" : "Martina_McBride () Catherine_Umstead",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Catherine_Umstead",
        "interaction" : "",
        "SUID" : 961,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "960",
        "source" : "280",
        "target" : "157",
        "EdgeBetweenness" : 2665.40513515,
        "shared_name" : "Martina_McBride () Mary_Kathryn_Van_Osdale",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Mary_Kathryn_Van_Osdale",
        "interaction" : "",
        "SUID" : 960,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "959",
        "source" : "280",
        "target" : "160",
        "EdgeBetweenness" : 2230.35155279,
        "shared_name" : "Martina_McBride () Gary_VanOsdale",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Gary_VanOsdale",
        "interaction" : "",
        "SUID" : 959,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "958",
        "source" : "280",
        "target" : "172",
        "EdgeBetweenness" : 37528.57272052,
        "shared_name" : "Martina_McBride () Rhonda_Vincent",
        "shared_interaction" : "",
        "name" : "Martina_McBride () Rhonda_Vincent",
        "interaction" : "",
        "SUID" : 958,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "957",
        "source" : "277",
        "target" : "211",
        "EdgeBetweenness" : 47016.31845661,
        "shared_name" : "Tony_Rice () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Tony_Rice () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 957,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "956",
        "source" : "277",
        "target" : "190",
        "EdgeBetweenness" : 32928.6782795,
        "shared_name" : "Tony_Rice () David_Grisman",
        "shared_interaction" : "",
        "name" : "Tony_Rice () David_Grisman",
        "interaction" : "",
        "SUID" : 956,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "955",
        "source" : "277",
        "target" : "79",
        "EdgeBetweenness" : 36655.84135674,
        "shared_name" : "Tony_Rice () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Tony_Rice () Sam_Bush",
        "interaction" : "",
        "SUID" : 955,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "954",
        "source" : "277",
        "target" : "273",
        "EdgeBetweenness" : 61444.89100642,
        "shared_name" : "Tony_Rice () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Tony_Rice () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 954,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "953",
        "source" : "277",
        "target" : "229",
        "EdgeBetweenness" : 65934.18806367,
        "shared_name" : "Tony_Rice () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Tony_Rice () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 953,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "952",
        "source" : "276",
        "target" : "273",
        "EdgeBetweenness" : 11365.15421026,
        "shared_name" : "Jim_Mills () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 952,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "951",
        "source" : "276",
        "target" : "235",
        "EdgeBetweenness" : 2825.73491671,
        "shared_name" : "Jim_Mills () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 951,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "950",
        "source" : "276",
        "target" : "194",
        "EdgeBetweenness" : 1155.92679777,
        "shared_name" : "Jim_Mills () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Barry_Bales",
        "interaction" : "",
        "SUID" : 950,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "949",
        "source" : "276",
        "target" : "196",
        "EdgeBetweenness" : 479.53801481,
        "shared_name" : "Jim_Mills () Paul_Brewster",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Paul_Brewster",
        "interaction" : "",
        "SUID" : 949,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "948",
        "source" : "276",
        "target" : "211",
        "EdgeBetweenness" : 7306.65563218,
        "shared_name" : "Jim_Mills () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 948,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "947",
        "source" : "276",
        "target" : "229",
        "EdgeBetweenness" : 9155.28902795,
        "shared_name" : "Jim_Mills () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 947,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "946",
        "source" : "276",
        "target" : "192",
        "EdgeBetweenness" : 3458.61569092,
        "shared_name" : "Jim_Mills () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Adam_Steffey",
        "interaction" : "",
        "SUID" : 946,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "945",
        "source" : "276",
        "target" : "219",
        "EdgeBetweenness" : 3920.21170455,
        "shared_name" : "Jim_Mills () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Jim_Mills () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 945,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "944",
        "source" : "273",
        "target" : "229",
        "EdgeBetweenness" : 129990.81251995,
        "shared_name" : "Jerry_Douglas () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 944,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "943",
        "source" : "273",
        "target" : "277",
        "EdgeBetweenness" : 61444.89100642,
        "shared_name" : "Jerry_Douglas () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Tony_Rice",
        "interaction" : "",
        "SUID" : 943,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "942",
        "source" : "273",
        "target" : "209",
        "EdgeBetweenness" : 78466.98160032,
        "shared_name" : "Jerry_Douglas () Byron_House",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Byron_House",
        "interaction" : "",
        "SUID" : 942,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "941",
        "source" : "273",
        "target" : "311",
        "EdgeBetweenness" : 24148.17173871,
        "shared_name" : "Jerry_Douglas () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Chris_Thile",
        "interaction" : "",
        "SUID" : 941,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "940",
        "source" : "273",
        "target" : "217",
        "EdgeBetweenness" : 37543.1908745,
        "shared_name" : "Jerry_Douglas () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 940,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "939",
        "source" : "273",
        "target" : "211",
        "EdgeBetweenness" : 67869.23484749,
        "shared_name" : "Jerry_Douglas () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 939,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "938",
        "source" : "273",
        "target" : "326",
        "EdgeBetweenness" : 91257.93399597,
        "shared_name" : "Jerry_Douglas () Maura_O'Connell",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Maura_O'Connell",
        "interaction" : "",
        "SUID" : 938,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "937",
        "source" : "273",
        "target" : "235",
        "EdgeBetweenness" : 20541.09078982,
        "shared_name" : "Jerry_Douglas () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 937,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "936",
        "source" : "273",
        "target" : "192",
        "EdgeBetweenness" : 52575.29339204,
        "shared_name" : "Jerry_Douglas () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Adam_Steffey",
        "interaction" : "",
        "SUID" : 936,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "935",
        "source" : "273",
        "target" : "194",
        "EdgeBetweenness" : 19048.75814525,
        "shared_name" : "Jerry_Douglas () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Barry_Bales",
        "interaction" : "",
        "SUID" : 935,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "934",
        "source" : "273",
        "target" : "208",
        "EdgeBetweenness" : 44541.0979322,
        "shared_name" : "Jerry_Douglas () Ron_Block",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Ron_Block",
        "interaction" : "",
        "SUID" : 934,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "933",
        "source" : "273",
        "target" : "215",
        "EdgeBetweenness" : 125108.59466615,
        "shared_name" : "Jerry_Douglas () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Alison_Krauss",
        "interaction" : "",
        "SUID" : 933,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "932",
        "source" : "273",
        "target" : "134",
        "EdgeBetweenness" : 24828.26305534,
        "shared_name" : "Jerry_Douglas () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 932,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "931",
        "source" : "273",
        "target" : "219",
        "EdgeBetweenness" : 53053.97310345,
        "shared_name" : "Jerry_Douglas () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 931,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "930",
        "source" : "273",
        "target" : "79",
        "EdgeBetweenness" : 65838.50102878,
        "shared_name" : "Jerry_Douglas () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Jerry_Douglas () Sam_Bush",
        "interaction" : "",
        "SUID" : 930,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "929",
        "source" : "272",
        "target" : "275",
        "EdgeBetweenness" : 9912.1723538,
        "shared_name" : "Bill_Anderson () Jan_Howard",
        "shared_interaction" : "",
        "name" : "Bill_Anderson () Jan_Howard",
        "interaction" : "",
        "SUID" : 929,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "928",
        "source" : "272",
        "target" : "188",
        "EdgeBetweenness" : 54355.25711733,
        "shared_name" : "Bill_Anderson () Willie_Nelson",
        "shared_interaction" : "",
        "name" : "Bill_Anderson () Willie_Nelson",
        "interaction" : "",
        "SUID" : 928,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "927",
        "source" : "272",
        "target" : "161",
        "EdgeBetweenness" : 28559.76835912,
        "shared_name" : "Bill_Anderson () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Bill_Anderson () Dolly_Parton",
        "interaction" : "",
        "SUID" : 927,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "926",
        "source" : "268",
        "target" : "163",
        "EdgeBetweenness" : 20384.65915169,
        "shared_name" : "Claire_Lynch () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 926,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "925",
        "source" : "268",
        "target" : "192",
        "EdgeBetweenness" : 23388.12818862,
        "shared_name" : "Claire_Lynch () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Adam_Steffey",
        "interaction" : "",
        "SUID" : 925,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "924",
        "source" : "268",
        "target" : "211",
        "EdgeBetweenness" : 41919.74509514,
        "shared_name" : "Claire_Lynch () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 924,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "923",
        "source" : "268",
        "target" : "167",
        "EdgeBetweenness" : 11179.94309384,
        "shared_name" : "Claire_Lynch () Pat_McInerney",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Pat_McInerney",
        "interaction" : "",
        "SUID" : 923,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "922",
        "source" : "268",
        "target" : "232",
        "EdgeBetweenness" : 31473.04396132,
        "shared_name" : "Claire_Lynch () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 922,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "921",
        "source" : "268",
        "target" : "308",
        "EdgeBetweenness" : 13443.58532548,
        "shared_name" : "Claire_Lynch () Terry_Eldredge",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Terry_Eldredge",
        "interaction" : "",
        "SUID" : 921,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "920",
        "source" : "268",
        "target" : "284",
        "EdgeBetweenness" : 31942.14407235,
        "shared_name" : "Claire_Lynch () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 920,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "919",
        "source" : "268",
        "target" : "235",
        "EdgeBetweenness" : 8966.13532367,
        "shared_name" : "Claire_Lynch () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 919,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "918",
        "source" : "268",
        "target" : "74",
        "EdgeBetweenness" : 19187.69743972,
        "shared_name" : "Claire_Lynch () Keith_Little",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Keith_Little",
        "interaction" : "",
        "SUID" : 918,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "917",
        "source" : "268",
        "target" : "79",
        "EdgeBetweenness" : 36254.61957969,
        "shared_name" : "Claire_Lynch () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Sam_Bush",
        "interaction" : "",
        "SUID" : 917,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "916",
        "source" : "268",
        "target" : "273",
        "EdgeBetweenness" : 66030.63501644,
        "shared_name" : "Claire_Lynch () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Claire_Lynch () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 916,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "915",
        "source" : "255",
        "target" : "152",
        "EdgeBetweenness" : 50942.5597342,
        "shared_name" : "Darrell_Webb () Randy_Kohrs",
        "shared_interaction" : "",
        "name" : "Darrell_Webb () Randy_Kohrs",
        "interaction" : "",
        "SUID" : 915,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "914",
        "source" : "255",
        "target" : "161",
        "EdgeBetweenness" : 181479.03568798,
        "shared_name" : "Darrell_Webb () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Darrell_Webb () Dolly_Parton",
        "interaction" : "",
        "SUID" : 914,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "913",
        "source" : "250",
        "target" : "112",
        "EdgeBetweenness" : 503.49277833,
        "shared_name" : "Danny_Roberts () Jimmy_Mattingly",
        "shared_interaction" : "",
        "name" : "Danny_Roberts () Jimmy_Mattingly",
        "interaction" : "",
        "SUID" : 913,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "912",
        "source" : "246",
        "target" : "152",
        "EdgeBetweenness" : 8967.14244393,
        "shared_name" : "Robert_Hale () Randy_Kohrs",
        "shared_interaction" : "",
        "name" : "Robert_Hale () Randy_Kohrs",
        "interaction" : "",
        "SUID" : 912,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "911",
        "source" : "240",
        "target" : "287",
        "EdgeBetweenness" : 30476.89935149,
        "shared_name" : "The_Grascals () George_Jones",
        "shared_interaction" : "",
        "name" : "The_Grascals () George_Jones",
        "interaction" : "",
        "SUID" : 911,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "910",
        "source" : "240",
        "target" : "292",
        "EdgeBetweenness" : 35231.72323528,
        "shared_name" : "The_Grascals () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "The_Grascals () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 910,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "909",
        "source" : "240",
        "target" : "223",
        "EdgeBetweenness" : 50503.43846382,
        "shared_name" : "The_Grascals () Andy_Hall",
        "shared_interaction" : "",
        "name" : "The_Grascals () Andy_Hall",
        "interaction" : "",
        "SUID" : 909,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "908",
        "source" : "240",
        "target" : "295",
        "EdgeBetweenness" : 4322.15446805,
        "shared_name" : "The_Grascals () Bob_Mater",
        "shared_interaction" : "",
        "name" : "The_Grascals () Bob_Mater",
        "interaction" : "",
        "SUID" : 908,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "907",
        "source" : "240",
        "target" : "112",
        "EdgeBetweenness" : 2365.38128128,
        "shared_name" : "The_Grascals () Jimmy_Mattingly",
        "shared_interaction" : "",
        "name" : "The_Grascals () Jimmy_Mattingly",
        "interaction" : "",
        "SUID" : 907,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "906",
        "source" : "240",
        "target" : "306",
        "EdgeBetweenness" : 14804.14454315,
        "shared_name" : "The_Grascals () Bobby_Osborne",
        "shared_interaction" : "",
        "name" : "The_Grascals () Bobby_Osborne",
        "interaction" : "",
        "SUID" : 906,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "905",
        "source" : "240",
        "target" : "161",
        "EdgeBetweenness" : 46375.4912197,
        "shared_name" : "The_Grascals () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "The_Grascals () Dolly_Parton",
        "interaction" : "",
        "SUID" : 905,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "904",
        "source" : "240",
        "target" : "227",
        "EdgeBetweenness" : 6513.95760966,
        "shared_name" : "The_Grascals () David_Talbot",
        "shared_interaction" : "",
        "name" : "The_Grascals () David_Talbot",
        "interaction" : "",
        "SUID" : 904,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "903",
        "source" : "240",
        "target" : "266",
        "EdgeBetweenness" : 2749.8483661,
        "shared_name" : "The_Grascals () Kent_Wells",
        "shared_interaction" : "",
        "name" : "The_Grascals () Kent_Wells",
        "interaction" : "",
        "SUID" : 903,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "902",
        "source" : "240",
        "target" : "308",
        "EdgeBetweenness" : 6174.16725308,
        "shared_name" : "The_Grascals () Terry_Eldredge",
        "shared_interaction" : "",
        "name" : "The_Grascals () Terry_Eldredge",
        "interaction" : "",
        "SUID" : 902,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "901",
        "source" : "240",
        "target" : "288",
        "EdgeBetweenness" : 11090.95052499,
        "shared_name" : "The_Grascals () Jamie_Johnson",
        "shared_interaction" : "",
        "name" : "The_Grascals () Jamie_Johnson",
        "interaction" : "",
        "SUID" : 901,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "900",
        "source" : "240",
        "target" : "250",
        "EdgeBetweenness" : 2134.34779799,
        "shared_name" : "The_Grascals () Danny_Roberts",
        "shared_interaction" : "",
        "name" : "The_Grascals () Danny_Roberts",
        "interaction" : "",
        "SUID" : 900,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "899",
        "source" : "240",
        "target" : "66",
        "EdgeBetweenness" : 3419.64006962,
        "shared_name" : "The_Grascals () Terry_Smith",
        "shared_interaction" : "",
        "name" : "The_Grascals () Terry_Smith",
        "interaction" : "",
        "SUID" : 899,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "898",
        "source" : "240",
        "target" : "265",
        "EdgeBetweenness" : 3561.6674777,
        "shared_name" : "The_Grascals () Steve_Turner",
        "shared_interaction" : "",
        "name" : "The_Grascals () Steve_Turner",
        "interaction" : "",
        "SUID" : 898,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "897",
        "source" : "229",
        "target" : "284",
        "EdgeBetweenness" : 66482.99827078,
        "shared_name" : "Ricky_Skaggs () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 897,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "896",
        "source" : "229",
        "target" : "74",
        "EdgeBetweenness" : 62816.70501954,
        "shared_name" : "Ricky_Skaggs () Keith_Little",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Keith_Little",
        "interaction" : "",
        "SUID" : 896,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "895",
        "source" : "229",
        "target" : "149",
        "EdgeBetweenness" : 15566.95654509,
        "shared_name" : "Ricky_Skaggs () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 895,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "894",
        "source" : "229",
        "target" : "82",
        "EdgeBetweenness" : 15883.82262901,
        "shared_name" : "Ricky_Skaggs () Leland_Sklar",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Leland_Sklar",
        "interaction" : "",
        "SUID" : 894,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "893",
        "source" : "229",
        "target" : "344",
        "EdgeBetweenness" : 9851.67938911,
        "shared_name" : "Ricky_Skaggs () Lisa_Bevill",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Lisa_Bevill",
        "interaction" : "",
        "SUID" : 893,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "892",
        "source" : "229",
        "target" : "293",
        "EdgeBetweenness" : 10590.12145194,
        "shared_name" : "Ricky_Skaggs () Don_Cobb",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Don_Cobb",
        "interaction" : "",
        "SUID" : 892,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "891",
        "source" : "229",
        "target" : "279",
        "EdgeBetweenness" : 17878.72230219,
        "shared_name" : "Ricky_Skaggs () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 891,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "890",
        "source" : "229",
        "target" : "281",
        "EdgeBetweenness" : 32038.52858701,
        "shared_name" : "Ricky_Skaggs () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Paul_Franklin",
        "interaction" : "",
        "SUID" : 890,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "889",
        "source" : "229",
        "target" : "328",
        "EdgeBetweenness" : 9387.14130859,
        "shared_name" : "Ricky_Skaggs () David_Hungate",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () David_Hungate",
        "interaction" : "",
        "SUID" : 889,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "888",
        "source" : "229",
        "target" : "232",
        "EdgeBetweenness" : 39406.3927772,
        "shared_name" : "Ricky_Skaggs () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 888,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "887",
        "source" : "229",
        "target" : "215",
        "EdgeBetweenness" : 123802.99801825,
        "shared_name" : "Ricky_Skaggs () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Alison_Krauss",
        "interaction" : "",
        "SUID" : 887,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "886",
        "source" : "229",
        "target" : "315",
        "EdgeBetweenness" : 20393.99237511,
        "shared_name" : "Ricky_Skaggs () John_Wesley_Ryles",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () John_Wesley_Ryles",
        "interaction" : "",
        "SUID" : 886,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "885",
        "source" : "229",
        "target" : "174",
        "EdgeBetweenness" : 292265.10138211,
        "shared_name" : "Ricky_Skaggs () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 885,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "884",
        "source" : "229",
        "target" : "277",
        "EdgeBetweenness" : 65934.18806367,
        "shared_name" : "Ricky_Skaggs () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Tony_Rice",
        "interaction" : "",
        "SUID" : 884,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "883",
        "source" : "229",
        "target" : "129",
        "EdgeBetweenness" : 9851.67938911,
        "shared_name" : "Ricky_Skaggs () Bob_Bailey",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Bob_Bailey",
        "interaction" : "",
        "SUID" : 883,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "882",
        "source" : "229",
        "target" : "79",
        "EdgeBetweenness" : 100494.77573348,
        "shared_name" : "Ricky_Skaggs () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Sam_Bush",
        "interaction" : "",
        "SUID" : 882,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "881",
        "source" : "229",
        "target" : "273",
        "EdgeBetweenness" : 129990.81251995,
        "shared_name" : "Ricky_Skaggs () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 881,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "880",
        "source" : "229",
        "target" : "75",
        "EdgeBetweenness" : 7944.59413647,
        "shared_name" : "Ricky_Skaggs () Kim_Fleming",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Kim_Fleming",
        "interaction" : "",
        "SUID" : 880,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "879",
        "source" : "229",
        "target" : "247",
        "EdgeBetweenness" : 10440.13788122,
        "shared_name" : "Ricky_Skaggs () Vicki_Hampton",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Vicki_Hampton",
        "interaction" : "",
        "SUID" : 879,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "878",
        "source" : "229",
        "target" : "332",
        "EdgeBetweenness" : 43103.92394203,
        "shared_name" : "Ricky_Skaggs () Patty_Loveless",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Patty_Loveless",
        "interaction" : "",
        "SUID" : 878,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "877",
        "source" : "229",
        "target" : "164",
        "EdgeBetweenness" : 16632.83781468,
        "shared_name" : "Ricky_Skaggs () Rob_McCoury",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Rob_McCoury",
        "interaction" : "",
        "SUID" : 877,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "876",
        "source" : "229",
        "target" : "163",
        "EdgeBetweenness" : 46413.00099437,
        "shared_name" : "Ricky_Skaggs () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 876,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "875",
        "source" : "229",
        "target" : "161",
        "EdgeBetweenness" : 228033.48393703,
        "shared_name" : "Ricky_Skaggs () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Dolly_Parton",
        "interaction" : "",
        "SUID" : 875,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "874",
        "source" : "229",
        "target" : "201",
        "EdgeBetweenness" : 13202.58484119,
        "shared_name" : "Ricky_Skaggs () Lee_Ann_Womack",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Lee_Ann_Womack",
        "interaction" : "",
        "SUID" : 874,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "873",
        "source" : "229",
        "target" : "211",
        "EdgeBetweenness" : 104842.61970628,
        "shared_name" : "Ricky_Skaggs () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 873,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "872",
        "source" : "229",
        "target" : "140",
        "EdgeBetweenness" : 7879.99432463,
        "shared_name" : "Ricky_Skaggs () Rebecca_Lynn_Howard",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Rebecca_Lynn_Howard",
        "interaction" : "",
        "SUID" : 872,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "871",
        "source" : "229",
        "target" : "217",
        "EdgeBetweenness" : 44231.32000159,
        "shared_name" : "Ricky_Skaggs () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 871,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "870",
        "source" : "229",
        "target" : "196",
        "EdgeBetweenness" : 8654.04695359,
        "shared_name" : "Ricky_Skaggs () Paul_Brewster",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Paul_Brewster",
        "interaction" : "",
        "SUID" : 870,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "869",
        "source" : "229",
        "target" : "276",
        "EdgeBetweenness" : 9155.28902795,
        "shared_name" : "Ricky_Skaggs () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Jim_Mills",
        "interaction" : "",
        "SUID" : 869,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "868",
        "source" : "229",
        "target" : "107",
        "EdgeBetweenness" : 21752.24162273,
        "shared_name" : "Ricky_Skaggs () Darrin_Vincent",
        "shared_interaction" : "",
        "name" : "Ricky_Skaggs () Darrin_Vincent",
        "interaction" : "",
        "SUID" : 868,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "867",
        "source" : "219",
        "target" : "273",
        "EdgeBetweenness" : 53053.97310345,
        "shared_name" : "Dan_Tyminski () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 867,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "866",
        "source" : "219",
        "target" : "215",
        "EdgeBetweenness" : 40573.01333269,
        "shared_name" : "Dan_Tyminski () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Alison_Krauss",
        "interaction" : "",
        "SUID" : 866,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "865",
        "source" : "219",
        "target" : "276",
        "EdgeBetweenness" : 3920.21170455,
        "shared_name" : "Dan_Tyminski () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Jim_Mills",
        "interaction" : "",
        "SUID" : 865,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "864",
        "source" : "219",
        "target" : "277",
        "EdgeBetweenness" : 22622.47239441,
        "shared_name" : "Dan_Tyminski () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Tony_Rice",
        "interaction" : "",
        "SUID" : 864,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "863",
        "source" : "219",
        "target" : "194",
        "EdgeBetweenness" : 3734.75241937,
        "shared_name" : "Dan_Tyminski () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Barry_Bales",
        "interaction" : "",
        "SUID" : 863,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "862",
        "source" : "219",
        "target" : "208",
        "EdgeBetweenness" : 10101.61901123,
        "shared_name" : "Dan_Tyminski () Ron_Block",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Ron_Block",
        "interaction" : "",
        "SUID" : 862,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "861",
        "source" : "219",
        "target" : "73",
        "EdgeBetweenness" : 4132.85415146,
        "shared_name" : "Dan_Tyminski () Neal_Cappellino",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Neal_Cappellino",
        "interaction" : "",
        "SUID" : 861,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "860",
        "source" : "219",
        "target" : "114",
        "EdgeBetweenness" : 3619.06809175,
        "shared_name" : "Dan_Tyminski () Gary_Paczosa",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Gary_Paczosa",
        "interaction" : "",
        "SUID" : 860,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "859",
        "source" : "219",
        "target" : "192",
        "EdgeBetweenness" : 8694.84281176,
        "shared_name" : "Dan_Tyminski () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Dan_Tyminski () Adam_Steffey",
        "interaction" : "",
        "SUID" : 859,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "858",
        "source" : "217",
        "target" : "79",
        "EdgeBetweenness" : 21395.79798639,
        "shared_name" : "Bryan_Sutton () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Sam_Bush",
        "interaction" : "",
        "SUID" : 858,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "857",
        "source" : "217",
        "target" : "211",
        "EdgeBetweenness" : 17386.54498129,
        "shared_name" : "Bryan_Sutton () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 857,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "856",
        "source" : "217",
        "target" : "311",
        "EdgeBetweenness" : 8150.2524354,
        "shared_name" : "Bryan_Sutton () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Chris_Thile",
        "interaction" : "",
        "SUID" : 856,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "855",
        "source" : "217",
        "target" : "227",
        "EdgeBetweenness" : 15176.81597885,
        "shared_name" : "Bryan_Sutton () David_Talbot",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () David_Talbot",
        "interaction" : "",
        "SUID" : 855,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "854",
        "source" : "217",
        "target" : "277",
        "EdgeBetweenness" : 26455.8529201,
        "shared_name" : "Bryan_Sutton () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Tony_Rice",
        "interaction" : "",
        "SUID" : 854,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "853",
        "source" : "217",
        "target" : "198",
        "EdgeBetweenness" : 8348.83120198,
        "shared_name" : "Bryan_Sutton () Brent_Truitt",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Brent_Truitt",
        "interaction" : "",
        "SUID" : 853,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "852",
        "source" : "217",
        "target" : "208",
        "EdgeBetweenness" : 9647.08817627,
        "shared_name" : "Bryan_Sutton () Ron_Block",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Ron_Block",
        "interaction" : "",
        "SUID" : 852,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "851",
        "source" : "217",
        "target" : "273",
        "EdgeBetweenness" : 37543.1908745,
        "shared_name" : "Bryan_Sutton () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 851,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "850",
        "source" : "217",
        "target" : "161",
        "EdgeBetweenness" : 78361.31464701,
        "shared_name" : "Bryan_Sutton () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Dolly_Parton",
        "interaction" : "",
        "SUID" : 850,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "849",
        "source" : "217",
        "target" : "229",
        "EdgeBetweenness" : 44231.32000159,
        "shared_name" : "Bryan_Sutton () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 849,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "848",
        "source" : "217",
        "target" : "331",
        "EdgeBetweenness" : 10680.05221247,
        "shared_name" : "Bryan_Sutton () Mike_Snider",
        "shared_interaction" : "",
        "name" : "Bryan_Sutton () Mike_Snider",
        "interaction" : "",
        "SUID" : 848,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "847",
        "source" : "215",
        "target" : "69",
        "EdgeBetweenness" : 10446.92664604,
        "shared_name" : "Alison_Krauss () Steve_Buckingham",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Steve_Buckingham",
        "interaction" : "",
        "SUID" : 847,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "846",
        "source" : "215",
        "target" : "73",
        "EdgeBetweenness" : 12399.39136373,
        "shared_name" : "Alison_Krauss () Neal_Cappellino",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Neal_Cappellino",
        "interaction" : "",
        "SUID" : 846,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "845",
        "source" : "215",
        "target" : "90",
        "EdgeBetweenness" : 18427.77054551,
        "shared_name" : "Alison_Krauss () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 845,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "844",
        "source" : "215",
        "target" : "68",
        "EdgeBetweenness" : 23773.59171455,
        "shared_name" : "Alison_Krauss () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Carl_Jackson",
        "interaction" : "",
        "SUID" : 844,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "843",
        "source" : "215",
        "target" : "97",
        "EdgeBetweenness" : 14622.09361545,
        "shared_name" : "Alison_Krauss () Abraham_Laboriel,_Sr.",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Abraham_Laboriel,_Sr.",
        "interaction" : "",
        "SUID" : 843,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "842",
        "source" : "215",
        "target" : "326",
        "EdgeBetweenness" : 87866.72339845,
        "shared_name" : "Alison_Krauss () Maura_O'Connell",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Maura_O'Connell",
        "interaction" : "",
        "SUID" : 842,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "841",
        "source" : "215",
        "target" : "111",
        "EdgeBetweenness" : 83211.50730087,
        "shared_name" : "Alison_Krauss () Michael_Omartian",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Michael_Omartian",
        "interaction" : "",
        "SUID" : 841,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "840",
        "source" : "215",
        "target" : "277",
        "EdgeBetweenness" : 73002.07849807,
        "shared_name" : "Alison_Krauss () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Tony_Rice",
        "interaction" : "",
        "SUID" : 840,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "839",
        "source" : "215",
        "target" : "217",
        "EdgeBetweenness" : 40068.5293858,
        "shared_name" : "Alison_Krauss () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 839,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "838",
        "source" : "215",
        "target" : "153",
        "EdgeBetweenness" : 13189.85192671,
        "shared_name" : "Alison_Krauss () Jim_Keltner",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Jim_Keltner",
        "interaction" : "",
        "SUID" : 838,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "837",
        "source" : "215",
        "target" : "161",
        "EdgeBetweenness" : 213994.33067879,
        "shared_name" : "Alison_Krauss () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Dolly_Parton",
        "interaction" : "",
        "SUID" : 837,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "836",
        "source" : "215",
        "target" : "336",
        "EdgeBetweenness" : 36523.07348571,
        "shared_name" : "Alison_Krauss () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Matt_Rollings",
        "interaction" : "",
        "SUID" : 836,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "835",
        "source" : "215",
        "target" : "194",
        "EdgeBetweenness" : 15355.94331631,
        "shared_name" : "Alison_Krauss () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Barry_Bales",
        "interaction" : "",
        "SUID" : 835,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "834",
        "source" : "215",
        "target" : "208",
        "EdgeBetweenness" : 32640.389576,
        "shared_name" : "Alison_Krauss () Ron_Block",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Ron_Block",
        "interaction" : "",
        "SUID" : 834,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "833",
        "source" : "215",
        "target" : "145",
        "EdgeBetweenness" : 8458.7446933,
        "shared_name" : "Alison_Krauss () Suzanne_Cox",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Suzanne_Cox",
        "interaction" : "",
        "SUID" : 833,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "832",
        "source" : "215",
        "target" : "211",
        "EdgeBetweenness" : 70568.18932313,
        "shared_name" : "Alison_Krauss () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 832,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "831",
        "source" : "215",
        "target" : "134",
        "EdgeBetweenness" : 18952.35278776,
        "shared_name" : "Alison_Krauss () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 831,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "830",
        "source" : "215",
        "target" : "114",
        "EdgeBetweenness" : 7546.72133674,
        "shared_name" : "Alison_Krauss () Gary_Paczosa",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Gary_Paczosa",
        "interaction" : "",
        "SUID" : 830,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "829",
        "source" : "215",
        "target" : "192",
        "EdgeBetweenness" : 43878.24991675,
        "shared_name" : "Alison_Krauss () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Adam_Steffey",
        "interaction" : "",
        "SUID" : 829,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "828",
        "source" : "215",
        "target" : "127",
        "EdgeBetweenness" : 63718.17634813,
        "shared_name" : "Alison_Krauss () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Harry_Stinson",
        "interaction" : "",
        "SUID" : 828,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "827",
        "source" : "215",
        "target" : "198",
        "EdgeBetweenness" : 23783.39567553,
        "shared_name" : "Alison_Krauss () Brent_Truitt",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Brent_Truitt",
        "interaction" : "",
        "SUID" : 827,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "826",
        "source" : "215",
        "target" : "219",
        "EdgeBetweenness" : 40573.01333269,
        "shared_name" : "Alison_Krauss () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 826,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "825",
        "source" : "215",
        "target" : "79",
        "EdgeBetweenness" : 81082.55168753,
        "shared_name" : "Alison_Krauss () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Sam_Bush",
        "interaction" : "",
        "SUID" : 825,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "824",
        "source" : "215",
        "target" : "273",
        "EdgeBetweenness" : 125108.59466615,
        "shared_name" : "Alison_Krauss () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 824,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "823",
        "source" : "215",
        "target" : "284",
        "EdgeBetweenness" : 80905.61469231,
        "shared_name" : "Alison_Krauss () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Alison_Krauss () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 823,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "822",
        "source" : "211",
        "target" : "79",
        "EdgeBetweenness" : 48972.96450197,
        "shared_name" : "Stuart_Duncan () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Stuart_Duncan () Sam_Bush",
        "interaction" : "",
        "SUID" : 822,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "821",
        "source" : "211",
        "target" : "273",
        "EdgeBetweenness" : 67869.23484749,
        "shared_name" : "Stuart_Duncan () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Stuart_Duncan () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 821,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "820",
        "source" : "211",
        "target" : "235",
        "EdgeBetweenness" : 12357.00966596,
        "shared_name" : "Stuart_Duncan () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Stuart_Duncan () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 820,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "819",
        "source" : "208",
        "target" : "134",
        "EdgeBetweenness" : 4421.88664595,
        "shared_name" : "Ron_Block () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Ron_Block () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 819,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "818",
        "source" : "208",
        "target" : "192",
        "EdgeBetweenness" : 5708.61327825,
        "shared_name" : "Ron_Block () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Ron_Block () Adam_Steffey",
        "interaction" : "",
        "SUID" : 818,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "817",
        "source" : "208",
        "target" : "311",
        "EdgeBetweenness" : 8169.55337848,
        "shared_name" : "Ron_Block () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Ron_Block () Chris_Thile",
        "interaction" : "",
        "SUID" : 817,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "816",
        "source" : "208",
        "target" : "170",
        "EdgeBetweenness" : 28925.53160993,
        "shared_name" : "Ron_Block () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Ron_Block () Sara_Watkins",
        "interaction" : "",
        "SUID" : 816,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "815",
        "source" : "208",
        "target" : "156",
        "EdgeBetweenness" : 11547.74251021,
        "shared_name" : "Ron_Block () Sean_Watkins",
        "shared_interaction" : "",
        "name" : "Ron_Block () Sean_Watkins",
        "interaction" : "",
        "SUID" : 815,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "814",
        "source" : "208",
        "target" : "194",
        "EdgeBetweenness" : 2763.55598954,
        "shared_name" : "Ron_Block () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Ron_Block () Barry_Bales",
        "interaction" : "",
        "SUID" : 814,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "813",
        "source" : "208",
        "target" : "79",
        "EdgeBetweenness" : 26053.33547729,
        "shared_name" : "Ron_Block () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Ron_Block () Sam_Bush",
        "interaction" : "",
        "SUID" : 813,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "812",
        "source" : "208",
        "target" : "145",
        "EdgeBetweenness" : 2481.86632107,
        "shared_name" : "Ron_Block () Suzanne_Cox",
        "shared_interaction" : "",
        "name" : "Ron_Block () Suzanne_Cox",
        "interaction" : "",
        "SUID" : 812,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "811",
        "source" : "208",
        "target" : "273",
        "EdgeBetweenness" : 44541.0979322,
        "shared_name" : "Ron_Block () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Ron_Block () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 811,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "810",
        "source" : "208",
        "target" : "211",
        "EdgeBetweenness" : 25641.35082676,
        "shared_name" : "Ron_Block () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Ron_Block () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 810,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "809",
        "source" : "208",
        "target" : "215",
        "EdgeBetweenness" : 32640.389576,
        "shared_name" : "Ron_Block () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Ron_Block () Alison_Krauss",
        "interaction" : "",
        "SUID" : 809,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "808",
        "source" : "208",
        "target" : "219",
        "EdgeBetweenness" : 10101.61901123,
        "shared_name" : "Ron_Block () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Ron_Block () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 808,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "807",
        "source" : "196",
        "target" : "211",
        "EdgeBetweenness" : 6064.10631046,
        "shared_name" : "Paul_Brewster () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 807,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "806",
        "source" : "196",
        "target" : "276",
        "EdgeBetweenness" : 479.53801481,
        "shared_name" : "Paul_Brewster () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Jim_Mills",
        "interaction" : "",
        "SUID" : 806,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "805",
        "source" : "196",
        "target" : "161",
        "EdgeBetweenness" : 14528.10581803,
        "shared_name" : "Paul_Brewster () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Dolly_Parton",
        "interaction" : "",
        "SUID" : 805,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "804",
        "source" : "196",
        "target" : "229",
        "EdgeBetweenness" : 8654.04695359,
        "shared_name" : "Paul_Brewster () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 804,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "803",
        "source" : "196",
        "target" : "192",
        "EdgeBetweenness" : 2884.92636741,
        "shared_name" : "Paul_Brewster () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Adam_Steffey",
        "interaction" : "",
        "SUID" : 803,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "802",
        "source" : "196",
        "target" : "217",
        "EdgeBetweenness" : 2467.81340125,
        "shared_name" : "Paul_Brewster () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 802,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "801",
        "source" : "196",
        "target" : "107",
        "EdgeBetweenness" : 1006.09207481,
        "shared_name" : "Paul_Brewster () Darrin_Vincent",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Darrin_Vincent",
        "interaction" : "",
        "SUID" : 801,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "800",
        "source" : "196",
        "target" : "201",
        "EdgeBetweenness" : 1754.61716294,
        "shared_name" : "Paul_Brewster () Lee_Ann_Womack",
        "shared_interaction" : "",
        "name" : "Paul_Brewster () Lee_Ann_Womack",
        "interaction" : "",
        "SUID" : 800,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "799",
        "source" : "192",
        "target" : "194",
        "EdgeBetweenness" : 2640.95131095,
        "shared_name" : "Adam_Steffey () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Barry_Bales",
        "interaction" : "",
        "SUID" : 799,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "798",
        "source" : "192",
        "target" : "208",
        "EdgeBetweenness" : 5708.61327825,
        "shared_name" : "Adam_Steffey () Ron_Block",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Ron_Block",
        "interaction" : "",
        "SUID" : 798,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "797",
        "source" : "192",
        "target" : "211",
        "EdgeBetweenness" : 32458.42299264,
        "shared_name" : "Adam_Steffey () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 797,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "796",
        "source" : "192",
        "target" : "152",
        "EdgeBetweenness" : 30022.61489644,
        "shared_name" : "Adam_Steffey () Randy_Kohrs",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Randy_Kohrs",
        "interaction" : "",
        "SUID" : 796,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "795",
        "source" : "192",
        "target" : "215",
        "EdgeBetweenness" : 43878.24991675,
        "shared_name" : "Adam_Steffey () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Alison_Krauss",
        "interaction" : "",
        "SUID" : 795,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "794",
        "source" : "192",
        "target" : "217",
        "EdgeBetweenness" : 10006.20757016,
        "shared_name" : "Adam_Steffey () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 794,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "793",
        "source" : "192",
        "target" : "219",
        "EdgeBetweenness" : 8694.84281176,
        "shared_name" : "Adam_Steffey () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Adam_Steffey () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 793,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "792",
        "source" : "190",
        "target" : "79",
        "EdgeBetweenness" : 77427.39791508,
        "shared_name" : "David_Grisman () Sam_Bush",
        "shared_interaction" : "",
        "name" : "David_Grisman () Sam_Bush",
        "interaction" : "",
        "SUID" : 792,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "791",
        "source" : "190",
        "target" : "211",
        "EdgeBetweenness" : 118778.08418949,
        "shared_name" : "David_Grisman () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "David_Grisman () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 791,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "790",
        "source" : "190",
        "target" : "284",
        "EdgeBetweenness" : 65139.73051723,
        "shared_name" : "David_Grisman () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "David_Grisman () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 790,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "789",
        "source" : "190",
        "target" : "235",
        "EdgeBetweenness" : 25571.01308606,
        "shared_name" : "David_Grisman () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "David_Grisman () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 789,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "788",
        "source" : "190",
        "target" : "282",
        "EdgeBetweenness" : 79775.26810099,
        "shared_name" : "David_Grisman () Herb_Pedersen",
        "shared_interaction" : "",
        "name" : "David_Grisman () Herb_Pedersen",
        "interaction" : "",
        "SUID" : 788,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "787",
        "source" : "190",
        "target" : "277",
        "EdgeBetweenness" : 32928.6782795,
        "shared_name" : "David_Grisman () Tony_Rice",
        "shared_interaction" : "",
        "name" : "David_Grisman () Tony_Rice",
        "interaction" : "",
        "SUID" : 787,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "786",
        "source" : "190",
        "target" : "229",
        "EdgeBetweenness" : 143572.09634414,
        "shared_name" : "David_Grisman () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "David_Grisman () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 786,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "785",
        "source" : "188",
        "target" : "319",
        "EdgeBetweenness" : 76038.26817745,
        "shared_name" : "Willie_Nelson () Kris_Kristofferson",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Kris_Kristofferson",
        "interaction" : "",
        "SUID" : 785,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "784",
        "source" : "188",
        "target" : "232",
        "EdgeBetweenness" : 54696.12674426,
        "shared_name" : "Willie_Nelson () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 784,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "783",
        "source" : "188",
        "target" : "292",
        "EdgeBetweenness" : 94085.15439676,
        "shared_name" : "Willie_Nelson () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 783,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "782",
        "source" : "188",
        "target" : "243",
        "EdgeBetweenness" : 80198.90238674,
        "shared_name" : "Willie_Nelson () Dan_Dugmore",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Dan_Dugmore",
        "interaction" : "",
        "SUID" : 782,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "781",
        "source" : "188",
        "target" : "252",
        "EdgeBetweenness" : 29761.0078597,
        "shared_name" : "Willie_Nelson () Louis_Dean_Nunley",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Louis_Dean_Nunley",
        "interaction" : "",
        "SUID" : 781,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "780",
        "source" : "188",
        "target" : "336",
        "EdgeBetweenness" : 76102.48659369,
        "shared_name" : "Willie_Nelson () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Matt_Rollings",
        "interaction" : "",
        "SUID" : 780,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "779",
        "source" : "188",
        "target" : "213",
        "EdgeBetweenness" : 27943.31942891,
        "shared_name" : "Willie_Nelson () Lisa_Silver",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Lisa_Silver",
        "interaction" : "",
        "SUID" : 779,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "778",
        "source" : "188",
        "target" : "127",
        "EdgeBetweenness" : 81801.92768219,
        "shared_name" : "Willie_Nelson () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Harry_Stinson",
        "interaction" : "",
        "SUID" : 778,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "777",
        "source" : "188",
        "target" : "264",
        "EdgeBetweenness" : 44661.31448625,
        "shared_name" : "Willie_Nelson () Dennis_Wilson",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Dennis_Wilson",
        "interaction" : "",
        "SUID" : 777,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "776",
        "source" : "188",
        "target" : "201",
        "EdgeBetweenness" : 11208.48479508,
        "shared_name" : "Willie_Nelson () Lee_Ann_Womack",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Lee_Ann_Womack",
        "interaction" : "",
        "SUID" : 776,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "775",
        "source" : "188",
        "target" : "207",
        "EdgeBetweenness" : 30681.60227716,
        "shared_name" : "Willie_Nelson () Reggie_Young",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Reggie_Young",
        "interaction" : "",
        "SUID" : 775,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "774",
        "source" : "188",
        "target" : "339",
        "EdgeBetweenness" : 10863.80951305,
        "shared_name" : "Willie_Nelson () Monisa_Angell",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Monisa_Angell",
        "interaction" : "",
        "SUID" : 774,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "773",
        "source" : "188",
        "target" : "239",
        "EdgeBetweenness" : 29005.43891279,
        "shared_name" : "Willie_Nelson () David_Davidson",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () David_Davidson",
        "interaction" : "",
        "SUID" : 773,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "772",
        "source" : "188",
        "target" : "174",
        "EdgeBetweenness" : 483489.91049787,
        "shared_name" : "Willie_Nelson () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 772,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "771",
        "source" : "188",
        "target" : "92",
        "EdgeBetweenness" : 43893.81476038,
        "shared_name" : "Willie_Nelson () Norah_Jones",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Norah_Jones",
        "interaction" : "",
        "SUID" : 771,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "770",
        "source" : "188",
        "target" : "215",
        "EdgeBetweenness" : 385267.37678698,
        "shared_name" : "Willie_Nelson () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Alison_Krauss",
        "interaction" : "",
        "SUID" : 770,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "769",
        "source" : "188",
        "target" : "94",
        "EdgeBetweenness" : 11245.47219456,
        "shared_name" : "Willie_Nelson () Anthony_LaMarchina",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Anthony_LaMarchina",
        "interaction" : "",
        "SUID" : 769,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "768",
        "source" : "188",
        "target" : "161",
        "EdgeBetweenness" : 406699.82981962,
        "shared_name" : "Willie_Nelson () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Dolly_Parton",
        "interaction" : "",
        "SUID" : 768,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "767",
        "source" : "188",
        "target" : "315",
        "EdgeBetweenness" : 18880.45247892,
        "shared_name" : "Willie_Nelson () John_Wesley_Ryles",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () John_Wesley_Ryles",
        "interaction" : "",
        "SUID" : 767,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "766",
        "source" : "188",
        "target" : "149",
        "EdgeBetweenness" : 18886.3133391,
        "shared_name" : "Willie_Nelson () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 766,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "765",
        "source" : "188",
        "target" : "219",
        "EdgeBetweenness" : 217564.76228226,
        "shared_name" : "Willie_Nelson () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 765,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "764",
        "source" : "188",
        "target" : "261",
        "EdgeBetweenness" : 28908.14129889,
        "shared_name" : "Willie_Nelson () Kristin_Wilkinson",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Kristin_Wilkinson",
        "interaction" : "",
        "SUID" : 764,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "763",
        "source" : "188",
        "target" : "279",
        "EdgeBetweenness" : 41235.89266413,
        "shared_name" : "Willie_Nelson () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 763,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "762",
        "source" : "188",
        "target" : "154",
        "EdgeBetweenness" : 8853.63596873,
        "shared_name" : "Willie_Nelson () Don_Potter",
        "shared_interaction" : "",
        "name" : "Willie_Nelson () Don_Potter",
        "interaction" : "",
        "SUID" : 762,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "761",
        "source" : "174",
        "target" : "332",
        "EdgeBetweenness" : 94652.95449171,
        "shared_name" : "Emmylou_Harris () Patty_Loveless",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Patty_Loveless",
        "interaction" : "",
        "SUID" : 761,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "760",
        "source" : "174",
        "target" : "126",
        "EdgeBetweenness" : 21483.22767467,
        "shared_name" : "Emmylou_Harris () Connie_Ellisor",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Connie_Ellisor",
        "interaction" : "",
        "SUID" : 760,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "759",
        "source" : "174",
        "target" : "90",
        "EdgeBetweenness" : 27585.54354036,
        "shared_name" : "Emmylou_Harris () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 759,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "758",
        "source" : "174",
        "target" : "132",
        "EdgeBetweenness" : 16957.42556441,
        "shared_name" : "Emmylou_Harris () Jim_Grosjean",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Jim_Grosjean",
        "interaction" : "",
        "SUID" : 758,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "757",
        "source" : "174",
        "target" : "268",
        "EdgeBetweenness" : 192179.73119899,
        "shared_name" : "Emmylou_Harris () Claire_Lynch",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Claire_Lynch",
        "interaction" : "",
        "SUID" : 757,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "756",
        "source" : "174",
        "target" : "149",
        "EdgeBetweenness" : 18467.93125321,
        "shared_name" : "Emmylou_Harris () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 756,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "755",
        "source" : "174",
        "target" : "160",
        "EdgeBetweenness" : 17558.4150662,
        "shared_name" : "Emmylou_Harris () Gary_VanOsdale",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Gary_VanOsdale",
        "interaction" : "",
        "SUID" : 755,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "754",
        "source" : "174",
        "target" : "261",
        "EdgeBetweenness" : 15869.26158874,
        "shared_name" : "Emmylou_Harris () Kristin_Wilkinson",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Kristin_Wilkinson",
        "interaction" : "",
        "SUID" : 754,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "753",
        "source" : "174",
        "target" : "145",
        "EdgeBetweenness" : 17851.30685518,
        "shared_name" : "Emmylou_Harris () Suzanne_Cox",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Suzanne_Cox",
        "interaction" : "",
        "SUID" : 753,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "752",
        "source" : "174",
        "target" : "264",
        "EdgeBetweenness" : 64706.30449018,
        "shared_name" : "Emmylou_Harris () Dennis_Wilson",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Dennis_Wilson",
        "interaction" : "",
        "SUID" : 752,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "751",
        "source" : "174",
        "target" : "285",
        "EdgeBetweenness" : 43370.86935904,
        "shared_name" : "Emmylou_Harris () Victor_Battista",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Victor_Battista",
        "interaction" : "",
        "SUID" : 751,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "750",
        "source" : "174",
        "target" : "79",
        "EdgeBetweenness" : 281402.8232735,
        "shared_name" : "Emmylou_Harris () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Sam_Bush",
        "interaction" : "",
        "SUID" : 750,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "749",
        "source" : "174",
        "target" : "273",
        "EdgeBetweenness" : 397149.83468311,
        "shared_name" : "Emmylou_Harris () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 749,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "748",
        "source" : "174",
        "target" : "284",
        "EdgeBetweenness" : 125269.32654808,
        "shared_name" : "Emmylou_Harris () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 748,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "747",
        "source" : "174",
        "target" : "190",
        "EdgeBetweenness" : 346181.22691772,
        "shared_name" : "Emmylou_Harris () David_Grisman",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () David_Grisman",
        "interaction" : "",
        "SUID" : 747,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "746",
        "source" : "174",
        "target" : "232",
        "EdgeBetweenness" : 72686.19197543,
        "shared_name" : "Emmylou_Harris () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 746,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "745",
        "source" : "174",
        "target" : "68",
        "EdgeBetweenness" : 98758.95823545,
        "shared_name" : "Emmylou_Harris () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Carl_Jackson",
        "interaction" : "",
        "SUID" : 745,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "744",
        "source" : "174",
        "target" : "287",
        "EdgeBetweenness" : 300210.11950606,
        "shared_name" : "Emmylou_Harris () George_Jones",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () George_Jones",
        "interaction" : "",
        "SUID" : 744,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "743",
        "source" : "174",
        "target" : "153",
        "EdgeBetweenness" : 14597.97742677,
        "shared_name" : "Emmylou_Harris () Jim_Keltner",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Jim_Keltner",
        "interaction" : "",
        "SUID" : 743,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "742",
        "source" : "174",
        "target" : "215",
        "EdgeBetweenness" : 326857.38483443,
        "shared_name" : "Emmylou_Harris () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Alison_Krauss",
        "interaction" : "",
        "SUID" : 742,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "741",
        "source" : "174",
        "target" : "296",
        "EdgeBetweenness" : 215475.97801812,
        "shared_name" : "Emmylou_Harris () David_Lindley",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () David_Lindley",
        "interaction" : "",
        "SUID" : 741,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "740",
        "source" : "174",
        "target" : "167",
        "EdgeBetweenness" : 38047.47200758,
        "shared_name" : "Emmylou_Harris () Pat_McInerney",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Pat_McInerney",
        "interaction" : "",
        "SUID" : 740,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "739",
        "source" : "174",
        "target" : "188",
        "EdgeBetweenness" : 483489.91049787,
        "shared_name" : "Emmylou_Harris () Willie_Nelson",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Willie_Nelson",
        "interaction" : "",
        "SUID" : 739,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "738",
        "source" : "174",
        "target" : "161",
        "EdgeBetweenness" : 506615.4941653,
        "shared_name" : "Emmylou_Harris () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Dolly_Parton",
        "interaction" : "",
        "SUID" : 738,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "737",
        "source" : "174",
        "target" : "226",
        "EdgeBetweenness" : 21816.30006938,
        "shared_name" : "Emmylou_Harris () Bill_Payne",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Bill_Payne",
        "interaction" : "",
        "SUID" : 737,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "736",
        "source" : "174",
        "target" : "282",
        "EdgeBetweenness" : 166095.41836663,
        "shared_name" : "Emmylou_Harris () Herb_Pedersen",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Herb_Pedersen",
        "interaction" : "",
        "SUID" : 736,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "735",
        "source" : "174",
        "target" : "277",
        "EdgeBetweenness" : 225735.6612466,
        "shared_name" : "Emmylou_Harris () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Tony_Rice",
        "interaction" : "",
        "SUID" : 735,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "734",
        "source" : "174",
        "target" : "316",
        "EdgeBetweenness" : 59301.7511848,
        "shared_name" : "Emmylou_Harris () Randy_Scruggs",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Randy_Scruggs",
        "interaction" : "",
        "SUID" : 734,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "733",
        "source" : "174",
        "target" : "229",
        "EdgeBetweenness" : 292265.10138211,
        "shared_name" : "Emmylou_Harris () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 733,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "732",
        "source" : "174",
        "target" : "127",
        "EdgeBetweenness" : 89600.4481219,
        "shared_name" : "Emmylou_Harris () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Harry_Stinson",
        "interaction" : "",
        "SUID" : 732,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "731",
        "source" : "174",
        "target" : "265",
        "EdgeBetweenness" : 10802.77010981,
        "shared_name" : "Emmylou_Harris () Steve_Turner",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Steve_Turner",
        "interaction" : "",
        "SUID" : 731,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "730",
        "source" : "174",
        "target" : "243",
        "EdgeBetweenness" : 81279.70906267,
        "shared_name" : "Emmylou_Harris () Dan_Dugmore",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Dan_Dugmore",
        "interaction" : "",
        "SUID" : 730,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "729",
        "source" : "174",
        "target" : "211",
        "EdgeBetweenness" : 343291.55152617,
        "shared_name" : "Emmylou_Harris () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 729,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "728",
        "source" : "174",
        "target" : "281",
        "EdgeBetweenness" : 70540.75685958,
        "shared_name" : "Emmylou_Harris () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Paul_Franklin",
        "interaction" : "",
        "SUID" : 728,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "727",
        "source" : "174",
        "target" : "136",
        "EdgeBetweenness" : 24553.65299272,
        "shared_name" : "Emmylou_Harris () Jim_Horn",
        "shared_interaction" : "",
        "name" : "Emmylou_Harris () Jim_Horn",
        "interaction" : "",
        "SUID" : 727,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "726",
        "source" : "172",
        "target" : "285",
        "EdgeBetweenness" : 26217.41180216,
        "shared_name" : "Rhonda_Vincent () Victor_Battista",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Victor_Battista",
        "interaction" : "",
        "SUID" : 726,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "725",
        "source" : "172",
        "target" : "306",
        "EdgeBetweenness" : 43584.42894453,
        "shared_name" : "Rhonda_Vincent () Bobby_Osborne",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Bobby_Osborne",
        "interaction" : "",
        "SUID" : 725,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "724",
        "source" : "172",
        "target" : "196",
        "EdgeBetweenness" : 8395.05231598,
        "shared_name" : "Rhonda_Vincent () Paul_Brewster",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Paul_Brewster",
        "interaction" : "",
        "SUID" : 724,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "723",
        "source" : "172",
        "target" : "223",
        "EdgeBetweenness" : 179092.2186761,
        "shared_name" : "Rhonda_Vincent () Andy_Hall",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Andy_Hall",
        "interaction" : "",
        "SUID" : 723,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "722",
        "source" : "172",
        "target" : "274",
        "EdgeBetweenness" : 9014.06395653,
        "shared_name" : "Rhonda_Vincent () Becky_Isaacs_Bowman",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Becky_Isaacs_Bowman",
        "interaction" : "",
        "SUID" : 722,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "721",
        "source" : "172",
        "target" : "192",
        "EdgeBetweenness" : 25259.45238665,
        "shared_name" : "Rhonda_Vincent () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Adam_Steffey",
        "interaction" : "",
        "SUID" : 721,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "720",
        "source" : "172",
        "target" : "118",
        "EdgeBetweenness" : 9529.6416934,
        "shared_name" : "Rhonda_Vincent () Keith_Urban",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Keith_Urban",
        "interaction" : "",
        "SUID" : 720,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "719",
        "source" : "172",
        "target" : "273",
        "EdgeBetweenness" : 187418.36978736,
        "shared_name" : "Rhonda_Vincent () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 719,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "718",
        "source" : "172",
        "target" : "284",
        "EdgeBetweenness" : 77475.4256736,
        "shared_name" : "Rhonda_Vincent () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 718,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "717",
        "source" : "172",
        "target" : "79",
        "EdgeBetweenness" : 107585.64953372,
        "shared_name" : "Rhonda_Vincent () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Sam_Bush",
        "interaction" : "",
        "SUID" : 717,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "716",
        "source" : "172",
        "target" : "159",
        "EdgeBetweenness" : 21789.20086214,
        "shared_name" : "Rhonda_Vincent () Sonya_Isaacs",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Sonya_Isaacs",
        "interaction" : "",
        "SUID" : 716,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "715",
        "source" : "172",
        "target" : "276",
        "EdgeBetweenness" : 11647.6626802,
        "shared_name" : "Rhonda_Vincent () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Jim_Mills",
        "interaction" : "",
        "SUID" : 715,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "714",
        "source" : "172",
        "target" : "217",
        "EdgeBetweenness" : 35568.8148396,
        "shared_name" : "Rhonda_Vincent () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 714,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "713",
        "source" : "172",
        "target" : "68",
        "EdgeBetweenness" : 22035.0916562,
        "shared_name" : "Rhonda_Vincent () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Carl_Jackson",
        "interaction" : "",
        "SUID" : 713,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "712",
        "source" : "172",
        "target" : "152",
        "EdgeBetweenness" : 88602.48283747,
        "shared_name" : "Rhonda_Vincent () Randy_Kohrs",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Randy_Kohrs",
        "interaction" : "",
        "SUID" : 712,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "711",
        "source" : "172",
        "target" : "281",
        "EdgeBetweenness" : 34851.92623725,
        "shared_name" : "Rhonda_Vincent () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Paul_Franklin",
        "interaction" : "",
        "SUID" : 711,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "710",
        "source" : "172",
        "target" : "215",
        "EdgeBetweenness" : 142672.77537775,
        "shared_name" : "Rhonda_Vincent () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Alison_Krauss",
        "interaction" : "",
        "SUID" : 710,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "709",
        "source" : "172",
        "target" : "161",
        "EdgeBetweenness" : 185352.8246653,
        "shared_name" : "Rhonda_Vincent () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Dolly_Parton",
        "interaction" : "",
        "SUID" : 709,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "708",
        "source" : "172",
        "target" : "292",
        "EdgeBetweenness" : 110218.72806737,
        "shared_name" : "Rhonda_Vincent () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 708,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "707",
        "source" : "172",
        "target" : "264",
        "EdgeBetweenness" : 23941.13013978,
        "shared_name" : "Rhonda_Vincent () Dennis_Wilson",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Dennis_Wilson",
        "interaction" : "",
        "SUID" : 707,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "706",
        "source" : "172",
        "target" : "279",
        "EdgeBetweenness" : 29452.36204027,
        "shared_name" : "Rhonda_Vincent () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 706,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "705",
        "source" : "172",
        "target" : "130",
        "EdgeBetweenness" : 11731.19969428,
        "shared_name" : "Rhonda_Vincent () Mark_Casstevens",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Mark_Casstevens",
        "interaction" : "",
        "SUID" : 705,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "704",
        "source" : "172",
        "target" : "243",
        "EdgeBetweenness" : 63236.99593797,
        "shared_name" : "Rhonda_Vincent () Dan_Dugmore",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Dan_Dugmore",
        "interaction" : "",
        "SUID" : 704,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "703",
        "source" : "172",
        "target" : "328",
        "EdgeBetweenness" : 13446.10317162,
        "shared_name" : "Rhonda_Vincent () David_Hungate",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () David_Hungate",
        "interaction" : "",
        "SUID" : 703,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "702",
        "source" : "172",
        "target" : "336",
        "EdgeBetweenness" : 48731.27624294,
        "shared_name" : "Rhonda_Vincent () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Matt_Rollings",
        "interaction" : "",
        "SUID" : 702,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "701",
        "source" : "172",
        "target" : "207",
        "EdgeBetweenness" : 80103.80534843,
        "shared_name" : "Rhonda_Vincent () Reggie_Young",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Reggie_Young",
        "interaction" : "",
        "SUID" : 701,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "700",
        "source" : "172",
        "target" : "272",
        "EdgeBetweenness" : 26427.43431782,
        "shared_name" : "Rhonda_Vincent () Bill_Anderson",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Bill_Anderson",
        "interaction" : "",
        "SUID" : 700,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "699",
        "source" : "172",
        "target" : "211",
        "EdgeBetweenness" : 110905.1320881,
        "shared_name" : "Rhonda_Vincent () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 699,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "698",
        "source" : "172",
        "target" : "287",
        "EdgeBetweenness" : 141430.3340591,
        "shared_name" : "Rhonda_Vincent () George_Jones",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () George_Jones",
        "interaction" : "",
        "SUID" : 698,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "697",
        "source" : "172",
        "target" : "107",
        "EdgeBetweenness" : 7275.36979172,
        "shared_name" : "Rhonda_Vincent () Darrin_Vincent",
        "shared_interaction" : "",
        "name" : "Rhonda_Vincent () Darrin_Vincent",
        "interaction" : "",
        "SUID" : 697,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "696",
        "source" : "170",
        "target" : "209",
        "EdgeBetweenness" : 38319.83539775,
        "shared_name" : "Sara_Watkins () Byron_House",
        "shared_interaction" : "",
        "name" : "Sara_Watkins () Byron_House",
        "interaction" : "",
        "SUID" : 696,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "695",
        "source" : "170",
        "target" : "268",
        "EdgeBetweenness" : 51706.32150041,
        "shared_name" : "Sara_Watkins () Claire_Lynch",
        "shared_interaction" : "",
        "name" : "Sara_Watkins () Claire_Lynch",
        "interaction" : "",
        "SUID" : 695,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "694",
        "source" : "170",
        "target" : "163",
        "EdgeBetweenness" : 46528.32599394,
        "shared_name" : "Sara_Watkins () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Sara_Watkins () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 694,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "693",
        "source" : "170",
        "target" : "311",
        "EdgeBetweenness" : 21559.81829216,
        "shared_name" : "Sara_Watkins () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Sara_Watkins () Chris_Thile",
        "interaction" : "",
        "SUID" : 693,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "692",
        "source" : "170",
        "target" : "156",
        "EdgeBetweenness" : 9987.7735761,
        "shared_name" : "Sara_Watkins () Sean_Watkins",
        "shared_interaction" : "",
        "name" : "Sara_Watkins () Sean_Watkins",
        "interaction" : "",
        "SUID" : 692,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "691",
        "source" : "163",
        "target" : "190",
        "EdgeBetweenness" : 44264.47503291,
        "shared_name" : "Ronnie_McCoury () David_Grisman",
        "shared_interaction" : "",
        "name" : "Ronnie_McCoury () David_Grisman",
        "interaction" : "",
        "SUID" : 691,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "690",
        "source" : "163",
        "target" : "273",
        "EdgeBetweenness" : 51669.19675818,
        "shared_name" : "Ronnie_McCoury () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Ronnie_McCoury () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 690,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "689",
        "source" : "163",
        "target" : "211",
        "EdgeBetweenness" : 37328.79349058,
        "shared_name" : "Ronnie_McCoury () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Ronnie_McCoury () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 689,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "688",
        "source" : "163",
        "target" : "164",
        "EdgeBetweenness" : 2460.668255,
        "shared_name" : "Ronnie_McCoury () Rob_McCoury",
        "shared_interaction" : "",
        "name" : "Ronnie_McCoury () Rob_McCoury",
        "interaction" : "",
        "SUID" : 688,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "687",
        "source" : "161",
        "target" : "165",
        "EdgeBetweenness" : 51451.99457974,
        "shared_name" : "Dolly_Parton () Jeff_Baxter",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jeff_Baxter",
        "interaction" : "",
        "SUID" : 687,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "686",
        "source" : "161",
        "target" : "168",
        "EdgeBetweenness" : 32050.49639136,
        "shared_name" : "Dolly_Parton () Larry_Carlton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Larry_Carlton",
        "interaction" : "",
        "SUID" : 686,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "685",
        "source" : "161",
        "target" : "171",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Leonard_Castro",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Leonard_Castro",
        "interaction" : "",
        "SUID" : 685,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "684",
        "source" : "161",
        "target" : "173",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () George_Corsillo",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () George_Corsillo",
        "interaction" : "",
        "SUID" : 684,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "683",
        "source" : "161",
        "target" : "177",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Claudia_Depkin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Claudia_Depkin",
        "interaction" : "",
        "SUID" : 683,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "682",
        "source" : "161",
        "target" : "178",
        "EdgeBetweenness" : 31563.66664234,
        "shared_name" : "Dolly_Parton () Mandana_Eidgah",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mandana_Eidgah",
        "interaction" : "",
        "SUID" : 682,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "681",
        "source" : "161",
        "target" : "179",
        "EdgeBetweenness" : 31563.66664234,
        "shared_name" : "Dolly_Parton () Joanne_Feltman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joanne_Feltman",
        "interaction" : "",
        "SUID" : 681,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "680",
        "source" : "161",
        "target" : "180",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () John_Goux",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () John_Goux",
        "interaction" : "",
        "SUID" : 680,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "679",
        "source" : "161",
        "target" : "181",
        "EdgeBetweenness" : 25240.55304619,
        "shared_name" : "Dolly_Parton () Jerry_Hey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jerry_Hey",
        "interaction" : "",
        "SUID" : 679,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "678",
        "source" : "161",
        "target" : "182",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Kim_Hutchcroft",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kim_Hutchcroft",
        "interaction" : "",
        "SUID" : 678,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "677",
        "source" : "161",
        "target" : "318",
        "EdgeBetweenness" : 21982.86590596,
        "shared_name" : "Dolly_Parton () Larry_Knechtel",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Larry_Knechtel",
        "interaction" : "",
        "SUID" : 677,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "676",
        "source" : "161",
        "target" : "63",
        "EdgeBetweenness" : 40961.36636327,
        "shared_name" : "Dolly_Parton () Glenn_Korman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Glenn_Korman",
        "interaction" : "",
        "SUID" : 676,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "675",
        "source" : "161",
        "target" : "97",
        "EdgeBetweenness" : 9965.90638455,
        "shared_name" : "Dolly_Parton () Abraham_Laboriel,_Sr.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Abraham_Laboriel,_Sr.",
        "interaction" : "",
        "SUID" : 675,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "674",
        "source" : "161",
        "target" : "183",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mary_Malin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mary_Malin",
        "interaction" : "",
        "SUID" : 674,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "673",
        "source" : "161",
        "target" : "184",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Denise_Maynelli",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Denise_Maynelli",
        "interaction" : "",
        "SUID" : 673,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "672",
        "source" : "161",
        "target" : "186",
        "EdgeBetweenness" : 20368.83527663,
        "shared_name" : "Dolly_Parton () Ron_Oates",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ron_Oates",
        "interaction" : "",
        "SUID" : 672,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "671",
        "source" : "161",
        "target" : "306",
        "EdgeBetweenness" : 84510.31348571,
        "shared_name" : "Dolly_Parton () Bobby_Osborne",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bobby_Osborne",
        "interaction" : "",
        "SUID" : 671,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "670",
        "source" : "161",
        "target" : "307",
        "EdgeBetweenness" : 26534.19860781,
        "shared_name" : "Dolly_Parton () Sonny_Osborne",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sonny_Osborne",
        "interaction" : "",
        "SUID" : 670,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "669",
        "source" : "161",
        "target" : "189",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Colleen_Owens",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Colleen_Owens",
        "interaction" : "",
        "SUID" : 669,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "668",
        "source" : "161",
        "target" : "193",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mike_Post",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mike_Post",
        "interaction" : "",
        "SUID" : 668,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "667",
        "source" : "161",
        "target" : "195",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mary_Ellen_Quinn",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mary_Ellen_Quinn",
        "interaction" : "",
        "SUID" : 667,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "666",
        "source" : "161",
        "target" : "197",
        "EdgeBetweenness" : 25240.55304619,
        "shared_name" : "Dolly_Parton () William_Frank_\"Bill\"_Reichenbach_Jr.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () William_Frank_\"Bill\"_Reichenbach_Jr.",
        "interaction" : "",
        "SUID" : 666,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "665",
        "source" : "161",
        "target" : "199",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Tom_Saviano",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tom_Saviano",
        "interaction" : "",
        "SUID" : 665,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "664",
        "source" : "161",
        "target" : "200",
        "EdgeBetweenness" : 19808.96034238,
        "shared_name" : "Dolly_Parton () Joey_Scarbury",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joey_Scarbury",
        "interaction" : "",
        "SUID" : 664,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "663",
        "source" : "161",
        "target" : "202",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Richard_Schlosser",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Richard_Schlosser",
        "interaction" : "",
        "SUID" : 663,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "662",
        "source" : "161",
        "target" : "82",
        "EdgeBetweenness" : 19901.3058249,
        "shared_name" : "Dolly_Parton () Leland_Sklar",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Leland_Sklar",
        "interaction" : "",
        "SUID" : 662,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "661",
        "source" : "161",
        "target" : "205",
        "EdgeBetweenness" : 29041.09388844,
        "shared_name" : "Dolly_Parton () Tom_Tierney",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tom_Tierney",
        "interaction" : "",
        "SUID" : 661,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "660",
        "source" : "161",
        "target" : "206",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Ian_Underwood",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ian_Underwood",
        "interaction" : "",
        "SUID" : 660,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "659",
        "source" : "161",
        "target" : "210",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Marty_Walsh",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Marty_Walsh",
        "interaction" : "",
        "SUID" : 659,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "658",
        "source" : "161",
        "target" : "263",
        "EdgeBetweenness" : 20477.98207137,
        "shared_name" : "Dolly_Parton () David_Richman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Richman",
        "interaction" : "",
        "SUID" : 658,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "657",
        "source" : "161",
        "target" : "162",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Anita_Ball",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Anita_Ball",
        "interaction" : "",
        "SUID" : 657,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "656",
        "source" : "161",
        "target" : "212",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Harry_Bluestone",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Harry_Bluestone",
        "interaction" : "",
        "SUID" : 656,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "655",
        "source" : "161",
        "target" : "214",
        "EdgeBetweenness" : 40474.60784382,
        "shared_name" : "Dolly_Parton () Lenny_Castro",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lenny_Castro",
        "interaction" : "",
        "SUID" : 655,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "654",
        "source" : "161",
        "target" : "175",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Frank_DeCaro",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Frank_DeCaro",
        "interaction" : "",
        "SUID" : 654,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "653",
        "source" : "161",
        "target" : "216",
        "EdgeBetweenness" : 16441.11163259,
        "shared_name" : "Dolly_Parton () Quitman_Dennis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Quitman_Dennis",
        "interaction" : "",
        "SUID" : 653,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "652",
        "source" : "161",
        "target" : "218",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Earl_Dumler",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Earl_Dumler",
        "interaction" : "",
        "SUID" : 652,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "651",
        "source" : "161",
        "target" : "220",
        "EdgeBetweenness" : 25240.55304619,
        "shared_name" : "Dolly_Parton () Chuck_Findley",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chuck_Findley",
        "interaction" : "",
        "SUID" : 651,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "650",
        "source" : "161",
        "target" : "221",
        "EdgeBetweenness" : 20413.71159635,
        "shared_name" : "Dolly_Parton () Roy_Galloway",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Roy_Galloway",
        "interaction" : "",
        "SUID" : 650,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "649",
        "source" : "161",
        "target" : "190",
        "EdgeBetweenness" : 219888.00021209,
        "shared_name" : "Dolly_Parton () David_Grisman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Grisman",
        "interaction" : "",
        "SUID" : 649,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "648",
        "source" : "161",
        "target" : "222",
        "EdgeBetweenness" : 16720.51963255,
        "shared_name" : "Dolly_Parton () Gary_Herbig",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gary_Herbig",
        "interaction" : "",
        "SUID" : 648,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "647",
        "source" : "161",
        "target" : "136",
        "EdgeBetweenness" : 20459.08032591,
        "shared_name" : "Dolly_Parton () Jim_Horn",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jim_Horn",
        "interaction" : "",
        "SUID" : 647,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "646",
        "source" : "161",
        "target" : "153",
        "EdgeBetweenness" : 31945.48952148,
        "shared_name" : "Dolly_Parton () Jim_Keltner",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jim_Keltner",
        "interaction" : "",
        "SUID" : 646,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "645",
        "source" : "161",
        "target" : "185",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Joe_McGuffee",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joe_McGuffee",
        "interaction" : "",
        "SUID" : 645,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "644",
        "source" : "161",
        "target" : "224",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Stephen_Munns",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Stephen_Munns",
        "interaction" : "",
        "SUID" : 644,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "643",
        "source" : "161",
        "target" : "111",
        "EdgeBetweenness" : 60239.95196682,
        "shared_name" : "Dolly_Parton () Michael_Omartian",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Michael_Omartian",
        "interaction" : "",
        "SUID" : 643,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "642",
        "source" : "161",
        "target" : "226",
        "EdgeBetweenness" : 22549.80847928,
        "shared_name" : "Dolly_Parton () Bill_Payne",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bill_Payne",
        "interaction" : "",
        "SUID" : 642,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "641",
        "source" : "161",
        "target" : "282",
        "EdgeBetweenness" : 168801.35105692,
        "shared_name" : "Dolly_Parton () Herb_Pedersen",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Herb_Pedersen",
        "interaction" : "",
        "SUID" : 641,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "640",
        "source" : "161",
        "target" : "191",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Gregg_Perry",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gregg_Perry",
        "interaction" : "",
        "SUID" : 640,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "639",
        "source" : "161",
        "target" : "228",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Dorothy_Remsen",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dorothy_Remsen",
        "interaction" : "",
        "SUID" : 639,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "638",
        "source" : "161",
        "target" : "203",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Sid_Sharp",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sid_Sharp",
        "interaction" : "",
        "SUID" : 638,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "637",
        "source" : "161",
        "target" : "229",
        "EdgeBetweenness" : 228033.48393703,
        "shared_name" : "Dolly_Parton () Ricky_Skaggs",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ricky_Skaggs",
        "interaction" : "",
        "SUID" : 637,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "636",
        "source" : "161",
        "target" : "204",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Stephanie_Spruill",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Stephanie_Spruill",
        "interaction" : "",
        "SUID" : 636,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "635",
        "source" : "161",
        "target" : "230",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Julia_Waters",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Julia_Waters",
        "interaction" : "",
        "SUID" : 635,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "634",
        "source" : "161",
        "target" : "231",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Maxine_Willard_Waters",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Maxine_Willard_Waters",
        "interaction" : "",
        "SUID" : 634,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "633",
        "source" : "161",
        "target" : "234",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Eric_Bennett",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Eric_Bennett",
        "interaction" : "",
        "SUID" : 633,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "632",
        "source" : "161",
        "target" : "236",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Blueniques",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Blueniques",
        "interaction" : "",
        "SUID" : 632,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "631",
        "source" : "161",
        "target" : "237",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Bambi_Breakstone",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bambi_Breakstone",
        "interaction" : "",
        "SUID" : 631,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "630",
        "source" : "161",
        "target" : "147",
        "EdgeBetweenness" : 111162.74468841,
        "shared_name" : "Dolly_Parton () Bob_Carlin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bob_Carlin",
        "interaction" : "",
        "SUID" : 630,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "629",
        "source" : "161",
        "target" : "238",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Gary_\"Biscuit\"_Davis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gary_\"Biscuit\"_Davis",
        "interaction" : "",
        "SUID" : 629,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "628",
        "source" : "161",
        "target" : "241",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Gary_D._Davis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gary_D._Davis",
        "interaction" : "",
        "SUID" : 628,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "627",
        "source" : "161",
        "target" : "244",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Steve_French",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Steve_French",
        "interaction" : "",
        "SUID" : 627,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "626",
        "source" : "161",
        "target" : "246",
        "EdgeBetweenness" : 27660.17402382,
        "shared_name" : "Dolly_Parton () Robert_Hale",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Robert_Hale",
        "interaction" : "",
        "SUID" : 626,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "625",
        "source" : "161",
        "target" : "152",
        "EdgeBetweenness" : 219045.42259886,
        "shared_name" : "Dolly_Parton () Randy_Kohrs",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Randy_Kohrs",
        "interaction" : "",
        "SUID" : 625,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "624",
        "source" : "161",
        "target" : "249",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Arthur_Rice",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Arthur_Rice",
        "interaction" : "",
        "SUID" : 624,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "623",
        "source" : "161",
        "target" : "254",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Keith_Rogers",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Keith_Rogers",
        "interaction" : "",
        "SUID" : 623,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "622",
        "source" : "161",
        "target" : "257",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () April_Stevens",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () April_Stevens",
        "interaction" : "",
        "SUID" : 622,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "621",
        "source" : "161",
        "target" : "258",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Beth_Stevens",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Beth_Stevens",
        "interaction" : "",
        "SUID" : 621,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "620",
        "source" : "161",
        "target" : "260",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () David_Sutton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Sutton",
        "interaction" : "",
        "SUID" : 620,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "619",
        "source" : "161",
        "target" : "262",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Virginia_Team",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Virginia_Team",
        "interaction" : "",
        "SUID" : 619,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "618",
        "source" : "161",
        "target" : "198",
        "EdgeBetweenness" : 33194.20827617,
        "shared_name" : "Dolly_Parton () Brent_Truitt",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Brent_Truitt",
        "interaction" : "",
        "SUID" : 618,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "617",
        "source" : "161",
        "target" : "265",
        "EdgeBetweenness" : 6763.67231128,
        "shared_name" : "Dolly_Parton () Steve_Turner",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Steve_Turner",
        "interaction" : "",
        "SUID" : 617,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "616",
        "source" : "161",
        "target" : "255",
        "EdgeBetweenness" : 181479.03568798,
        "shared_name" : "Dolly_Parton () Darrell_Webb",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Darrell_Webb",
        "interaction" : "",
        "SUID" : 616,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "615",
        "source" : "161",
        "target" : "320",
        "EdgeBetweenness" : 21345.11826086,
        "shared_name" : "Dolly_Parton () Monty_Allen",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Monty_Allen",
        "interaction" : "",
        "SUID" : 615,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "614",
        "source" : "161",
        "target" : "269",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Marc_Campbell",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Marc_Campbell",
        "interaction" : "",
        "SUID" : 614,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "613",
        "source" : "161",
        "target" : "73",
        "EdgeBetweenness" : 9276.94683514,
        "shared_name" : "Dolly_Parton () Neal_Cappellino",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Neal_Cappellino",
        "interaction" : "",
        "SUID" : 613,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "612",
        "source" : "161",
        "target" : "271",
        "EdgeBetweenness" : 19080.7818794,
        "shared_name" : "Dolly_Parton () Chip_Davis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chip_Davis",
        "interaction" : "",
        "SUID" : 612,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "611",
        "source" : "161",
        "target" : "209",
        "EdgeBetweenness" : 91909.16699188,
        "shared_name" : "Dolly_Parton () Byron_House",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Byron_House",
        "interaction" : "",
        "SUID" : 611,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "610",
        "source" : "161",
        "target" : "140",
        "EdgeBetweenness" : 11141.83411683,
        "shared_name" : "Dolly_Parton () Rebecca_Lynn_Howard",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rebecca_Lynn_Howard",
        "interaction" : "",
        "SUID" : 610,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "609",
        "source" : "161",
        "target" : "274",
        "EdgeBetweenness" : 14913.32219375,
        "shared_name" : "Dolly_Parton () Becky_Isaacs_Bowman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Becky_Isaacs_Bowman",
        "interaction" : "",
        "SUID" : 609,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "608",
        "source" : "161",
        "target" : "159",
        "EdgeBetweenness" : 61988.05968269,
        "shared_name" : "Dolly_Parton () Sonya_Isaacs",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sonya_Isaacs",
        "interaction" : "",
        "SUID" : 608,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "607",
        "source" : "161",
        "target" : "337",
        "EdgeBetweenness" : 23532.43718056,
        "shared_name" : "Dolly_Parton () Mark_Kelly",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mark_Kelly",
        "interaction" : "",
        "SUID" : 607,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "606",
        "source" : "161",
        "target" : "139",
        "EdgeBetweenness" : 44835.91401179,
        "shared_name" : "Dolly_Parton () John_Mock",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () John_Mock",
        "interaction" : "",
        "SUID" : 606,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "605",
        "source" : "161",
        "target" : "326",
        "EdgeBetweenness" : 104559.96911799,
        "shared_name" : "Dolly_Parton () Maura_O'Connell",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Maura_O'Connell",
        "interaction" : "",
        "SUID" : 605,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "604",
        "source" : "161",
        "target" : "343",
        "EdgeBetweenness" : 23532.43718056,
        "shared_name" : "Dolly_Parton () Proinsias_O'Maonaigh",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Proinsias_O'Maonaigh",
        "interaction" : "",
        "SUID" : 604,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "603",
        "source" : "161",
        "target" : "114",
        "EdgeBetweenness" : 21050.30810766,
        "shared_name" : "Dolly_Parton () Gary_Paczosa",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gary_Paczosa",
        "interaction" : "",
        "SUID" : 603,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "602",
        "source" : "161",
        "target" : "278",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Barbara_Richardson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Barbara_Richardson",
        "interaction" : "",
        "SUID" : 602,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "601",
        "source" : "161",
        "target" : "331",
        "EdgeBetweenness" : 13907.94778753,
        "shared_name" : "Dolly_Parton () Mike_Snider",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mike_Snider",
        "interaction" : "",
        "SUID" : 601,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "600",
        "source" : "161",
        "target" : "321",
        "EdgeBetweenness" : 379285.84921863,
        "shared_name" : "Dolly_Parton () Altan",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Altan",
        "interaction" : "",
        "SUID" : 600,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "599",
        "source" : "161",
        "target" : "283",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Ricky_Baker",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ricky_Baker",
        "interaction" : "",
        "SUID" : 599,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "598",
        "source" : "161",
        "target" : "285",
        "EdgeBetweenness" : 28751.81304831,
        "shared_name" : "Dolly_Parton () Victor_Battista",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Victor_Battista",
        "interaction" : "",
        "SUID" : 598,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "597",
        "source" : "161",
        "target" : "208",
        "EdgeBetweenness" : 42989.76633725,
        "shared_name" : "Dolly_Parton () Ron_Block",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ron_Block",
        "interaction" : "",
        "SUID" : 597,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "596",
        "source" : "161",
        "target" : "323",
        "EdgeBetweenness" : 317630.95478921,
        "shared_name" : "Dolly_Parton () Dermot_Byrne",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dermot_Byrne",
        "interaction" : "",
        "SUID" : 596,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "595",
        "source" : "161",
        "target" : "72",
        "EdgeBetweenness" : 23532.43718056,
        "shared_name" : "Dolly_Parton () The_Dermot_Byrne_Blues_Combo",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () The_Dermot_Byrne_Blues_Combo",
        "interaction" : "",
        "SUID" : 595,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "594",
        "source" : "161",
        "target" : "348",
        "EdgeBetweenness" : 28547.65964934,
        "shared_name" : "Dolly_Parton () Jere_Carr_II",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jere_Carr_II",
        "interaction" : "",
        "SUID" : 594,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "593",
        "source" : "161",
        "target" : "290",
        "EdgeBetweenness" : 26676.30702013,
        "shared_name" : "Dolly_Parton () Michael_Clark",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Michael_Clark",
        "interaction" : "",
        "SUID" : 593,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "592",
        "source" : "161",
        "target" : "293",
        "EdgeBetweenness" : 11286.94840207,
        "shared_name" : "Dolly_Parton () Don_Cobb",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Don_Cobb",
        "interaction" : "",
        "SUID" : 592,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "591",
        "source" : "161",
        "target" : "294",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Timmy_Collins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Timmy_Collins",
        "interaction" : "",
        "SUID" : 591,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "590",
        "source" : "161",
        "target" : "297",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Shane_Cooper",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Shane_Cooper",
        "interaction" : "",
        "SUID" : 590,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "589",
        "source" : "161",
        "target" : "298",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Suzanne_Coz",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Suzanne_Coz",
        "interaction" : "",
        "SUID" : 589,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "588",
        "source" : "161",
        "target" : "324",
        "EdgeBetweenness" : 23438.53227217,
        "shared_name" : "Dolly_Parton () Ciaran_Curran",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ciaran_Curran",
        "interaction" : "",
        "SUID" : 588,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "587",
        "source" : "161",
        "target" : "300",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Michael_Davis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Michael_Davis",
        "interaction" : "",
        "SUID" : 587,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "586",
        "source" : "161",
        "target" : "301",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Carol_Elliott",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Carol_Elliott",
        "interaction" : "",
        "SUID" : 586,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "585",
        "source" : "161",
        "target" : "302",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Jennifer_O'Brien_Enoch",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jennifer_O'Brien_Enoch",
        "interaction" : "",
        "SUID" : 585,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "584",
        "source" : "161",
        "target" : "303",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Curtis_Flatt",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Curtis_Flatt",
        "interaction" : "",
        "SUID" : 584,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "583",
        "source" : "161",
        "target" : "304",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Phil_Gitomer",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Phil_Gitomer",
        "interaction" : "",
        "SUID" : 583,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "582",
        "source" : "161",
        "target" : "305",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Teresa_Hughes",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Teresa_Hughes",
        "interaction" : "",
        "SUID" : 582,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "581",
        "source" : "161",
        "target" : "232",
        "EdgeBetweenness" : 69363.42685147,
        "shared_name" : "Dolly_Parton () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 581,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "580",
        "source" : "161",
        "target" : "284",
        "EdgeBetweenness" : 107890.7825958,
        "shared_name" : "Dolly_Parton () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 580,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "579",
        "source" : "161",
        "target" : "68",
        "EdgeBetweenness" : 41341.88589297,
        "shared_name" : "Dolly_Parton () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Carl_Jackson",
        "interaction" : "",
        "SUID" : 579,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "578",
        "source" : "161",
        "target" : "338",
        "EdgeBetweenness" : 23532.43718056,
        "shared_name" : "Dolly_Parton () Frankie_Kennedy",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Frankie_Kennedy",
        "interaction" : "",
        "SUID" : 578,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "577",
        "source" : "161",
        "target" : "309",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mark_Kiracofe",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mark_Kiracofe",
        "interaction" : "",
        "SUID" : 577,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "576",
        "source" : "161",
        "target" : "310",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Cari_Landers",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Cari_Landers",
        "interaction" : "",
        "SUID" : 576,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "575",
        "source" : "161",
        "target" : "296",
        "EdgeBetweenness" : 163907.73541316,
        "shared_name" : "Dolly_Parton () David_Lindley",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Lindley",
        "interaction" : "",
        "SUID" : 575,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "574",
        "source" : "161",
        "target" : "312",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Sean_McClintock",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sean_McClintock",
        "interaction" : "",
        "SUID" : 574,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "573",
        "source" : "161",
        "target" : "164",
        "EdgeBetweenness" : 52288.74576034,
        "shared_name" : "Dolly_Parton () Rob_McCoury",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rob_McCoury",
        "interaction" : "",
        "SUID" : 573,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "572",
        "source" : "161",
        "target" : "163",
        "EdgeBetweenness" : 84904.51612504,
        "shared_name" : "Dolly_Parton () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 572,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "571",
        "source" : "161",
        "target" : "330",
        "EdgeBetweenness" : 37305.42346325,
        "shared_name" : "Dolly_Parton () Mairead_Ni_Mhaonaigh",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mairead_Ni_Mhaonaigh",
        "interaction" : "",
        "SUID" : 571,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "570",
        "source" : "161",
        "target" : "313",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Maighread_Ni_Dhomnaill",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Maighread_Ni_Dhomnaill",
        "interaction" : "",
        "SUID" : 570,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "569",
        "source" : "161",
        "target" : "253",
        "EdgeBetweenness" : 402725.33038514,
        "shared_name" : "Dolly_Parton () Jerry_O'Sullivan",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jerry_O'Sullivan",
        "interaction" : "",
        "SUID" : 569,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "568",
        "source" : "161",
        "target" : "187",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Judy_Ogle",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Judy_Ogle",
        "interaction" : "",
        "SUID" : 568,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "567",
        "source" : "161",
        "target" : "314",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Ray_Parker",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ray_Parker",
        "interaction" : "",
        "SUID" : 567,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "566",
        "source" : "161",
        "target" : "316",
        "EdgeBetweenness" : 59846.721316,
        "shared_name" : "Dolly_Parton () Randy_Scruggs",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Randy_Scruggs",
        "interaction" : "",
        "SUID" : 566,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "565",
        "source" : "161",
        "target" : "213",
        "EdgeBetweenness" : 45499.22664145,
        "shared_name" : "Dolly_Parton () Lisa_Silver",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lisa_Silver",
        "interaction" : "",
        "SUID" : 565,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "564",
        "source" : "161",
        "target" : "317",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Phil_Sommers",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Phil_Sommers",
        "interaction" : "",
        "SUID" : 564,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "563",
        "source" : "161",
        "target" : "333",
        "EdgeBetweenness" : 50030.460176,
        "shared_name" : "Dolly_Parton () Daithi_Sproule",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Daithi_Sproule",
        "interaction" : "",
        "SUID" : 563,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "562",
        "source" : "161",
        "target" : "127",
        "EdgeBetweenness" : 81037.62588105,
        "shared_name" : "Dolly_Parton () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Harry_Stinson",
        "interaction" : "",
        "SUID" : 562,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "561",
        "source" : "161",
        "target" : "322",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () David_Sutherland",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Sutherland",
        "interaction" : "",
        "SUID" : 561,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "560",
        "source" : "161",
        "target" : "334",
        "EdgeBetweenness" : 17057.69352289,
        "shared_name" : "Dolly_Parton () Ciaran_Tourish",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ciaran_Tourish",
        "interaction" : "",
        "SUID" : 560,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "559",
        "source" : "161",
        "target" : "325",
        "EdgeBetweenness" : 20885.02478915,
        "shared_name" : "Dolly_Parton () Don_Warden",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Don_Warden",
        "interaction" : "",
        "SUID" : 559,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "558",
        "source" : "161",
        "target" : "299",
        "EdgeBetweenness" : 45322.03840752,
        "shared_name" : "Dolly_Parton () Bruce_Watkins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bruce_Watkins",
        "interaction" : "",
        "SUID" : 558,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "557",
        "source" : "161",
        "target" : "327",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Rollow_Welch",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rollow_Welch",
        "interaction" : "",
        "SUID" : 557,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "556",
        "source" : "161",
        "target" : "329",
        "EdgeBetweenness" : 30695.42673344,
        "shared_name" : "Dolly_Parton () Rick_Williams",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rick_Williams",
        "interaction" : "",
        "SUID" : 556,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "555",
        "source" : "161",
        "target" : "194",
        "EdgeBetweenness" : 33955.01033865,
        "shared_name" : "Dolly_Parton () Barry_Bales",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Barry_Bales",
        "interaction" : "",
        "SUID" : 555,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "554",
        "source" : "161",
        "target" : "273",
        "EdgeBetweenness" : 285449.97849961,
        "shared_name" : "Dolly_Parton () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 554,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "553",
        "source" : "161",
        "target" : "74",
        "EdgeBetweenness" : 75175.58664953,
        "shared_name" : "Dolly_Parton () Keith_Little",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Keith_Little",
        "interaction" : "",
        "SUID" : 553,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "552",
        "source" : "161",
        "target" : "332",
        "EdgeBetweenness" : 76373.58799358,
        "shared_name" : "Dolly_Parton () Patty_Loveless",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Patty_Loveless",
        "interaction" : "",
        "SUID" : 552,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "551",
        "source" : "161",
        "target" : "268",
        "EdgeBetweenness" : 90644.19310791,
        "shared_name" : "Dolly_Parton () Claire_Lynch",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Claire_Lynch",
        "interaction" : "",
        "SUID" : 551,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "550",
        "source" : "161",
        "target" : "276",
        "EdgeBetweenness" : 19884.94758187,
        "shared_name" : "Dolly_Parton () Jim_Mills",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jim_Mills",
        "interaction" : "",
        "SUID" : 550,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "549",
        "source" : "161",
        "target" : "235",
        "EdgeBetweenness" : 59219.65345324,
        "shared_name" : "Dolly_Parton () Alan_O'Bryant",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Alan_O'Bryant",
        "interaction" : "",
        "SUID" : 549,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "548",
        "source" : "161",
        "target" : "335",
        "EdgeBetweenness" : 36285.16636657,
        "shared_name" : "Dolly_Parton () Toby_Seay",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Toby_Seay",
        "interaction" : "",
        "SUID" : 548,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "547",
        "source" : "161",
        "target" : "107",
        "EdgeBetweenness" : 28654.40042161,
        "shared_name" : "Dolly_Parton () Darrin_Vincent",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Darrin_Vincent",
        "interaction" : "",
        "SUID" : 547,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "546",
        "source" : "161",
        "target" : "339",
        "EdgeBetweenness" : 13209.34837685,
        "shared_name" : "Dolly_Parton () Monisa_Angell",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Monisa_Angell",
        "interaction" : "",
        "SUID" : 546,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "545",
        "source" : "161",
        "target" : "340",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Pennie_Aust",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Pennie_Aust",
        "interaction" : "",
        "SUID" : 545,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "544",
        "source" : "161",
        "target" : "341",
        "EdgeBetweenness" : 13197.0390224,
        "shared_name" : "Dolly_Parton () Robert_Bailey,_Jr.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Robert_Bailey,_Jr.",
        "interaction" : "",
        "SUID" : 544,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "543",
        "source" : "161",
        "target" : "342",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Kristin_Bauer",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kristin_Bauer",
        "interaction" : "",
        "SUID" : 543,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "542",
        "source" : "161",
        "target" : "233",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Robert_Behar",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Robert_Behar",
        "interaction" : "",
        "SUID" : 542,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "541",
        "source" : "161",
        "target" : "344",
        "EdgeBetweenness" : 8541.00576922,
        "shared_name" : "Dolly_Parton () Lisa_Bevill",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lisa_Bevill",
        "interaction" : "",
        "SUID" : 541,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "540",
        "source" : "161",
        "target" : "345",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Matt_Boynton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Matt_Boynton",
        "interaction" : "",
        "SUID" : 540,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "539",
        "source" : "161",
        "target" : "79",
        "EdgeBetweenness" : 170227.06185113,
        "shared_name" : "Dolly_Parton () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sam_Bush",
        "interaction" : "",
        "SUID" : 539,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "538",
        "source" : "161",
        "target" : "346",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Lori_Casteel",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lori_Casteel",
        "interaction" : "",
        "SUID" : 538,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "537",
        "source" : "161",
        "target" : "347",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mike_Casteel",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mike_Casteel",
        "interaction" : "",
        "SUID" : 537,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "536",
        "source" : "161",
        "target" : "349",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Drew_Cline",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Drew_Cline",
        "interaction" : "",
        "SUID" : 536,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "535",
        "source" : "161",
        "target" : "62",
        "EdgeBetweenness" : 12258.04383143,
        "shared_name" : "Dolly_Parton () Lisa_Cochran",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lisa_Cochran",
        "interaction" : "",
        "SUID" : 535,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "534",
        "source" : "161",
        "target" : "64",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Judy_Collins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Judy_Collins",
        "interaction" : "",
        "SUID" : 534,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "533",
        "source" : "161",
        "target" : "65",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Angel_Cruz",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Angel_Cruz",
        "interaction" : "",
        "SUID" : 533,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "532",
        "source" : "161",
        "target" : "67",
        "EdgeBetweenness" : 95625.48864032,
        "shared_name" : "Dolly_Parton () Callie_Cryar",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Callie_Cryar",
        "interaction" : "",
        "SUID" : 532,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "531",
        "source" : "161",
        "target" : "70",
        "EdgeBetweenness" : 95625.48864032,
        "shared_name" : "Dolly_Parton () Phoebe_Cryar",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Phoebe_Cryar",
        "interaction" : "",
        "SUID" : 531,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "530",
        "source" : "161",
        "target" : "239",
        "EdgeBetweenness" : 24388.60106623,
        "shared_name" : "Dolly_Parton () David_Davidson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Davidson",
        "interaction" : "",
        "SUID" : 530,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "529",
        "source" : "161",
        "target" : "71",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Steve_Davis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Steve_Davis",
        "interaction" : "",
        "SUID" : 529,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "528",
        "source" : "161",
        "target" : "211",
        "EdgeBetweenness" : 187347.66841202,
        "shared_name" : "Dolly_Parton () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 528,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "527",
        "source" : "161",
        "target" : "308",
        "EdgeBetweenness" : 41732.03727686,
        "shared_name" : "Dolly_Parton () Terry_Eldredge",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Terry_Eldredge",
        "interaction" : "",
        "SUID" : 527,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "526",
        "source" : "161",
        "target" : "126",
        "EdgeBetweenness" : 13153.48072943,
        "shared_name" : "Dolly_Parton () Connie_Ellisor",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Connie_Ellisor",
        "interaction" : "",
        "SUID" : 526,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "525",
        "source" : "161",
        "target" : "75",
        "EdgeBetweenness" : 8072.9078143,
        "shared_name" : "Dolly_Parton () Kim_Fleming",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kim_Fleming",
        "interaction" : "",
        "SUID" : 525,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "524",
        "source" : "161",
        "target" : "76",
        "EdgeBetweenness" : 32385.06481139,
        "shared_name" : "Dolly_Parton () David_Foster",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Foster",
        "interaction" : "",
        "SUID" : 524,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "523",
        "source" : "161",
        "target" : "77",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Dave_Fowler",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dave_Fowler",
        "interaction" : "",
        "SUID" : 523,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "522",
        "source" : "161",
        "target" : "90",
        "EdgeBetweenness" : 10846.41705508,
        "shared_name" : "Dolly_Parton () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 522,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "521",
        "source" : "161",
        "target" : "78",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Jack_Green",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jack_Green",
        "interaction" : "",
        "SUID" : 521,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "520",
        "source" : "161",
        "target" : "132",
        "EdgeBetweenness" : 11765.58492345,
        "shared_name" : "Dolly_Parton () Jim_Grosjean",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jim_Grosjean",
        "interaction" : "",
        "SUID" : 520,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "519",
        "source" : "161",
        "target" : "223",
        "EdgeBetweenness" : 337613.26915945,
        "shared_name" : "Dolly_Parton () Andy_Hall",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Andy_Hall",
        "interaction" : "",
        "SUID" : 519,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "518",
        "source" : "161",
        "target" : "80",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Duane_Hamilton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Duane_Hamilton",
        "interaction" : "",
        "SUID" : 518,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "517",
        "source" : "161",
        "target" : "81",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () George_Hamilton_IV",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () George_Hamilton_IV",
        "interaction" : "",
        "SUID" : 517,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "516",
        "source" : "161",
        "target" : "291",
        "EdgeBetweenness" : 76025.5215208,
        "shared_name" : "Dolly_Parton () Jim_Hoke",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jim_Hoke",
        "interaction" : "",
        "SUID" : 516,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "515",
        "source" : "161",
        "target" : "83",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Paul_Hollowell",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Paul_Hollowell",
        "interaction" : "",
        "SUID" : 515,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "514",
        "source" : "161",
        "target" : "84",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mary_Hopkin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mary_Hopkin",
        "interaction" : "",
        "SUID" : 514,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "513",
        "source" : "161",
        "target" : "275",
        "EdgeBetweenness" : 76771.24535335,
        "shared_name" : "Dolly_Parton () Jan_Howard",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jan_Howard",
        "interaction" : "",
        "SUID" : 513,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "512",
        "source" : "161",
        "target" : "85",
        "EdgeBetweenness" : 20410.49902751,
        "shared_name" : "Dolly_Parton () Tom_Howard",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tom_Howard",
        "interaction" : "",
        "SUID" : 512,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "511",
        "source" : "161",
        "target" : "86",
        "EdgeBetweenness" : 20937.49718585,
        "shared_name" : "Dolly_Parton () Jon_Mark_Ivey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jon_Mark_Ivey",
        "interaction" : "",
        "SUID" : 511,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "510",
        "source" : "161",
        "target" : "87",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Tommy_James",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tommy_James",
        "interaction" : "",
        "SUID" : 510,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "509",
        "source" : "161",
        "target" : "88",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Tommy_Lee_James",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tommy_Lee_James",
        "interaction" : "",
        "SUID" : 509,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "508",
        "source" : "161",
        "target" : "89",
        "EdgeBetweenness" : 16203.59104954,
        "shared_name" : "Dolly_Parton () Jack_Jezzro",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jack_Jezzro",
        "interaction" : "",
        "SUID" : 508,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "507",
        "source" : "161",
        "target" : "288",
        "EdgeBetweenness" : 98601.86907182,
        "shared_name" : "Dolly_Parton () Jamie_Johnson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jamie_Johnson",
        "interaction" : "",
        "SUID" : 507,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "506",
        "source" : "161",
        "target" : "287",
        "EdgeBetweenness" : 183365.8860964,
        "shared_name" : "Dolly_Parton () George_Jones",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () George_Jones",
        "interaction" : "",
        "SUID" : 506,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "505",
        "source" : "161",
        "target" : "92",
        "EdgeBetweenness" : 93287.18145857,
        "shared_name" : "Dolly_Parton () Norah_Jones",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Norah_Jones",
        "interaction" : "",
        "SUID" : 505,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "504",
        "source" : "161",
        "target" : "93",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Kid_Connection_Inc.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kid_Connection_Inc.",
        "interaction" : "",
        "SUID" : 504,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "503",
        "source" : "161",
        "target" : "319",
        "EdgeBetweenness" : 123799.68586864,
        "shared_name" : "Dolly_Parton () Kris_Kristofferson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kris_Kristofferson",
        "interaction" : "",
        "SUID" : 503,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "502",
        "source" : "161",
        "target" : "94",
        "EdgeBetweenness" : 11137.31135403,
        "shared_name" : "Dolly_Parton () Anthony_LaMarchina",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Anthony_LaMarchina",
        "interaction" : "",
        "SUID" : 502,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "501",
        "source" : "161",
        "target" : "95",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Brenda_Lee",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Brenda_Lee",
        "interaction" : "",
        "SUID" : 501,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "500",
        "source" : "161",
        "target" : "295",
        "EdgeBetweenness" : 16697.12100604,
        "shared_name" : "Dolly_Parton () Bob_Mater",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bob_Mater",
        "interaction" : "",
        "SUID" : 500,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "499",
        "source" : "161",
        "target" : "112",
        "EdgeBetweenness" : 21323.00558045,
        "shared_name" : "Dolly_Parton () Jimmy_Mattingly",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jimmy_Mattingly",
        "interaction" : "",
        "SUID" : 499,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "498",
        "source" : "161",
        "target" : "96",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Mel_McDaniel",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mel_McDaniel",
        "interaction" : "",
        "SUID" : 498,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "497",
        "source" : "161",
        "target" : "91",
        "EdgeBetweenness" : 24371.60472399,
        "shared_name" : "Dolly_Parton () Roger_McGuinn",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Roger_McGuinn",
        "interaction" : "",
        "SUID" : 497,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "496",
        "source" : "161",
        "target" : "98",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Jimmy_C._Newman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jimmy_C._Newman",
        "interaction" : "",
        "SUID" : 496,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "495",
        "source" : "161",
        "target" : "99",
        "EdgeBetweenness" : 23421.99059123,
        "shared_name" : "Dolly_Parton () Joe_Nichols",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joe_Nichols",
        "interaction" : "",
        "SUID" : 495,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "494",
        "source" : "161",
        "target" : "101",
        "EdgeBetweenness" : 35461.73115316,
        "shared_name" : "Dolly_Parton () Nickel_Creek",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Nickel_Creek",
        "interaction" : "",
        "SUID" : 494,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "493",
        "source" : "161",
        "target" : "102",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Opry_Gang",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Opry_Gang",
        "interaction" : "",
        "SUID" : 493,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "492",
        "source" : "161",
        "target" : "103",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Ira_Parker",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ira_Parker",
        "interaction" : "",
        "SUID" : 492,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "491",
        "source" : "161",
        "target" : "104",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Kelly_Pribble",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kelly_Pribble",
        "interaction" : "",
        "SUID" : 491,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "490",
        "source" : "161",
        "target" : "105",
        "EdgeBetweenness" : 19912.1391111,
        "shared_name" : "Dolly_Parton () Carole_Rabinowitz-Neuen",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Carole_Rabinowitz-Neuen",
        "interaction" : "",
        "SUID" : 490,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "489",
        "source" : "161",
        "target" : "277",
        "EdgeBetweenness" : 161301.56092603,
        "shared_name" : "Dolly_Parton () Tony_Rice",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tony_Rice",
        "interaction" : "",
        "SUID" : 489,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "488",
        "source" : "161",
        "target" : "251",
        "EdgeBetweenness" : 13197.0390224,
        "shared_name" : "Dolly_Parton () Cheryl_Riddle",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Cheryl_Riddle",
        "interaction" : "",
        "SUID" : 488,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "487",
        "source" : "161",
        "target" : "250",
        "EdgeBetweenness" : 18801.95157816,
        "shared_name" : "Dolly_Parton () Danny_Roberts",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Danny_Roberts",
        "interaction" : "",
        "SUID" : 487,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "486",
        "source" : "161",
        "target" : "256",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Rose_Sanico",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rose_Sanico",
        "interaction" : "",
        "SUID" : 486,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "485",
        "source" : "161",
        "target" : "106",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Joey_Schmidt",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joey_Schmidt",
        "interaction" : "",
        "SUID" : 485,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "484",
        "source" : "161",
        "target" : "108",
        "EdgeBetweenness" : 18610.62941599,
        "shared_name" : "Dolly_Parton () Jeannie_Seely",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jeannie_Seely",
        "interaction" : "",
        "SUID" : 484,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "483",
        "source" : "161",
        "target" : "109",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Alan_Silverman",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Alan_Silverman",
        "interaction" : "",
        "SUID" : 483,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "482",
        "source" : "161",
        "target" : "149",
        "EdgeBetweenness" : 15650.72033945,
        "shared_name" : "Dolly_Parton () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 482,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "481",
        "source" : "161",
        "target" : "110",
        "EdgeBetweenness" : 67519.32362207,
        "shared_name" : "Dolly_Parton () Mindy_Smith",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mindy_Smith",
        "interaction" : "",
        "SUID" : 481,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "480",
        "source" : "161",
        "target" : "66",
        "EdgeBetweenness" : 23284.91867146,
        "shared_name" : "Dolly_Parton () Terry_Smith",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Terry_Smith",
        "interaction" : "",
        "SUID" : 480,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "479",
        "source" : "161",
        "target" : "113",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Tony_Smith",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tony_Smith",
        "interaction" : "",
        "SUID" : 479,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "478",
        "source" : "161",
        "target" : "245",
        "EdgeBetweenness" : 280045.21293563,
        "shared_name" : "Dolly_Parton () Kathy_Stewart",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kathy_Stewart",
        "interaction" : "",
        "SUID" : 478,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "477",
        "source" : "161",
        "target" : "217",
        "EdgeBetweenness" : 78361.31464701,
        "shared_name" : "Dolly_Parton () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 477,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "476",
        "source" : "161",
        "target" : "227",
        "EdgeBetweenness" : 35709.21505192,
        "shared_name" : "Dolly_Parton () David_Talbot",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Talbot",
        "interaction" : "",
        "SUID" : 476,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "475",
        "source" : "161",
        "target" : "311",
        "EdgeBetweenness" : 65104.99917762,
        "shared_name" : "Dolly_Parton () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chris_Thile",
        "interaction" : "",
        "SUID" : 475,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "474",
        "source" : "161",
        "target" : "115",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Chuck_Tilley",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chuck_Tilley",
        "interaction" : "",
        "SUID" : 474,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "473",
        "source" : "161",
        "target" : "116",
        "EdgeBetweenness" : 27732.39931738,
        "shared_name" : "Dolly_Parton () Pam_Tillis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Pam_Tillis",
        "interaction" : "",
        "SUID" : 473,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "472",
        "source" : "161",
        "target" : "117",
        "EdgeBetweenness" : 22546.81642681,
        "shared_name" : "Dolly_Parton () Ilya_Toshinsky",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ilya_Toshinsky",
        "interaction" : "",
        "SUID" : 472,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "471",
        "source" : "161",
        "target" : "151",
        "EdgeBetweenness" : 17719.7212643,
        "shared_name" : "Dolly_Parton () Alan_Umstead",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Alan_Umstead",
        "interaction" : "",
        "SUID" : 471,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "470",
        "source" : "161",
        "target" : "155",
        "EdgeBetweenness" : 19858.36163414,
        "shared_name" : "Dolly_Parton () Catherine_Umstead",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Catherine_Umstead",
        "interaction" : "",
        "SUID" : 470,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "469",
        "source" : "161",
        "target" : "118",
        "EdgeBetweenness" : 12950.74539839,
        "shared_name" : "Dolly_Parton () Keith_Urban",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Keith_Urban",
        "interaction" : "",
        "SUID" : 469,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "468",
        "source" : "161",
        "target" : "157",
        "EdgeBetweenness" : 18208.97072332,
        "shared_name" : "Dolly_Parton () Mary_Kathryn_Van_Osdale",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mary_Kathryn_Van_Osdale",
        "interaction" : "",
        "SUID" : 468,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "467",
        "source" : "161",
        "target" : "160",
        "EdgeBetweenness" : 10753.01724998,
        "shared_name" : "Dolly_Parton () Gary_VanOsdale",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Gary_VanOsdale",
        "interaction" : "",
        "SUID" : 467,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "466",
        "source" : "161",
        "target" : "172",
        "EdgeBetweenness" : 185352.8246653,
        "shared_name" : "Dolly_Parton () Rhonda_Vincent",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Rhonda_Vincent",
        "interaction" : "",
        "SUID" : 466,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "465",
        "source" : "161",
        "target" : "119",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Sasha_Vosk",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sasha_Vosk",
        "interaction" : "",
        "SUID" : 465,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "464",
        "source" : "161",
        "target" : "120",
        "EdgeBetweenness" : 93900.14864135,
        "shared_name" : "Dolly_Parton () Porter_Wagoner",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Porter_Wagoner",
        "interaction" : "",
        "SUID" : 464,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "463",
        "source" : "161",
        "target" : "121",
        "EdgeBetweenness" : 13260.23338836,
        "shared_name" : "Dolly_Parton () Billy_Joe_Walker,_Jr.",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Billy_Joe_Walker,_Jr.",
        "interaction" : "",
        "SUID" : 463,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "462",
        "source" : "161",
        "target" : "122",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Doctor_Ming_Wang",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Doctor_Ming_Wang",
        "interaction" : "",
        "SUID" : 462,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "461",
        "source" : "161",
        "target" : "170",
        "EdgeBetweenness" : 136619.81469207,
        "shared_name" : "Dolly_Parton () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sara_Watkins",
        "interaction" : "",
        "SUID" : 461,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "460",
        "source" : "161",
        "target" : "156",
        "EdgeBetweenness" : 63461.42360414,
        "shared_name" : "Dolly_Parton () Sean_Watkins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Sean_Watkins",
        "interaction" : "",
        "SUID" : 460,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "459",
        "source" : "161",
        "target" : "123",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Emily_Webb",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Emily_Webb",
        "interaction" : "",
        "SUID" : 459,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "458",
        "source" : "161",
        "target" : "266",
        "EdgeBetweenness" : 21838.1516339,
        "shared_name" : "Dolly_Parton () Kent_Wells",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kent_Wells",
        "interaction" : "",
        "SUID" : 458,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "457",
        "source" : "161",
        "target" : "261",
        "EdgeBetweenness" : 22863.04769915,
        "shared_name" : "Dolly_Parton () Kristin_Wilkinson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kristin_Wilkinson",
        "interaction" : "",
        "SUID" : 457,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "456",
        "source" : "161",
        "target" : "125",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Anna_Wilson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Anna_Wilson",
        "interaction" : "",
        "SUID" : 456,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "455",
        "source" : "161",
        "target" : "201",
        "EdgeBetweenness" : 22546.93966147,
        "shared_name" : "Dolly_Parton () Lee_Ann_Womack",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Lee_Ann_Womack",
        "interaction" : "",
        "SUID" : 455,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "454",
        "source" : "161",
        "target" : "128",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Yusuf",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Yusuf",
        "interaction" : "",
        "SUID" : 454,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "453",
        "source" : "161",
        "target" : "129",
        "EdgeBetweenness" : 8541.00576922,
        "shared_name" : "Dolly_Parton () Bob_Bailey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Bob_Bailey",
        "interaction" : "",
        "SUID" : 453,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "452",
        "source" : "161",
        "target" : "279",
        "EdgeBetweenness" : 34067.62853709,
        "shared_name" : "Dolly_Parton () Eddie_Bayers",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Eddie_Bayers",
        "interaction" : "",
        "SUID" : 452,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "451",
        "source" : "161",
        "target" : "131",
        "EdgeBetweenness" : 15134.74552068,
        "shared_name" : "Dolly_Parton () Matraca_Berg",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Matraca_Berg",
        "interaction" : "",
        "SUID" : 451,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "450",
        "source" : "161",
        "target" : "133",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Crystal_Bernard",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Crystal_Bernard",
        "interaction" : "",
        "SUID" : 450,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "449",
        "source" : "161",
        "target" : "267",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () David_Blair",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Blair",
        "interaction" : "",
        "SUID" : 449,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "448",
        "source" : "161",
        "target" : "69",
        "EdgeBetweenness" : 9781.74817253,
        "shared_name" : "Dolly_Parton () Steve_Buckingham",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Steve_Buckingham",
        "interaction" : "",
        "SUID" : 448,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "447",
        "source" : "161",
        "target" : "270",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Jennie_Carey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jennie_Carey",
        "interaction" : "",
        "SUID" : 447,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "446",
        "source" : "161",
        "target" : "135",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Kim_Carnes",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Kim_Carnes",
        "interaction" : "",
        "SUID" : 446,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "445",
        "source" : "161",
        "target" : "130",
        "EdgeBetweenness" : 22445.22715925,
        "shared_name" : "Dolly_Parton () Mark_Casstevens",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Mark_Casstevens",
        "interaction" : "",
        "SUID" : 445,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "444",
        "source" : "161",
        "target" : "289",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Tony_Chase",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Tony_Chase",
        "interaction" : "",
        "SUID" : 444,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "443",
        "source" : "161",
        "target" : "145",
        "EdgeBetweenness" : 8358.21510515,
        "shared_name" : "Dolly_Parton () Suzanne_Cox",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Suzanne_Cox",
        "interaction" : "",
        "SUID" : 443,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "442",
        "source" : "161",
        "target" : "176",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Richard_Dennison",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Richard_Dennison",
        "interaction" : "",
        "SUID" : 442,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "441",
        "source" : "161",
        "target" : "138",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Steve_Dorff",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Steve_Dorff",
        "interaction" : "",
        "SUID" : 441,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "440",
        "source" : "161",
        "target" : "243",
        "EdgeBetweenness" : 64573.86298204,
        "shared_name" : "Dolly_Parton () Dan_Dugmore",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dan_Dugmore",
        "interaction" : "",
        "SUID" : 440,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "439",
        "source" : "161",
        "target" : "281",
        "EdgeBetweenness" : 44037.68099398,
        "shared_name" : "Dolly_Parton () Paul_Franklin",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Paul_Franklin",
        "interaction" : "",
        "SUID" : 439,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "438",
        "source" : "161",
        "target" : "247",
        "EdgeBetweenness" : 9976.19378645,
        "shared_name" : "Dolly_Parton () Vicki_Hampton",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Vicki_Hampton",
        "interaction" : "",
        "SUID" : 438,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "437",
        "source" : "161",
        "target" : "242",
        "EdgeBetweenness" : 15265.65482434,
        "shared_name" : "Dolly_Parton () David_Hidalgo",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Hidalgo",
        "interaction" : "",
        "SUID" : 437,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "436",
        "source" : "161",
        "target" : "328",
        "EdgeBetweenness" : 15715.66072374,
        "shared_name" : "Dolly_Parton () David_Hungate",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () David_Hungate",
        "interaction" : "",
        "SUID" : 436,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "435",
        "source" : "161",
        "target" : "215",
        "EdgeBetweenness" : 213994.33067879,
        "shared_name" : "Dolly_Parton () Alison_Krauss",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Alison_Krauss",
        "interaction" : "",
        "SUID" : 435,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "434",
        "source" : "161",
        "target" : "134",
        "EdgeBetweenness" : 45048.53396761,
        "shared_name" : "Dolly_Parton () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 434,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "433",
        "source" : "161",
        "target" : "141",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Ladysmith_Black_Mambazo",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Ladysmith_Black_Mambazo",
        "interaction" : "",
        "SUID" : 433,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "432",
        "source" : "161",
        "target" : "143",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Andy_Landis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Andy_Landis",
        "interaction" : "",
        "SUID" : 432,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "431",
        "source" : "161",
        "target" : "142",
        "EdgeBetweenness" : 66809.56398746,
        "shared_name" : "Dolly_Parton () Raul_Malo",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Raul_Malo",
        "interaction" : "",
        "SUID" : 431,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "430",
        "source" : "161",
        "target" : "144",
        "EdgeBetweenness" : 23529.005907,
        "shared_name" : "Dolly_Parton () Liana_Manis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Liana_Manis",
        "interaction" : "",
        "SUID" : 430,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "429",
        "source" : "161",
        "target" : "167",
        "EdgeBetweenness" : 42469.7893419,
        "shared_name" : "Dolly_Parton () Pat_McInerney",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Pat_McInerney",
        "interaction" : "",
        "SUID" : 429,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "428",
        "source" : "161",
        "target" : "146",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Darci_Monet",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Darci_Monet",
        "interaction" : "",
        "SUID" : 428,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "427",
        "source" : "161",
        "target" : "148",
        "EdgeBetweenness" : 35709.52753187,
        "shared_name" : "Dolly_Parton () Farrell_Morris",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Farrell_Morris",
        "interaction" : "",
        "SUID" : 427,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "426",
        "source" : "161",
        "target" : "252",
        "EdgeBetweenness" : 76679.74046753,
        "shared_name" : "Dolly_Parton () Louis_Dean_Nunley",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Louis_Dean_Nunley",
        "interaction" : "",
        "SUID" : 426,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "425",
        "source" : "161",
        "target" : "248",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Jennifer_O'Brien",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Jennifer_O'Brien",
        "interaction" : "",
        "SUID" : 425,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "424",
        "source" : "161",
        "target" : "225",
        "EdgeBetweenness" : 25370.63015529,
        "shared_name" : "Dolly_Parton () Dean_Parks",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dean_Parks",
        "interaction" : "",
        "SUID" : 424,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "423",
        "source" : "161",
        "target" : "150",
        "EdgeBetweenness" : 22944.09916961,
        "shared_name" : "Dolly_Parton () John_Popper",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () John_Popper",
        "interaction" : "",
        "SUID" : 423,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "422",
        "source" : "161",
        "target" : "154",
        "EdgeBetweenness" : 9623.20150131,
        "shared_name" : "Dolly_Parton () Don_Potter",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Don_Potter",
        "interaction" : "",
        "SUID" : 422,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "421",
        "source" : "161",
        "target" : "292",
        "EdgeBetweenness" : 160642.03139924,
        "shared_name" : "Dolly_Parton () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 421,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "420",
        "source" : "161",
        "target" : "158",
        "EdgeBetweenness" : 10907.16521959,
        "shared_name" : "Dolly_Parton () Chris_Rodriguez",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chris_Rodriguez",
        "interaction" : "",
        "SUID" : 420,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "419",
        "source" : "161",
        "target" : "336",
        "EdgeBetweenness" : 41880.2412101,
        "shared_name" : "Dolly_Parton () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Matt_Rollings",
        "interaction" : "",
        "SUID" : 419,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "418",
        "source" : "161",
        "target" : "315",
        "EdgeBetweenness" : 19703.7597691,
        "shared_name" : "Dolly_Parton () John_Wesley_Ryles",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () John_Wesley_Ryles",
        "interaction" : "",
        "SUID" : 418,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "417",
        "source" : "161",
        "target" : "124",
        "EdgeBetweenness" : 43877.36957206,
        "shared_name" : "Dolly_Parton () John_Sebastian",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () John_Sebastian",
        "interaction" : "",
        "SUID" : 417,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "416",
        "source" : "161",
        "target" : "259",
        "EdgeBetweenness" : 50434.78800973,
        "shared_name" : "Dolly_Parton () Joe_Spivey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Joe_Spivey",
        "interaction" : "",
        "SUID" : 416,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "415",
        "source" : "161",
        "target" : "166",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Duane_Starling",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Duane_Starling",
        "interaction" : "",
        "SUID" : 415,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "414",
        "source" : "161",
        "target" : "192",
        "EdgeBetweenness" : 80920.11660659,
        "shared_name" : "Dolly_Parton () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Adam_Steffey",
        "interaction" : "",
        "SUID" : 414,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "413",
        "source" : "161",
        "target" : "219",
        "EdgeBetweenness" : 95569.02460466,
        "shared_name" : "Dolly_Parton () Dan_Tyminski",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dan_Tyminski",
        "interaction" : "",
        "SUID" : 413,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "412",
        "source" : "161",
        "target" : "169",
        "EdgeBetweenness" : 24588.0,
        "shared_name" : "Dolly_Parton () Chris_Willis",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Chris_Willis",
        "interaction" : "",
        "SUID" : 412,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "411",
        "source" : "161",
        "target" : "264",
        "EdgeBetweenness" : 28185.39140044,
        "shared_name" : "Dolly_Parton () Dennis_Wilson",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Dennis_Wilson",
        "interaction" : "",
        "SUID" : 411,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "410",
        "source" : "161",
        "target" : "207",
        "EdgeBetweenness" : 72199.08281584,
        "shared_name" : "Dolly_Parton () Reggie_Young",
        "shared_interaction" : "",
        "name" : "Dolly_Parton () Reggie_Young",
        "interaction" : "",
        "SUID" : 410,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "409",
        "source" : "156",
        "target" : "273",
        "EdgeBetweenness" : 94130.3682415,
        "shared_name" : "Sean_Watkins () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Sean_Watkins () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 409,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "408",
        "source" : "156",
        "target" : "211",
        "EdgeBetweenness" : 73042.85361107,
        "shared_name" : "Sean_Watkins () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Sean_Watkins () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 408,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "407",
        "source" : "156",
        "target" : "311",
        "EdgeBetweenness" : 12182.90842759,
        "shared_name" : "Sean_Watkins () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Sean_Watkins () Chris_Thile",
        "interaction" : "",
        "SUID" : 407,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "406",
        "source" : "156",
        "target" : "170",
        "EdgeBetweenness" : 9987.7735761,
        "shared_name" : "Sean_Watkins () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Sean_Watkins () Sara_Watkins",
        "interaction" : "",
        "SUID" : 406,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "405",
        "source" : "152",
        "target" : "211",
        "EdgeBetweenness" : 154069.17175126,
        "shared_name" : "Randy_Kohrs () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 405,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "404",
        "source" : "152",
        "target" : "161",
        "EdgeBetweenness" : 219045.42259886,
        "shared_name" : "Randy_Kohrs () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Dolly_Parton",
        "interaction" : "",
        "SUID" : 404,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "403",
        "source" : "152",
        "target" : "68",
        "EdgeBetweenness" : 34298.45921457,
        "shared_name" : "Randy_Kohrs () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Carl_Jackson",
        "interaction" : "",
        "SUID" : 403,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "402",
        "source" : "152",
        "target" : "107",
        "EdgeBetweenness" : 8694.58731776,
        "shared_name" : "Randy_Kohrs () Darrin_Vincent",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Darrin_Vincent",
        "interaction" : "",
        "SUID" : 402,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "401",
        "source" : "152",
        "target" : "172",
        "EdgeBetweenness" : 88602.48283747,
        "shared_name" : "Randy_Kohrs () Rhonda_Vincent",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Rhonda_Vincent",
        "interaction" : "",
        "SUID" : 401,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "400",
        "source" : "152",
        "target" : "208",
        "EdgeBetweenness" : 23041.35299258,
        "shared_name" : "Randy_Kohrs () Ron_Block",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Ron_Block",
        "interaction" : "",
        "SUID" : 400,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "399",
        "source" : "152",
        "target" : "79",
        "EdgeBetweenness" : 138963.91639173,
        "shared_name" : "Randy_Kohrs () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Sam_Bush",
        "interaction" : "",
        "SUID" : 399,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "398",
        "source" : "152",
        "target" : "192",
        "EdgeBetweenness" : 30022.61489644,
        "shared_name" : "Randy_Kohrs () Adam_Steffey",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Adam_Steffey",
        "interaction" : "",
        "SUID" : 398,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "397",
        "source" : "152",
        "target" : "217",
        "EdgeBetweenness" : 43804.8529456,
        "shared_name" : "Randy_Kohrs () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Randy_Kohrs () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 397,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "396",
        "source" : "142",
        "target" : "214",
        "EdgeBetweenness" : 2753.48055861,
        "shared_name" : "Raul_Malo () Lenny_Castro",
        "shared_interaction" : "",
        "name" : "Raul_Malo () Lenny_Castro",
        "interaction" : "",
        "SUID" : 396,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "395",
        "source" : "142",
        "target" : "291",
        "EdgeBetweenness" : 10529.67481354,
        "shared_name" : "Raul_Malo () Jim_Hoke",
        "shared_interaction" : "",
        "name" : "Raul_Malo () Jim_Hoke",
        "interaction" : "",
        "SUID" : 395,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "394",
        "source" : "137",
        "target" : "79",
        "EdgeBetweenness" : 45051.37285859,
        "shared_name" : "Solomon_Burke () Sam_Bush",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Sam_Bush",
        "interaction" : "",
        "SUID" : 394,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "393",
        "source" : "137",
        "target" : "90",
        "EdgeBetweenness" : 4098.76567648,
        "shared_name" : "Solomon_Burke () Carl_Gorodetzky",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Carl_Gorodetzky",
        "interaction" : "",
        "SUID" : 393,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "392",
        "source" : "137",
        "target" : "174",
        "EdgeBetweenness" : 61179.57709186,
        "shared_name" : "Solomon_Burke () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 392,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "391",
        "source" : "137",
        "target" : "209",
        "EdgeBetweenness" : 11722.28069821,
        "shared_name" : "Solomon_Burke () Byron_House",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Byron_House",
        "interaction" : "",
        "SUID" : 391,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "390",
        "source" : "137",
        "target" : "85",
        "EdgeBetweenness" : 4177.50097249,
        "shared_name" : "Solomon_Burke () Tom_Howard",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Tom_Howard",
        "interaction" : "",
        "SUID" : 390,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "389",
        "source" : "137",
        "target" : "332",
        "EdgeBetweenness" : 18914.13557195,
        "shared_name" : "Solomon_Burke () Patty_Loveless",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Patty_Loveless",
        "interaction" : "",
        "SUID" : 389,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "388",
        "source" : "137",
        "target" : "161",
        "EdgeBetweenness" : 53803.41795581,
        "shared_name" : "Solomon_Burke () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Dolly_Parton",
        "interaction" : "",
        "SUID" : 388,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "387",
        "source" : "137",
        "target" : "105",
        "EdgeBetweenness" : 4324.39749059,
        "shared_name" : "Solomon_Burke () Carole_Rabinowitz-Neuen",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Carole_Rabinowitz-Neuen",
        "interaction" : "",
        "SUID" : 387,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "386",
        "source" : "137",
        "target" : "149",
        "EdgeBetweenness" : 5296.25317041,
        "shared_name" : "Solomon_Burke () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Solomon_Burke () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 386,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "385",
        "source" : "120",
        "target" : "275",
        "EdgeBetweenness" : 11052.57731789,
        "shared_name" : "Porter_Wagoner () Jan_Howard",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Jan_Howard",
        "interaction" : "",
        "SUID" : 385,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "384",
        "source" : "120",
        "target" : "161",
        "EdgeBetweenness" : 93900.14864135,
        "shared_name" : "Porter_Wagoner () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Dolly_Parton",
        "interaction" : "",
        "SUID" : 384,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "383",
        "source" : "120",
        "target" : "292",
        "EdgeBetweenness" : 24673.92021141,
        "shared_name" : "Porter_Wagoner () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 383,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "382",
        "source" : "120",
        "target" : "325",
        "EdgeBetweenness" : 3702.97521085,
        "shared_name" : "Porter_Wagoner () Don_Warden",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Don_Warden",
        "interaction" : "",
        "SUID" : 382,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "381",
        "source" : "120",
        "target" : "211",
        "EdgeBetweenness" : 151910.05676199,
        "shared_name" : "Porter_Wagoner () Stuart_Duncan",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Stuart_Duncan",
        "interaction" : "",
        "SUID" : 381,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "380",
        "source" : "120",
        "target" : "68",
        "EdgeBetweenness" : 26188.52157227,
        "shared_name" : "Porter_Wagoner () Carl_Jackson",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Carl_Jackson",
        "interaction" : "",
        "SUID" : 380,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "379",
        "source" : "120",
        "target" : "127",
        "EdgeBetweenness" : 29891.3889094,
        "shared_name" : "Porter_Wagoner () Harry_Stinson",
        "shared_interaction" : "",
        "name" : "Porter_Wagoner () Harry_Stinson",
        "interaction" : "",
        "SUID" : 379,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "378",
        "source" : "110",
        "target" : "69",
        "EdgeBetweenness" : 2417.24606112,
        "shared_name" : "Mindy_Smith () Steve_Buckingham",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Steve_Buckingham",
        "interaction" : "",
        "SUID" : 378,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "377",
        "source" : "110",
        "target" : "243",
        "EdgeBetweenness" : 14145.87826841,
        "shared_name" : "Mindy_Smith () Dan_Dugmore",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Dan_Dugmore",
        "interaction" : "",
        "SUID" : 377,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "376",
        "source" : "110",
        "target" : "126",
        "EdgeBetweenness" : 3351.0560736,
        "shared_name" : "Mindy_Smith () Connie_Ellisor",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Connie_Ellisor",
        "interaction" : "",
        "SUID" : 376,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "375",
        "source" : "110",
        "target" : "132",
        "EdgeBetweenness" : 3613.97980455,
        "shared_name" : "Mindy_Smith () Jim_Grosjean",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Jim_Grosjean",
        "interaction" : "",
        "SUID" : 375,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "374",
        "source" : "110",
        "target" : "134",
        "EdgeBetweenness" : 11623.66367663,
        "shared_name" : "Mindy_Smith () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 374,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "373",
        "source" : "110",
        "target" : "161",
        "EdgeBetweenness" : 67519.32362207,
        "shared_name" : "Mindy_Smith () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Dolly_Parton",
        "interaction" : "",
        "SUID" : 373,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "372",
        "source" : "110",
        "target" : "336",
        "EdgeBetweenness" : 14779.48718035,
        "shared_name" : "Mindy_Smith () Matt_Rollings",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Matt_Rollings",
        "interaction" : "",
        "SUID" : 372,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "371",
        "source" : "110",
        "target" : "149",
        "EdgeBetweenness" : 6744.58492326,
        "shared_name" : "Mindy_Smith () Pamela_Sixfin",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Pamela_Sixfin",
        "interaction" : "",
        "SUID" : 371,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "370",
        "source" : "110",
        "target" : "217",
        "EdgeBetweenness" : 40710.45748828,
        "shared_name" : "Mindy_Smith () Bryan_Sutton",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Bryan_Sutton",
        "interaction" : "",
        "SUID" : 370,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "369",
        "source" : "110",
        "target" : "261",
        "EdgeBetweenness" : 6819.61806957,
        "shared_name" : "Mindy_Smith () Kristin_Wilkinson",
        "shared_interaction" : "",
        "name" : "Mindy_Smith () Kristin_Wilkinson",
        "interaction" : "",
        "SUID" : 369,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "368",
        "source" : "101",
        "target" : "209",
        "EdgeBetweenness" : 10041.66817911,
        "shared_name" : "Nickel_Creek () Byron_House",
        "shared_interaction" : "",
        "name" : "Nickel_Creek () Byron_House",
        "interaction" : "",
        "SUID" : 368,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "367",
        "source" : "101",
        "target" : "114",
        "EdgeBetweenness" : 3376.33026013,
        "shared_name" : "Nickel_Creek () Gary_Paczosa",
        "shared_interaction" : "",
        "name" : "Nickel_Creek () Gary_Paczosa",
        "interaction" : "",
        "SUID" : 367,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "366",
        "source" : "101",
        "target" : "311",
        "EdgeBetweenness" : 8486.25564769,
        "shared_name" : "Nickel_Creek () Chris_Thile",
        "shared_interaction" : "",
        "name" : "Nickel_Creek () Chris_Thile",
        "interaction" : "",
        "SUID" : 366,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "365",
        "source" : "101",
        "target" : "170",
        "EdgeBetweenness" : 6403.52891486,
        "shared_name" : "Nickel_Creek () Sara_Watkins",
        "shared_interaction" : "",
        "name" : "Nickel_Creek () Sara_Watkins",
        "interaction" : "",
        "SUID" : 365,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "364",
        "source" : "101",
        "target" : "156",
        "EdgeBetweenness" : 2367.69683524,
        "shared_name" : "Nickel_Creek () Sean_Watkins",
        "shared_interaction" : "",
        "name" : "Nickel_Creek () Sean_Watkins",
        "interaction" : "",
        "SUID" : 364,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "363",
        "source" : "100",
        "target" : "308",
        "EdgeBetweenness" : 8063.40198656,
        "shared_name" : "Johnny_Russell () Terry_Eldredge",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Terry_Eldredge",
        "interaction" : "",
        "SUID" : 363,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "362",
        "source" : "100",
        "target" : "164",
        "EdgeBetweenness" : 7959.39382294,
        "shared_name" : "Johnny_Russell () Rob_McCoury",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Rob_McCoury",
        "interaction" : "",
        "SUID" : 362,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "361",
        "source" : "100",
        "target" : "163",
        "EdgeBetweenness" : 23698.50454081,
        "shared_name" : "Johnny_Russell () Ronnie_McCoury",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Ronnie_McCoury",
        "interaction" : "",
        "SUID" : 361,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "360",
        "source" : "100",
        "target" : "306",
        "EdgeBetweenness" : 8872.66754848,
        "shared_name" : "Johnny_Russell () Bobby_Osborne",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Bobby_Osborne",
        "interaction" : "",
        "SUID" : 360,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "359",
        "source" : "100",
        "target" : "161",
        "EdgeBetweenness" : 49339.51179228,
        "shared_name" : "Johnny_Russell () Dolly_Parton",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Dolly_Parton",
        "interaction" : "",
        "SUID" : 359,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "358",
        "source" : "100",
        "target" : "292",
        "EdgeBetweenness" : 14931.25254953,
        "shared_name" : "Johnny_Russell () Hargus_\"Pig\"_Robbins",
        "shared_interaction" : "",
        "name" : "Johnny_Russell () Hargus_\"Pig\"_Robbins",
        "interaction" : "",
        "SUID" : 358,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "357",
        "source" : "79",
        "target" : "232",
        "EdgeBetweenness" : 44936.89525747,
        "shared_name" : "Sam_Bush () Roy_Huskey_Jr.",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Roy_Huskey_Jr.",
        "interaction" : "",
        "SUID" : 357,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "356",
        "source" : "79",
        "target" : "284",
        "EdgeBetweenness" : 40851.54311191,
        "shared_name" : "Sam_Bush () Roy_M._\"Junior\"_Husky",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Roy_M._\"Junior\"_Husky",
        "interaction" : "",
        "SUID" : 356,
        "selected" : true
      },
      "selected" : true
    }, {
      "data" : {
        "id" : "355",
        "source" : "79",
        "target" : "134",
        "EdgeBetweenness" : 16807.81823441,
        "shared_name" : "Sam_Bush () Viktor_Krauss",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Viktor_Krauss",
        "interaction" : "",
        "SUID" : 355,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "354",
        "source" : "79",
        "target" : "273",
        "EdgeBetweenness" : 65838.50102878,
        "shared_name" : "Sam_Bush () Jerry_Douglas",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Jerry_Douglas",
        "interaction" : "",
        "SUID" : 354,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "353",
        "source" : "79",
        "target" : "209",
        "EdgeBetweenness" : 41242.63728812,
        "shared_name" : "Sam_Bush () Byron_House",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Byron_House",
        "interaction" : "",
        "SUID" : 353,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "352",
        "source" : "79",
        "target" : "174",
        "EdgeBetweenness" : 281402.8232735,
        "shared_name" : "Sam_Bush () Emmylou_Harris",
        "shared_interaction" : "",
        "name" : "Sam_Bush () Emmylou_Harris",
        "interaction" : "",
        "SUID" : 352,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "351",
        "source" : "79",
        "target" : "190",
        "EdgeBetweenness" : 77427.39791508,
        "shared_name" : "Sam_Bush () David_Grisman",
        "shared_interaction" : "",
        "name" : "Sam_Bush () David_Grisman",
        "interaction" : "",
        "SUID" : 351,
        "selected" : false
      },
      "selected" : false
    }, {
      "data" : {
        "id" : "350",
        "source" : "74",
        "target" : "268",
        "EdgeBetweenness" : 19187.69743972,
        "shared_name" : "Keith_Little () Claire_Lynch",
        "shared_interaction" : "",
        "name" : "Keith_Little () Claire_Lynch",
        "interaction" : "",
        "SUID" : 350,
        "selected" : false
      },
      "selected" : false
    } ]
  }
}